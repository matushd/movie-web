--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-07-19 00:07:00 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE "movieDb";
--
-- TOC entry 2224 (class 1262 OID 16384)
-- Name: movieDb; Type: DATABASE; Schema: -; Owner: app
--

CREATE DATABASE "movieDb" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pl_PL.UTF-8' LC_CTYPE = 'pl_PL.UTF-8';


ALTER DATABASE "movieDb" OWNER TO app;

\connect "movieDb"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 1 (class 3079 OID 11789)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 229 (class 1255 OID 16640)
-- Name: clear_db(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION clear_db() RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM public.movie_company;
	DELETE FROM public.movie_country;
	DELETE FROM public.movie_genre;
	DELETE FROM public.company;
	DELETE FROM public.country;
	DELETE FROM public.crew_in;
	DELETE FROM public.crew_image;
	DELETE FROM public.crew;
	DELETE FROM public.department;
	DELETE FROM public.genre;
	DELETE FROM public.role;
	DELETE FROM public.actor_image;
	DELETE FROM public.actor;
	DELETE FROM public.movie_image;
	DELETE FROM public.image;
	DELETE FROM public.image_type;
	DELETE FROM public.movie_company;
	DELETE FROM public.movie_country;
	DELETE FROM public.movie_genre;
	DELETE FROM public.movie_keyword;
	DELETE FROM public.keyword;
	DELETE FROM public.video;
	DELETE FROM public.movie_similar;
	DELETE FROM public.review;
	DELETE FROM public.movie;
	DELETE FROM public.video_type;
	DELETE FROM public.personal;
	RETURN 1;   
END;
$$;


ALTER FUNCTION public.clear_db() OWNER TO postgres;

--
-- TOC entry 230 (class 1255 OID 98752)
-- Name: movie_similar_bi_trg(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION movie_similar_bi_trg() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE movie_tmp INT;
BEGIN
	IF NEW.movie_id = NEW.movie_similar_id THEN
		return null;
	ELSEIF NEW.movie_id > NEW.movie_similar_id THEN
		movie_tmp := NEW.movie_similar_id;
		NEW.movie_similar_id := NEW.movie_id; 
		NEW.movie_id := movie_tmp;
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.movie_similar_bi_trg() OWNER TO postgres;

--
-- TOC entry 231 (class 1255 OID 98755)
-- Name: movie_similar_bu_trg(); Type: FUNCTION; Schema: public; Owner: app
--

CREATE FUNCTION movie_similar_bu_trg() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE movie_tmp INT;
BEGIN
    IF NEW.movie_id = OLD.movie_similar_id OR NEW.movie_similar_id = OLD.movie_id THEN
	RETURN NULL;
    END IF;
    IF NEW.movie_id >= NEW.movie_similar_id  THEN
		movie_tmp := NEW.movie_similar_id;
		NEW.movie_similar_id := NEW.movie_id; 
		NEW.movie_id := movie_tmp;
    END IF;
    IF OLD.movie_one >= NEW.movie_similar_id AND NEW.movie_id = null THEN
		NEW.movie_similar_id := OLD.movie_id; 
		NEW.movie_id := NEW.movie_similar_id;
    END IF;
    IF NEW.movie_id >= OLD.movie_similar_id AND NEW.movie_similar_id = null THEN
		NEW.movie_similar_id := NEW.movie_id; 
		NEW.movie_id := OLD.movie_similar_id;
    END IF;
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.movie_similar_bu_trg() OWNER TO app;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 16386)
-- Name: actor; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE actor (
    actor_id integer NOT NULL,
    actor_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.actor OWNER TO app;

--
-- TOC entry 172 (class 1259 OID 16389)
-- Name: actor_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE actor_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.actor_id_seq OWNER TO app;

--
-- TOC entry 211 (class 1259 OID 140018)
-- Name: actor_image; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE actor_image (
    actor_id integer NOT NULL,
    image_id integer NOT NULL
);


ALTER TABLE public.actor_image OWNER TO app;

--
-- TOC entry 173 (class 1259 OID 16396)
-- Name: company; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE company (
    company_id integer NOT NULL,
    company_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.company OWNER TO app;

--
-- TOC entry 174 (class 1259 OID 16399)
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE company_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_id_seq OWNER TO app;

--
-- TOC entry 175 (class 1259 OID 16401)
-- Name: country; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE country (
    country_id integer NOT NULL,
    country_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL,
    country_iso character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.country OWNER TO app;

--
-- TOC entry 176 (class 1259 OID 16404)
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE country_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id_seq OWNER TO app;

--
-- TOC entry 177 (class 1259 OID 16406)
-- Name: crew; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE crew (
    crew_id integer NOT NULL,
    crew_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL,
    department_id integer NOT NULL
);


ALTER TABLE public.crew OWNER TO app;

--
-- TOC entry 178 (class 1259 OID 16409)
-- Name: crew_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE crew_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crew_id_seq OWNER TO app;

--
-- TOC entry 212 (class 1259 OID 140038)
-- Name: crew_image; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE crew_image (
    crew_id integer NOT NULL,
    image_id integer NOT NULL
);


ALTER TABLE public.crew_image OWNER TO app;

--
-- TOC entry 179 (class 1259 OID 16411)
-- Name: crew_in; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE crew_in (
    crew_in_id integer NOT NULL,
    crew_id integer NOT NULL,
    movie_id integer NOT NULL,
    crew_in_job character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.crew_in OWNER TO app;

--
-- TOC entry 180 (class 1259 OID 16414)
-- Name: crew_in_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE crew_in_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crew_in_id_seq OWNER TO app;

--
-- TOC entry 181 (class 1259 OID 16416)
-- Name: department; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE department (
    department_id integer NOT NULL,
    department_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.department OWNER TO app;

--
-- TOC entry 182 (class 1259 OID 16419)
-- Name: department_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE department_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.department_id_seq OWNER TO app;

--
-- TOC entry 183 (class 1259 OID 16421)
-- Name: genre; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE genre (
    genre_id integer NOT NULL,
    genre_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.genre OWNER TO app;

--
-- TOC entry 184 (class 1259 OID 16424)
-- Name: genre_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE genre_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genre_id_seq OWNER TO app;

--
-- TOC entry 185 (class 1259 OID 16426)
-- Name: image; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE image (
    image_id integer NOT NULL,
    image_width integer NOT NULL,
    image_height integer NOT NULL,
    image_file_path character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL,
    image_type_id integer NOT NULL,
    image_hash_id character varying(50) COLLATE pg_catalog."pl_PL.utf8"
);


ALTER TABLE public.image OWNER TO app;

--
-- TOC entry 186 (class 1259 OID 16429)
-- Name: image_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE image_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.image_id_seq OWNER TO app;

--
-- TOC entry 187 (class 1259 OID 16431)
-- Name: image_type; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE image_type (
    image_type_id integer NOT NULL,
    image_type_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.image_type OWNER TO app;

--
-- TOC entry 188 (class 1259 OID 16434)
-- Name: image_type_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE image_type_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.image_type_id_seq OWNER TO app;

--
-- TOC entry 209 (class 1259 OID 115213)
-- Name: keyword; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE keyword (
    keyword_id integer NOT NULL,
    keyword_name character(50) NOT NULL
);


ALTER TABLE public.keyword OWNER TO app;

--
-- TOC entry 189 (class 1259 OID 16436)
-- Name: movie; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie (
    movie_id integer NOT NULL,
    movie_title character varying(90) NOT NULL,
    movie_vote_avg double precision NOT NULL,
    movie_vote_count integer NOT NULL,
    movie_date date DEFAULT ('now'::text)::date NOT NULL,
    movie_original_title character varying(90) NOT NULL,
    movie_overview text COLLATE pg_catalog."pl_PL.utf8",
    movie_release_date date DEFAULT ('now'::text)::date NOT NULL,
    movie_homepage character(64) COLLATE pg_catalog."pl_PL.utf8",
    movie_budget double precision,
    movie_runtime integer,
    movie_revenue double precision,
    movie_original_lang character(3)
);


ALTER TABLE public.movie OWNER TO app;

--
-- TOC entry 190 (class 1259 OID 16444)
-- Name: movie_company; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie_company (
    company_id integer NOT NULL,
    movie_id integer NOT NULL
);


ALTER TABLE public.movie_company OWNER TO app;

--
-- TOC entry 191 (class 1259 OID 16447)
-- Name: movie_company_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE movie_company_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_company_id_seq OWNER TO app;

--
-- TOC entry 192 (class 1259 OID 16449)
-- Name: movie_country; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie_country (
    country_id integer NOT NULL,
    movie_id integer NOT NULL
);


ALTER TABLE public.movie_country OWNER TO app;

--
-- TOC entry 193 (class 1259 OID 16452)
-- Name: movie_country_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE movie_country_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_country_id_seq OWNER TO app;

--
-- TOC entry 194 (class 1259 OID 16454)
-- Name: movie_genre; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie_genre (
    genre_id integer NOT NULL,
    movie_id integer NOT NULL
);


ALTER TABLE public.movie_genre OWNER TO app;

--
-- TOC entry 195 (class 1259 OID 16457)
-- Name: movie_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE movie_genre_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_genre_id_seq OWNER TO app;

--
-- TOC entry 196 (class 1259 OID 16459)
-- Name: movie_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE movie_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_id_seq OWNER TO app;

--
-- TOC entry 197 (class 1259 OID 16461)
-- Name: movie_image; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie_image (
    movie_id integer NOT NULL,
    image_id integer NOT NULL
);


ALTER TABLE public.movie_image OWNER TO app;

--
-- TOC entry 198 (class 1259 OID 16464)
-- Name: movie_image_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE movie_image_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_image_id_seq OWNER TO app;

--
-- TOC entry 210 (class 1259 OID 115222)
-- Name: movie_keyword; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie_keyword (
    keyword_id integer NOT NULL,
    movie_id integer NOT NULL
);


ALTER TABLE public.movie_keyword OWNER TO app;

--
-- TOC entry 207 (class 1259 OID 98745)
-- Name: movie_similar; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE movie_similar (
    movie_id integer NOT NULL,
    movie_similar_id integer NOT NULL
);


ALTER TABLE public.movie_similar OWNER TO app;

--
-- TOC entry 214 (class 1259 OID 189187)
-- Name: pagerank_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE pagerank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pagerank_id_seq OWNER TO app;

--
-- TOC entry 213 (class 1259 OID 189181)
-- Name: pagerank; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE pagerank (
    pagerank_id integer DEFAULT nextval('pagerank_id_seq'::regclass) NOT NULL,
    pagerank_url character varying(72) COLLATE pg_catalog."pl_PL.utf8" NOT NULL,
    pagerank_value double precision DEFAULT 0 NOT NULL,
    pagerank_date date DEFAULT ('now'::text)::date NOT NULL,
    pagerank_title character varying(128) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.pagerank OWNER TO app;

--
-- TOC entry 206 (class 1259 OID 98701)
-- Name: personal; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE personal (
    personal_id integer NOT NULL,
    personal_bio text COLLATE pg_catalog."pl_PL.utf8",
    personal_homepage character varying(64) COLLATE pg_catalog."pl_PL.utf8",
    personal_popularity double precision,
    personal_birth_place character varying(64) COLLATE pg_catalog."pl_PL.utf8",
    personal_birth date,
    personal_death date,
    personal_adult boolean DEFAULT false,
    personal_knows_as character varying(128) COLLATE pg_catalog."pl_PL.utf8",
    personal_gender integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.personal OWNER TO app;

--
-- TOC entry 216 (class 1259 OID 189282)
-- Name: recommended_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE recommended_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recommended_id_seq OWNER TO app;

--
-- TOC entry 215 (class 1259 OID 189265)
-- Name: recommended; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE recommended (
    recommended_id integer DEFAULT nextval('recommended_id_seq'::regclass) NOT NULL,
    recommended_movie_id integer NOT NULL,
    recommended_prediction double precision DEFAULT 1 NOT NULL,
    recommended_date date DEFAULT now() NOT NULL,
    recommended_userid character varying(255) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.recommended OWNER TO app;

--
-- TOC entry 208 (class 1259 OID 115198)
-- Name: review; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE review (
    review_id character(32) NOT NULL,
    review_author character(90) COLLATE pg_catalog."pl_PL.utf8" NOT NULL,
    movie_id integer NOT NULL,
    review_content text COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.review OWNER TO app;

--
-- TOC entry 199 (class 1259 OID 16466)
-- Name: role; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE role (
    role_id integer NOT NULL,
    role_character character varying(150) NOT NULL,
    role_order smallint NOT NULL,
    actor_id integer NOT NULL,
    movie_id integer NOT NULL,
    role_profile_path character varying(50)
);


ALTER TABLE public.role OWNER TO app;

--
-- TOC entry 200 (class 1259 OID 16469)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE role_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO app;

--
-- TOC entry 201 (class 1259 OID 16471)
-- Name: userconnection; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE userconnection (
    providerid character varying(255) NOT NULL,
    provideruserid character varying(255) NOT NULL,
    rank integer NOT NULL,
    displayname character varying(255),
    profileurl character varying(512),
    imageurl character varying(512),
    accesstoken character varying(255) NOT NULL,
    secret character varying(255),
    refreshtoken character varying(255),
    expiretime bigint,
    userid character varying(255) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.userconnection OWNER TO app;

--
-- TOC entry 202 (class 1259 OID 16477)
-- Name: video; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE video (
    video_id integer NOT NULL,
    video_name character varying(90) NOT NULL,
    video_key character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL,
    video_size integer NOT NULL,
    video_type_id integer NOT NULL,
    movie_id integer NOT NULL
);


ALTER TABLE public.video OWNER TO app;

--
-- TOC entry 203 (class 1259 OID 16480)
-- Name: video_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE video_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_id_seq OWNER TO app;

--
-- TOC entry 204 (class 1259 OID 16482)
-- Name: video_type; Type: TABLE; Schema: public; Owner: app; Tablespace: 
--

CREATE TABLE video_type (
    video_type_id integer NOT NULL,
    video_type_name character varying(50) COLLATE pg_catalog."pl_PL.utf8" NOT NULL
);


ALTER TABLE public.video_type OWNER TO app;

--
-- TOC entry 205 (class 1259 OID 16485)
-- Name: video_type_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE video_type_id_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_type_id_seq OWNER TO app;

--
-- TOC entry 2019 (class 2606 OID 16491)
-- Name: pk_actor; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY actor
    ADD CONSTRAINT pk_actor PRIMARY KEY (actor_id);


--
-- TOC entry 2077 (class 2606 OID 140022)
-- Name: pk_actor_image; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY actor_image
    ADD CONSTRAINT pk_actor_image PRIMARY KEY (image_id, actor_id);


--
-- TOC entry 2021 (class 2606 OID 16493)
-- Name: pk_company; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY company
    ADD CONSTRAINT pk_company PRIMARY KEY (company_id);


--
-- TOC entry 2023 (class 2606 OID 16495)
-- Name: pk_country; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT pk_country PRIMARY KEY (country_id);


--
-- TOC entry 2027 (class 2606 OID 16497)
-- Name: pk_crew; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY crew
    ADD CONSTRAINT pk_crew PRIMARY KEY (crew_id);


--
-- TOC entry 2079 (class 2606 OID 140042)
-- Name: pk_crew_image; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY crew_image
    ADD CONSTRAINT pk_crew_image PRIMARY KEY (image_id, crew_id);


--
-- TOC entry 2029 (class 2606 OID 16499)
-- Name: pk_crew_in; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY crew_in
    ADD CONSTRAINT pk_crew_in PRIMARY KEY (crew_in_id);


--
-- TOC entry 2031 (class 2606 OID 16501)
-- Name: pk_department; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY department
    ADD CONSTRAINT pk_department PRIMARY KEY (department_id);


--
-- TOC entry 2035 (class 2606 OID 16503)
-- Name: pk_genre; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY genre
    ADD CONSTRAINT pk_genre PRIMARY KEY (genre_id);


--
-- TOC entry 2037 (class 2606 OID 16505)
-- Name: pk_image; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY image
    ADD CONSTRAINT pk_image PRIMARY KEY (image_id);


--
-- TOC entry 2041 (class 2606 OID 16507)
-- Name: pk_image_type; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY image_type
    ADD CONSTRAINT pk_image_type PRIMARY KEY (image_type_id);


--
-- TOC entry 2073 (class 2606 OID 115217)
-- Name: pk_keyword_id; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY keyword
    ADD CONSTRAINT pk_keyword_id PRIMARY KEY (keyword_id);


--
-- TOC entry 2045 (class 2606 OID 16509)
-- Name: pk_movie; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie
    ADD CONSTRAINT pk_movie PRIMARY KEY (movie_id);


--
-- TOC entry 2047 (class 2606 OID 16511)
-- Name: pk_movie_company; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie_company
    ADD CONSTRAINT pk_movie_company PRIMARY KEY (company_id, movie_id);


--
-- TOC entry 2049 (class 2606 OID 16513)
-- Name: pk_movie_country; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie_country
    ADD CONSTRAINT pk_movie_country PRIMARY KEY (country_id, movie_id);


--
-- TOC entry 2051 (class 2606 OID 16515)
-- Name: pk_movie_genre; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie_genre
    ADD CONSTRAINT pk_movie_genre PRIMARY KEY (genre_id, movie_id);


--
-- TOC entry 2053 (class 2606 OID 16517)
-- Name: pk_movie_image; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie_image
    ADD CONSTRAINT pk_movie_image PRIMARY KEY (image_id, movie_id);


--
-- TOC entry 2075 (class 2606 OID 115226)
-- Name: pk_movie_keyword; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie_keyword
    ADD CONSTRAINT pk_movie_keyword PRIMARY KEY (keyword_id, movie_id);


--
-- TOC entry 2069 (class 2606 OID 98749)
-- Name: pk_movie_similar; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY movie_similar
    ADD CONSTRAINT pk_movie_similar PRIMARY KEY (movie_id, movie_similar_id);


--
-- TOC entry 2081 (class 2606 OID 189186)
-- Name: pk_pagerank; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY pagerank
    ADD CONSTRAINT pk_pagerank PRIMARY KEY (pagerank_id);


--
-- TOC entry 2067 (class 2606 OID 98708)
-- Name: pk_personal; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT pk_personal PRIMARY KEY (personal_id);


--
-- TOC entry 2083 (class 2606 OID 189271)
-- Name: pk_recommended; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY recommended
    ADD CONSTRAINT pk_recommended PRIMARY KEY (recommended_id);


--
-- TOC entry 2071 (class 2606 OID 115205)
-- Name: pk_review_id; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY review
    ADD CONSTRAINT pk_review_id PRIMARY KEY (review_id);


--
-- TOC entry 2055 (class 2606 OID 16519)
-- Name: pk_role; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT pk_role PRIMARY KEY (role_id);


--
-- TOC entry 2057 (class 2606 OID 189285)
-- Name: pk_userconnection; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY userconnection
    ADD CONSTRAINT pk_userconnection PRIMARY KEY (userid);


--
-- TOC entry 2059 (class 2606 OID 16521)
-- Name: pk_video; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY video
    ADD CONSTRAINT pk_video PRIMARY KEY (video_id);


--
-- TOC entry 2063 (class 2606 OID 16523)
-- Name: pk_video_type; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY video_type
    ADD CONSTRAINT pk_video_type PRIMARY KEY (video_type_id);


--
-- TOC entry 2025 (class 2606 OID 74030)
-- Name: ux_country_iso; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT ux_country_iso UNIQUE (country_iso);


--
-- TOC entry 2033 (class 2606 OID 74047)
-- Name: ux_department_name; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY department
    ADD CONSTRAINT ux_department_name UNIQUE (department_name);


--
-- TOC entry 2039 (class 2606 OID 74043)
-- Name: ux_image_hash_id; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY image
    ADD CONSTRAINT ux_image_hash_id UNIQUE (image_hash_id);


--
-- TOC entry 2043 (class 2606 OID 74045)
-- Name: ux_image_type_name; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY image_type
    ADD CONSTRAINT ux_image_type_name UNIQUE (image_type_name);


--
-- TOC entry 2061 (class 2606 OID 74041)
-- Name: ux_video_key; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY video
    ADD CONSTRAINT ux_video_key UNIQUE (video_key);


--
-- TOC entry 2065 (class 2606 OID 74039)
-- Name: ux_video_type_name; Type: CONSTRAINT; Schema: public; Owner: app; Tablespace: 
--

ALTER TABLE ONLY video_type
    ADD CONSTRAINT ux_video_type_name UNIQUE (video_type_name);


--
-- TOC entry 2111 (class 2620 OID 98753)
-- Name: movie_similar_bi_trg; Type: TRIGGER; Schema: public; Owner: app
--

CREATE TRIGGER movie_similar_bi_trg BEFORE INSERT ON movie_similar FOR EACH ROW EXECUTE PROCEDURE movie_similar_bi_trg();


--
-- TOC entry 2112 (class 2620 OID 98756)
-- Name: movie_similar_bu_trg; Type: TRIGGER; Schema: public; Owner: app
--

CREATE TRIGGER movie_similar_bu_trg BEFORE UPDATE ON movie_similar FOR EACH ROW EXECUTE PROCEDURE movie_similar_bu_trg();


--
-- TOC entry 2096 (class 2606 OID 16531)
-- Name: fk1_actor; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY role
    ADD CONSTRAINT fk1_actor FOREIGN KEY (actor_id) REFERENCES actor(actor_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2105 (class 2606 OID 140023)
-- Name: fk1_actor; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY actor_image
    ADD CONSTRAINT fk1_actor FOREIGN KEY (actor_id) REFERENCES actor(actor_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2088 (class 2606 OID 16536)
-- Name: fk1_company; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_company
    ADD CONSTRAINT fk1_company FOREIGN KEY (company_id) REFERENCES company(company_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2090 (class 2606 OID 16541)
-- Name: fk1_country; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_country
    ADD CONSTRAINT fk1_country FOREIGN KEY (country_id) REFERENCES country(country_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2107 (class 2606 OID 140043)
-- Name: fk1_crew; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY crew_image
    ADD CONSTRAINT fk1_crew FOREIGN KEY (crew_id) REFERENCES crew(crew_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2084 (class 2606 OID 16546)
-- Name: fk1_department; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY crew
    ADD CONSTRAINT fk1_department FOREIGN KEY (department_id) REFERENCES department(department_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2092 (class 2606 OID 16551)
-- Name: fk1_genre; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_genre
    ADD CONSTRAINT fk1_genre FOREIGN KEY (genre_id) REFERENCES genre(genre_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2087 (class 2606 OID 16561)
-- Name: fk1_image_type; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY image
    ADD CONSTRAINT fk1_image_type FOREIGN KEY (image_type_id) REFERENCES image_type(image_type_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2094 (class 2606 OID 16566)
-- Name: fk1_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_image
    ADD CONSTRAINT fk1_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2085 (class 2606 OID 16571)
-- Name: fk1_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY crew_in
    ADD CONSTRAINT fk1_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2098 (class 2606 OID 16576)
-- Name: fk1_video_type; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY video
    ADD CONSTRAINT fk1_video_type FOREIGN KEY (video_type_id) REFERENCES video_type(video_type_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2086 (class 2606 OID 16581)
-- Name: fk2_crew; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY crew_in
    ADD CONSTRAINT fk2_crew FOREIGN KEY (crew_id) REFERENCES crew(crew_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2095 (class 2606 OID 16591)
-- Name: fk2_image; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_image
    ADD CONSTRAINT fk2_image FOREIGN KEY (image_id) REFERENCES image(image_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2106 (class 2606 OID 140028)
-- Name: fk2_image; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY actor_image
    ADD CONSTRAINT fk2_image FOREIGN KEY (image_id) REFERENCES image(image_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2108 (class 2606 OID 140048)
-- Name: fk2_image; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY crew_image
    ADD CONSTRAINT fk2_image FOREIGN KEY (image_id) REFERENCES image(image_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2097 (class 2606 OID 16596)
-- Name: fk2_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY role
    ADD CONSTRAINT fk2_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2091 (class 2606 OID 16601)
-- Name: fk2_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_country
    ADD CONSTRAINT fk2_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2089 (class 2606 OID 16606)
-- Name: fk2_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_company
    ADD CONSTRAINT fk2_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2093 (class 2606 OID 16611)
-- Name: fk2_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_genre
    ADD CONSTRAINT fk2_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2099 (class 2606 OID 16616)
-- Name: fk2_movie; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY video
    ADD CONSTRAINT fk2_movie FOREIGN KEY (movie_id) REFERENCES movie(movie_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2103 (class 2606 OID 115227)
-- Name: fk_keyword_id; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_keyword
    ADD CONSTRAINT fk_keyword_id FOREIGN KEY (keyword_id) REFERENCES keyword(keyword_id);


--
-- TOC entry 2100 (class 2606 OID 98757)
-- Name: fk_movie_id; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_similar
    ADD CONSTRAINT fk_movie_id FOREIGN KEY (movie_id) REFERENCES movie(movie_id);


--
-- TOC entry 2104 (class 2606 OID 115232)
-- Name: fk_movie_id; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_keyword
    ADD CONSTRAINT fk_movie_id FOREIGN KEY (movie_id) REFERENCES movie(movie_id);


--
-- TOC entry 2101 (class 2606 OID 98762)
-- Name: fk_movie_similar_id; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY movie_similar
    ADD CONSTRAINT fk_movie_similar_id FOREIGN KEY (movie_similar_id) REFERENCES movie(movie_id);


--
-- TOC entry 2109 (class 2606 OID 189272)
-- Name: fk_recommended_movie_id; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY recommended
    ADD CONSTRAINT fk_recommended_movie_id FOREIGN KEY (recommended_movie_id) REFERENCES movie(movie_id);


--
-- TOC entry 2110 (class 2606 OID 189286)
-- Name: fk_recommended_userconnection; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY recommended
    ADD CONSTRAINT fk_recommended_userconnection FOREIGN KEY (recommended_userid) REFERENCES userconnection(userid);


--
-- TOC entry 2102 (class 2606 OID 115206)
-- Name: pk_movie_id; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY review
    ADD CONSTRAINT pk_movie_id FOREIGN KEY (movie_id) REFERENCES movie(movie_id);


--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-07-19 00:07:00 CEST

--
-- PostgreSQL database dump complete
--

