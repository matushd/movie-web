/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.image.model;

import com.emde.movieweb.actor.model.Actor;
import com.emde.movieweb.crew.model.Crew;
import com.emde.movieweb.movie.model.Movie;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.logging.log4j.LogManager;
import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "image")
public class Image implements Serializable {
    
	@Version
	private static final long serialVersionUID = -1615353848233979553L;

	public final static String BASE_URL = "http://image.tmdb.org/t/p/w";
    
    @Id
    @SequenceGenerator(name = "imageSeq", sequenceName="image_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "imageSeq")
    @Column(name = "image_id")
    private Integer id;
    
    @Column(name = "image_width")
    private Integer width;
    
    @Column(name = "image_height")
    private Integer height;
    
    @Column(name = "image_file_path")
    private String filePath;
    
    @JsonBackReference
    @Column(name = "image_hash_id", unique = true)
    private String hashId;
    
    @ManyToOne
    @JoinColumn(name = "image_type_id")
    private ImageType type;
    
    @JsonIgnore
    @ManyToMany(mappedBy="images", 
    	cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Actor> actors;
    
    @JsonIgnore
    @ManyToMany(mappedBy="images", 
    	cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Crew> crews;
    
    @JsonIgnore
    @ManyToMany(mappedBy="images", 
    	cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Movie> movies;

    public Image() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getFilePath() {
        return filePath;
    }
    
    public String getFilePath(Integer width) {
        return BASE_URL + width + filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }
    
    public void setHashId(byte[] hashId) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(hashId);
            this.hashId = new String("a_"+messageDigest.digest());
        } catch (NoSuchAlgorithmException ex) {
            LogManager.getLogger(Image.class.getName()).warn(ex.getMessage(), ex);
        }
    }

    public Set<Actor> getActors() {
        return actors;
    }

    public void setActors(Set<Actor> actors) {
        this.actors = actors;
    }

    public Set<Crew> getCrews() {
        return crews;
    }

    public void setCrews(Set<Crew> crews) {
        this.crews = crews;
    }
    
}
