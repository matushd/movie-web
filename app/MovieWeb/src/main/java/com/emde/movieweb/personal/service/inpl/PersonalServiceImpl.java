/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.personal.service.inpl;

import java.sql.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emde.movieweb.personal.exception.PersonalAlreadyExists;
import com.emde.movieweb.personal.exception.PersonalNotFound;
import com.emde.movieweb.personal.model.Personal;
import com.emde.movieweb.personal.repository.PersonalRepository;
import com.emde.movieweb.personal.service.PersonalService;
import com.google.common.base.Strings;
import com.omertron.themoviedbapi.model.person.PersonInfo;

/**
 *
 * @author emde
 */
@Service
public class PersonalServiceImpl implements PersonalService {

	@Resource
    private PersonalRepository personalRepository;
	
	@Override
	@Transactional(rollbackFor=PersonalAlreadyExists.class)
	public Personal create(Personal personal) throws PersonalAlreadyExists {
		if(personalRepository.exists(personal.getId())) {
			throw new PersonalAlreadyExists(personal.getId());
		}
		return personalRepository.save(personal);
	}
	
	@Override
	@Transactional(rollbackFor=PersonalAlreadyExists.class)
	public Personal create(PersonInfo person) throws PersonalAlreadyExists {
		if(personalRepository.exists(person.getId())) {
			throw new PersonalAlreadyExists(person.getId());
		}
		Personal personal = new Personal();
		personal.setAdult(person.isAdult());
		personal.setBio(person.getBiography());
		if(!Strings.isNullOrEmpty(person.getBirthday())) {
			personal.setBirthDate(Date.valueOf(person.getBirthday()));
		}
		personal.setBirthPlace(person.getPlaceOfBirth());
		if(!Strings.isNullOrEmpty(person.getDeathday())) {
			personal.setDeathDate(Date.valueOf(person.getDeathday()));
		}
		personal.setHomepage(person.getHomepage());
		personal.setId(person.getId());
		personal.setKnowsAs(person.getAlsoKnownAs().toString()
				.substring(1, person.getAlsoKnownAs().toString().length()-1));
		personal.setPopularity(new Double(person.getPopularity()));
		personal.setGender(person.getGender().ordinal());
		return personalRepository.save(personal);
	}

	@Override
	@Transactional(rollbackFor=PersonalNotFound.class)
	public Personal delete(Integer id) throws PersonalNotFound {
		Personal deleted = personalRepository.findOne(id);
        if (deleted == null) {
        	throw new PersonalNotFound();
        }
        personalRepository.delete(deleted);
        return deleted;
	}

	@Override
	@Transactional
	public List<Personal> findAll() {
		return personalRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor=PersonalNotFound.class)
	public Personal update(Personal personal) throws PersonalNotFound {
		if(!personalRepository.exists(personal.getId())) {
			throw new PersonalNotFound();
		}
        return personalRepository.save(personal);
	}

	@Override
	@Transactional
	public Personal findById(Integer id) {
		return personalRepository.findOne(id);
	}
    
}
