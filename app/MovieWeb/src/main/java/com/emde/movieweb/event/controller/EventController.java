/**
 * 
 */
package com.emde.movieweb.event.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emde.movieweb.event.model.Event;

/**
 * @author emde
 *
 */
@RestController
@RequestMapping(value="/event")
public class EventController {
	
	private static Logger logger = LogManager.getLogger(EventController.class.getName());
	
	/**
	 * Event push handler
	 * @param event
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/push", method=RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public String push(@RequestBody Event event, HttpSession session,
			UsernamePasswordAuthenticationToken user) {
		event.setUserId(user != null ? (String)user.getPrincipal() : "");
		event.setSessionId(session.getId());
		logger.info(event);
		return "";
	}
}
