/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.crew.service.inpl;

import com.emde.movieweb.crew.exception.CrewAlreadyExists;
import com.emde.movieweb.crew.exception.CrewNotFound;
import com.emde.movieweb.crew.exception.CrewInAlreadyExists;
import com.emde.movieweb.crew.exception.CrewInNotFound;
import com.emde.movieweb.crew.model.Crew;
import com.emde.movieweb.crew.model.Job;
import com.emde.movieweb.crew.repository.CrewRepository;
import com.emde.movieweb.crew.repository.JobRepository;
import com.emde.movieweb.crew.service.CrewService;
import com.emde.movieweb.crew.specifications.CrewSpecifications;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.repository.MovieRepository;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class CrewServiceImpl implements CrewService {

    @Resource
    private CrewRepository crewRepository;
    
    @Resource
    private JobRepository crewInRepository;
    
    @Resource
    private MovieRepository movieRepository;
    
    /* Crew */
    @Override
    @Transactional(rollbackFor=CrewAlreadyExists.class)
    public Crew create(Crew crew) throws CrewAlreadyExists {
    	if(crewRepository.exists(crew.getId())) {
    		throw new CrewAlreadyExists(crew.getId());
    	}
        return crewRepository.save(crew);
    }

    @Override
    @Transactional(rollbackFor=CrewNotFound.class)
    public Crew delete(Integer id) throws CrewNotFound {
        Crew deleted = crewRepository.findOne(id);
        if (deleted == null) {
        	throw new CrewNotFound();
        }
        crewRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public Page<Crew> findAll(Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Crew> crews =  crewRepository.findAll(pageRequest);
    	crews.iterator().forEachRemaining(crew -> {
    		Hibernate.initialize(crew.getImage());
    	});
    	return crews;
    }

    @Override
    @Transactional(rollbackFor=CrewNotFound.class)
    public Crew update(Crew crew) throws CrewNotFound {
    	if(!crewRepository.exists(crew.getId())) {
    		throw new CrewNotFound();
    	}
        return crewRepository.save(crew);
    }

    @Override
    @Transactional
    public Crew findById(Integer id) {
        return crewRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Crew findFullById(Integer id) {
    	Crew crew = crewRepository.findOne(id);
    	Hibernate.initialize(crew.getJobs());
    	Hibernate.initialize(crew.getImages());
        return crew;
    }

    /* Job */
    @Override
    @Transactional(rollbackFor=CrewInAlreadyExists.class)
    public Job createJob(Job crewIn) throws CrewInAlreadyExists {
    	if (crewIn.getId() != null && crewInRepository.exists(crewIn.getId())) {
    		throw new CrewInAlreadyExists(crewIn.getId());
    	}
        return crewInRepository.save(crewIn);
    }

    @Override
    @Transactional(rollbackFor=CrewInNotFound.class)
    public Job deleteJob(Integer id) throws CrewInNotFound {
        Job deleted = crewInRepository.findOne(id);
        if (deleted == null) {
        	throw new CrewInNotFound();
        }
        crewInRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<Job> findAllJob() {
        return crewInRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=CrewInNotFound.class)
    public Job updateJob(Job crewIn) throws CrewInNotFound {
    	if(!crewInRepository.exists(crewIn.getId())) {
    		throw new CrewInNotFound();
    	}
        return crewInRepository.save(crewIn);
    }

    @Override
    @Transactional
    public Job findJobById(Integer id) {
        return crewInRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Page<Job> searchByMovieId(Integer movieId, Integer pageNo, Integer size)
    		throws MovieNotFound, CrewNotFound {
    	if(!movieRepository.exists(movieId)) {
    		throw new MovieNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Job> jobs = crewInRepository.searchByMovieId(movieId, pageRequest);
    	if(jobs.getSize() <= 0) {
    		throw new CrewNotFound();
    	}
    	jobs.iterator().forEachRemaining(job -> {
    		Hibernate.initialize(job.getCrew().getImage());
    	});
    	return jobs;
    }
    
    @Override
    @Transactional
    public TreeMap<Integer, List<Job>> searchByCrewIdGroupedByYear(Integer crewId, Integer pageNo, Integer size)
    		throws CrewNotFound {
    	if(!crewRepository.exists(crewId)) {
    		throw new CrewNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Job> jobs = crewInRepository.searchByCrewIdOrderByYearDesc(crewId, pageRequest);
    	Map<Integer, List<Job>> rolesByYear = jobs.getContent().stream().collect(
				Collectors.groupingBy(role -> {
					Calendar cal = Calendar.getInstance();
					cal.setTime(role.getMovie().getReleaseDate());
					return cal.get(Calendar.YEAR);
				}));
    	return new TreeMap<Integer, List<Job>>(rolesByYear);
    }
    
    @Override
    @Transactional
    public Page<Crew> searchByName(String name, Integer pageNo, Integer size) throws CrewNotFound {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "id");
    	Page<Crew> crews = crewRepository.findAll(CrewSpecifications.nameIsLike(name.toLowerCase()),
    			pageRequest);
        if(crews.getSize() <= 0) throw new CrewNotFound();
        crews.iterator().forEachRemaining(crew -> {
        	if(crew.getImages().size() > 0) {
        		Hibernate.initialize(crew.getImages().iterator().next());
        	}
        });
        return crews;
    }
}
