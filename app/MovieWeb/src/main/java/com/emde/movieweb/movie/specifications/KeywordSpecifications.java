/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

import com.emde.movieweb.movie.model.Keyword;

/**
 *
 * @author emde
 */
public class KeywordSpecifications {
    
    public static Specification<Keyword> nameIsLike(final String name) {
        return new Specification<Keyword>() {
            @Override
            public Predicate toPredicate(Root<Keyword> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(cb.lower(root.get("name")), "%" + name + "%");
            }
        };
    }
}
