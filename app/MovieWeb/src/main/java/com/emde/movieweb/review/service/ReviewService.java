/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.review.service;

import org.springframework.data.domain.Page;

import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.review.exception.ReviewAlreadyExists;
import com.emde.movieweb.review.exception.ReviewNotFound;
import com.emde.movieweb.review.model.Review;

/**
 *
 * @author emde
 */
public interface ReviewService {
    /* Review */
	public void importMovieReviews(Movie movie) throws Exception;
	public Review create(Review review) throws ReviewAlreadyExists;
	public Page<Review> findAll(Integer movieId, Integer pageNo, Integer size) throws MovieNotFound, ReviewNotFound;
}
