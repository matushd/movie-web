/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.personal.service;

import java.util.List;

import com.emde.movieweb.personal.exception.PersonalAlreadyExists;
import com.emde.movieweb.personal.exception.PersonalNotFound;
import com.emde.movieweb.personal.model.Personal;
import com.omertron.themoviedbapi.model.person.PersonInfo;

/**
 *
 * @author emde
 */

public interface PersonalService {
    /* Personal */
    public Personal create(Personal personal) throws PersonalAlreadyExists;
    public Personal create(PersonInfo person) throws PersonalAlreadyExists;
    public Personal delete(Integer id) throws PersonalNotFound;
    public List<Personal> findAll();
    public Personal update(Personal personal) throws PersonalNotFound;
    public Personal findById(Integer id);
}
