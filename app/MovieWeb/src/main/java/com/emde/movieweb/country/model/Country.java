/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.country.model;

import com.emde.movieweb.movie.model.Movie;

import java.beans.Transient;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.springframework.data.domain.Persistable;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "country")
public class Country implements Persistable<Integer> {
    
	@Version
	private static final long serialVersionUID = -971238871075769730L;

	@Id
	@SequenceGenerator(name = "countrySeq", sequenceName="country_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "countrySeq")
    @Column(name = "country_id")
    private Integer id;
    
    @Column(name = "country_name")
    private String name;
    
    @Column(name = "country_iso", unique = true)
    private String iso;
    
    @ManyToMany(mappedBy="countries",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Movie> movies;

    public Country() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

	@Override
	@Transient
	public boolean isNew() {
		return id == null;
	}
    
}
