/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.controller;

import com.emde.movieweb.actor.exception.ActorNotFound;
import com.emde.movieweb.actor.model.Role;
import com.emde.movieweb.actor.service.ActorService;
import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.crew.exception.CrewNotFound;
import com.emde.movieweb.crew.model.Job;
import com.emde.movieweb.crew.service.CrewService;
import com.emde.movieweb.image.model.Image;
import com.emde.movieweb.image.service.ImageService;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.ImportService;
import com.emde.movieweb.movie.service.MovieService;
import com.emde.movieweb.movie.service.impl.ImportServiceImpl;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.model.movie.MovieInfo;
import com.omertron.themoviedbapi.results.ResultList;

import dummiesmind.breadcrumb.springmvc.annotations.Link;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author emde
 */
//@RestController
@Controller
@RequestMapping(value="/movie")
public class MovieController {
	
	public static final Integer PACK_SIZE = 8;
    
    @Autowired
    private ImageService imageService;
    
    @Autowired
    private MovieService movieService;
    
    @Autowired
    private ImportService importService;
    
    @Autowired
    private ActorService actorService;
    
    @Autowired
    private CrewService crewService;
    
    @Autowired
    private TheMovieDbApi moviedbApi;
    
    private static Logger logger = LogManager.getLogger(MovieController.class.getName());
    
    /**
     * Movie backdrops rest action
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value="/backdrops/{movieId}", method=RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Image getBackdrops(Model model, @PathVariable(value="movieId") Integer id) {
        List<Image> res = imageService.findBackdrops(id, 1, 1);
        return res.iterator().next();
    }
    
    /**
     * Movie posters rest action
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value="/posters/{movieId}", method=RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Image getPosters(Model model, @PathVariable(value="movieId") Integer id) {
        List<Image> res = imageService.findPosters(id, 1, 1);
        return res.iterator().next();
    }
    
    /**
     * Movie page action
     * @param model
     * @param id
     * @return
     * @throws MovieDbException 
     * @throws MovieNotFound 
     * @throws ActorNotFound 
     * @throws CrewNotFound 
     */
    @Link(label="Film", family="", parent = "Filmy")
    @RequestMapping(value="/{movieId}", method=RequestMethod.GET)
    public String movie(Model model, @PathVariable(value="movieId") Integer id, HttpSession session,
    		UsernamePasswordAuthenticationToken user) throws MovieDbException, ActorNotFound, MovieNotFound, CrewNotFound {
    	Movie movie;
    	Page<Role> roles;
    	Page<Job> jobs;
    	
    	try {
    		movie = movieService.findFullById(id);
    	} catch(MovieNotFound ex) {
    		logger.info(ex.getMessage());
    		if(!importMovie(id)) {
        		return "common/error";
        	}
        	movie = movieService.findFullById(id);
    	}
        
        if(movie.getSimilarMovies().isEmpty()) {
        	importSimilarMovies(movie);
        } else {
        	supplement(movie);
        }
        
        roles = actorService.searchByMovieId(id, 1, ImportServiceImpl.actorLimit);
        jobs = crewService.searchByMovieId(id, 1, ImportServiceImpl.crewLimit);
        
        Breadcrump.setLastLabelName(session, movie.getTitle());
        model.addAttribute("backdrops", imageService.findBackdrops(id, 1, 50));
        model.addAttribute("roles", roles);
        model.addAttribute("jobs", jobs);
        model.addAttribute(movie);
        model.addAttribute("recommended", user != null ? movieService.recommendedForUser((String)user.getPrincipal(), 1, 5) : null);
        return "movie/detail";
    }
    
    /**
     * Movie list action
     * @param model
     * @return
     */
    @Link(label="Filmy", family="", parent = "MovieWeb")
    @RequestMapping(value="/all", method=RequestMethod.GET)
    public String movies(Model model) {
        model.addAttribute("movies", movieService.findAll(1, PACK_SIZE));
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/movie/all/");
        return "movie/all";
    }
    
    /**
     * Movie pageable list action
     * @param model
     * @return
     */
    @Link(label="Filmy", family="", parent = "MovieWeb")
    @RequestMapping(value="/all/{pageNo}", method=RequestMethod.GET)
    public String movies(Model model, @PathVariable(value="pageNo") Integer pageNo) {
        model.addAttribute("movies", movieService.findAll(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/movie/all/");
        return "movie/all";
    }
    
    /**
     * Movie popular list action
     * @param model
     * @return
     */
    @Link(label="Najpopularniejsze", family="", parent = "Filmy")
    @RequestMapping(value="/popular", method=RequestMethod.GET)
    public String moviesPopular(Model model) {
    	try {
    		movieService.importPopularMovies(1);
		} catch (MovieDbException | MovieNotFound e) {
			logger.warn(e.getMessage());
		}
        model.addAttribute("movies", movieService.findPopular(1, PACK_SIZE));
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/movie/popular/");
        return "movie/all";
    }
    
    /**
     * Movie popular pageable list action
     * @param model
     * @return
     */
    @Link(label="Najpopularniejsze", family="", parent = "Filmy")
    @RequestMapping(value="/popular/{pageNo}", method=RequestMethod.GET)
    public String moviesPopular(Model model, @PathVariable(value="pageNo") Integer pageNo) {
    	try {
			movieService.importPopularMovies(pageNo);
		} catch (MovieDbException | MovieNotFound e) {
			logger.warn(e.getMessage());
		}
        model.addAttribute("movies", movieService.findPopular(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/movie/popular/");
        return "movie/all";
    }
    
    /**
     * Movie rated list action
     * @param model
     * @return
     */
    @Link(label="Najlepiej oceniane", family="", parent = "Filmy")
    @RequestMapping(value="/rated", method=RequestMethod.GET)
    public String moviesRated(Model model) {
        model.addAttribute("movies", movieService.findBestRated(1, PACK_SIZE));
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/movie/rated/");
        return "movie/all";
    }
    
    /**
     * Movie rated pageable list action
     * @param model
     * @return
     */
    @Link(label="Najlepiej oceniane", family="", parent = "Filmy")
    @RequestMapping(value="/rated/{pageNo}", method=RequestMethod.GET)
    public String moviesRated(Model model, @PathVariable(value="pageNo") Integer pageNo) {
        model.addAttribute("movies", movieService.findBestRated(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/movie/rated/");
        return "movie/all";
    }
    
    /**
     * Movie rated list action
     * @param model
     * @return
     */
    @Link(label="Obecnie grane", family="", parent = "Filmy")
    @RequestMapping(value="/playing", method=RequestMethod.GET)
    public String moviesNowPlaying(Model model) {
    	try {
			movieService.importNowPlayingMovies(1);
		} catch (MovieDbException | MovieNotFound e) {
			logger.warn(e.getMessage());
		}
        model.addAttribute("movies", movieService.findNowPlaying(1, PACK_SIZE));
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/movie/playing/");
        return "movie/all";
    }
    
    /**
     * Movie rated pageable list action
     * @param model
     * @return
     */
    @Link(label="Obecnie grane", family="", parent = "Filmy")
    @RequestMapping(value="/playing/{pageNo}", method=RequestMethod.GET)
    public String moviesNowPlaying(Model model, @PathVariable(value="pageNo") Integer pageNo) {
        model.addAttribute("movies", movieService.findNowPlaying(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/movie/playing/");
        return "movie/all";
    }
    
    /**
     * Import movie by movie id
     * @param String data
     * @throws MovieDbException
     */
    private Boolean importMovie(Integer movieId) throws MovieDbException {
    	MovieInfo movie = moviedbApi.getMovieInfo(movieId, SearchController.locale, "");
        return movie != null ? importService.run(movie) : false;
    }
    
    /**
     * Import similar movies async
     * @param Movie movie
     * @throws MovieDbException
     * @throws MovieNotFound 
     */
    private void importSimilarMovies(Movie movie) throws MovieDbException, MovieNotFound {
    	ResultList<MovieInfo> list = moviedbApi.getSimilarMovies(movie.getId(), 1, SearchController.locale);
        logger.debug("Similar movies results size " + list.getResults().size() + " for movie: " + movie.getTitle());
        
        if(!list.getResults().isEmpty()) {
        	importService.runSimilarAsync(list, 0, SearchController.supplementLimit, movie);
        }
    }
    
    /**
     * Supplement async next similar movies from list
     * @param movies
     * @param movieInfos
     */
    private void supplement(Movie movie) {
    	ResultList<MovieInfo> movieInfos;
    	Integer i = 0;
		try {
			movieInfos = moviedbApi.getSimilarMovies(movie.getId(), 1, SearchController.locale);
	        for(MovieInfo movieInfo : movieInfos.getResults()) {
	            List<Movie> result = movie.getSimilarMovies().stream().filter(p -> p.getId()
	                    .equals(movieInfo.getId())).collect(Collectors.toList());
	            if(result.isEmpty()) {
	            	importService.runSimilar(movieInfo, movie);
	                i++;
	            }
	            if(i >= SearchController.supplementLimit) {
	                break;
	            }
	        }
		} catch (MovieDbException e) {
			logger.warn(e.getMessage());
		}
    }
}
