/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.company.service.impl;

import com.emde.movieweb.company.exception.CompanyAlreadyExists;
import com.emde.movieweb.company.exception.CompanyNotFound;
import com.emde.movieweb.company.model.Company;
import com.emde.movieweb.company.repository.CompanyRepository;
import com.emde.movieweb.company.service.CompanyService;
import com.emde.movieweb.company.specifications.CompanySpecifications;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    
    @Resource
    private CompanyRepository companyRepository;

    @Override
    @Transactional(rollbackFor=CompanyAlreadyExists.class)
    public Company create(Company company) throws CompanyAlreadyExists {
        if (companyRepository.exists(company.getId())) {
        	throw new CompanyAlreadyExists(company.getName());
        }
        return companyRepository.save(company);
    }

    @Override
    @Transactional(rollbackFor=CompanyNotFound.class)
    public Company delete(Integer id) throws CompanyNotFound {
        Company deleted = companyRepository.findOne(id);
        if (deleted == null) {
        	throw new CompanyNotFound();
        }
        companyRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public Page<Company> findAll(Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "id");
        return companyRepository.findAll(pageRequest);
    }

    @Override
    @Transactional(rollbackFor=CompanyNotFound.class)
    public Company update(Company company) throws CompanyNotFound {
    	if (!companyRepository.exists(company.getId())) {
    		throw new CompanyNotFound();
    	}
        return companyRepository.save(company);
    }

    @Override
    @Transactional
    public Company findById(Integer id) {
        return companyRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Page<Company> searchByName(String name, Integer pageNo, Integer size) throws CompanyNotFound {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "id");
    	Page<Company> companies = companyRepository.findAll(CompanySpecifications.nameIsLike(name),
    			pageRequest);
        if(companies.getSize() <= 0) throw new CompanyNotFound();
        return companies;
    }
}
