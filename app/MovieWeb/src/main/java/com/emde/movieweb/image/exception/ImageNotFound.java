/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.image.exception;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
public class ImageNotFound extends Exception {

	@Version
	private static final long serialVersionUID = 5154364464861948309L;
    
}
