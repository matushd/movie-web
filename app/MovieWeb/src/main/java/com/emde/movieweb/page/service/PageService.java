/**
 * 
 */
package com.emde.movieweb.page.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.emde.movieweb.page.exception.PageRankAlreadyExists;
import com.emde.movieweb.page.exception.PageRankNotFound;
import com.emde.movieweb.page.model.PageRank;

/**
 * @author emde
 *
 */
public interface PageService {
	public PageRank create(PageRank pagerank) throws PageRankAlreadyExists;
    public PageRank delete(Integer id) throws PageRankNotFound;
    public Page<PageRank> findAll(Integer pageNo, Integer size);
    public List<PageRank> findAllSortByDate();
    public PageRank update(PageRank pagerank) throws PageRankNotFound;
    public PageRank findById(Integer id);
}
