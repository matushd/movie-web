/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.company.model;

import com.emde.movieweb.movie.model.Movie;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "company")
public class Company implements Persistable<Integer> {
    
	private static final long serialVersionUID = -7992443035957961516L;

	@Id
    @Column(name = "company_id")
    private Integer id;
    
    @Column(name = "company_name")
    private String name;
    
    @ManyToMany(mappedBy="companies",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Movie> movies;

    public Company() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

	@Override
	public boolean isNew() {
		return id == null;
	}
}
