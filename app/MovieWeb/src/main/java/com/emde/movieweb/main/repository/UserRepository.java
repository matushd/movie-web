/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.main.repository;

import com.emde.movieweb.main.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface UserRepository extends JpaRepository<User, Long> {
    
    @Query("select u from User u where u.login = ?1")
    public User findByLogin(String username);
    
}
