/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.department.service.impl;

import com.emde.movieweb.department.exception.DepartmentAlreadyExists;
import com.emde.movieweb.department.exception.DepartmentNotFound;
import com.emde.movieweb.department.model.Department;
import com.emde.movieweb.department.repository.DepartmentRepository;
import com.emde.movieweb.department.service.DepartmentService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {
    
    @Resource
    private DepartmentRepository departmentRepository;

    @Override
    @Transactional(rollbackFor=DepartmentAlreadyExists.class)
    public Department create(Department department) throws DepartmentAlreadyExists {
        Department created = departmentRepository.findByName(department.getName());
        if (created != null) {
        	return created;
        }
        created = department;
        return departmentRepository.save(created);
    }

    @Override
    @Transactional(rollbackFor=DepartmentNotFound.class)
    public Department delete(Integer id) throws DepartmentNotFound {
        Department deleted = departmentRepository.findOne(id);
        if (deleted == null) {
        	throw new DepartmentNotFound();
        }
        departmentRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=DepartmentNotFound.class)
    public Department update(Department department) throws DepartmentNotFound {
        if(!departmentRepository.exists(department.getId())) {
        	throw new DepartmentNotFound();
        }
        return departmentRepository.save(department);
    }

    @Override
    @Transactional
    public Department findById(Integer id) {
        return departmentRepository.findOne(id);
    }
    
}
