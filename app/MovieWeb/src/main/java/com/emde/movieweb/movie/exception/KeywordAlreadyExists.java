package com.emde.movieweb.movie.exception;

import org.springframework.data.annotation.Version;

public class KeywordAlreadyExists extends Exception {
	
	@Version
	private static final long serialVersionUID = 5797674261787477172L;

	public KeywordAlreadyExists(String name) {
		super("Keyword '" + name + "' is already exist.");
	}
}
