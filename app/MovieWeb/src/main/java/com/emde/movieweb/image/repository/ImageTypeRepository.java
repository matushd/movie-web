/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.image.repository;

import com.emde.movieweb.image.model.ImageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface ImageTypeRepository extends JpaRepository<ImageType, Integer> {
    
	@Query("select it from ImageType it where it.name = ?1")
    public ImageType findByName(String name);
	
}
