/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.department.model;

import com.emde.movieweb.crew.model.Crew;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "department")
public class Department implements Serializable {
    
	@Version
	private static final long serialVersionUID = -8104126328146992744L;

	@Id
    @SequenceGenerator(name = "departmentSeq", sequenceName="department_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departmentSeq")
    @Column(name = "department_id")
    private Integer id;
    
    @Column(name = "department_name", unique = true)
    private String name;
    
    @OneToMany(mappedBy = "department")
    private Set<Crew> crews;

    public Department() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Crew> getCrews() {
        return crews;
    }

    public void setCrews(Set<Crew> crews) {
        this.crews = crews;
    }
    
}
