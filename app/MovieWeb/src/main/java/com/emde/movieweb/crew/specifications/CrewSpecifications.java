/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.crew.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

import com.emde.movieweb.crew.model.Crew;

/**
 *
 * @author emde
 */
public class CrewSpecifications {
    
    public static Specification<Crew> nameIsLike(final String name) {
        return new Specification<Crew>() {
            @Override
            public Predicate toPredicate(Root<Crew> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(cb.lower(root.get("name")), "%" + name + "%");
            }
        };
    }
}
