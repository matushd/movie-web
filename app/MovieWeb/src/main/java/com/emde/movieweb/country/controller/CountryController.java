/**
 * 
 */
package com.emde.movieweb.country.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.country.exception.CountryNotFound;
import com.emde.movieweb.country.service.CountryService;
import com.emde.movieweb.movie.controller.MovieController;
import com.emde.movieweb.movie.exception.KeywordNotFound;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.service.MovieService;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

/**
 * @author emde
 *
 */
@Controller
@RequestMapping(value="/country")
public class CountryController {
	
	private final Integer PACK_SIZE = 50;

	@Autowired
	private CountryService countryService;
	
	@Autowired
	private MovieService movieService;
	
	/**
     * Country list action
     * @param model
     * @return
     */
	@Link(label="Kraje", family="", parent = "MovieWeb")
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public String companies(Model model) {
        model.addAttribute("countries",countryService.findAll(1, PACK_SIZE));
        model.addAttribute("current", 1);
        return "country/list";
    }
	
	/**
     * Genre pageable list action
     * @param model
     * @return
     */
	@Link(label="Kraje", family="", parent = "MovieWeb")
    @RequestMapping(value="/list/{pageNo}", method=RequestMethod.GET)
    public String companies(Model model, @PathVariable(value="pageNo") Integer pageNo) {
        model.addAttribute("countries",countryService.findAll(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        return "country/list";
    }
    
    /**
     * Movie list by genre
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound 
     */
    @Link(label="Kraj", family="", parent = "Kraje")
    @RequestMapping(value="/{companyId}", method=RequestMethod.GET)
    public String company(Model model, @PathVariable(value="companyId") Integer id, HttpSession session)
    		throws MovieNotFound, CountryNotFound {
    	model.addAttribute("movies", movieService.searchByCountryId(id, 1, MovieController.PACK_SIZE));
    	model.addAttribute("current", 1);
    	model.addAttribute("baseUrl", "/country/" + id + "/");
    	String countryName = countryService.findById(id).getName();
    	Breadcrump.setLastLabelName(session, countryName);
    	model.addAttribute("name", countryName);
        return "movie/all";
    }
    
    /**
     * Movie pageable list by genre
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound 
     */
    @Link(label="Kraj", family="", parent = "Kraje")
    @RequestMapping(value="/{companyId}/{pageNo}", method=RequestMethod.GET)
    public String company(Model model, @PathVariable(value="companyId") Integer id, HttpSession session,
    		@PathVariable(value="pageNo") Integer pageNo) throws MovieNotFound, CountryNotFound {
    	model.addAttribute("movies", movieService.searchByCountryId(id, pageNo, MovieController.PACK_SIZE));
    	model.addAttribute("current", pageNo);
    	model.addAttribute("baseUrl", "/country/" + id + "/");
    	String countryName = countryService.findById(id).getName();
    	Breadcrump.setLastLabelName(session, countryName);
    	model.addAttribute("name", countryName);
        return "movie/all";
    }
}
