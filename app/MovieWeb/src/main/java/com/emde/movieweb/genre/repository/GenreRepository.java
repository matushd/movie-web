/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.genre.repository;

import com.emde.movieweb.genre.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface GenreRepository extends JpaRepository<Genre, Integer> {
    
	@Query("select g from Genre g where g.name = ?1")
    public Genre findByName(String name);
	
}
