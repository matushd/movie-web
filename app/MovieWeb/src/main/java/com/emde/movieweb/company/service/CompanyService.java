/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.company.service;

import com.emde.movieweb.company.exception.CompanyAlreadyExists;
import com.emde.movieweb.company.exception.CompanyNotFound;
import com.emde.movieweb.company.model.Company;
import org.springframework.data.domain.Page;

/**
 *
 * @author emde
 */
public interface CompanyService {
    public Company create(Company company) throws CompanyAlreadyExists;
    public Company delete(Integer id) throws CompanyNotFound;
    public Page<Company> findAll(Integer pageNo, Integer size);
    public Company update(Company company) throws CompanyNotFound;
    public Company findById(Integer id);
    public Page<Company> searchByName(String name, Integer pageNo, Integer size) throws CompanyNotFound;
}
