/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.review.specifications;

import com.emde.movieweb.review.model.Review;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author emde
 */
public class ReviewSpecifications {
    
    public static Specification<Review> movieIdIsEqual(final Integer movieId) {
    	return new Specification<Review>() {
    		@Override
            public Predicate toPredicate(Root<Review> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
    			Join<?, ?> movie = root.join("movie");
    			return movie.on(cb.equal(movie.get("id"), movieId)).getOn();
            }
    	};
    }
}
