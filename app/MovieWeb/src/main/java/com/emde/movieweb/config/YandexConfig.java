/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import com.omertron.themoviedbapi.MovieDbException;
import com.rmtheis.yandtran.translate.Translate;

/**
 *
 * @author emde
 */
@Configuration
@PropertySource("classpath:application.properties")
public class YandexConfig {
    
	@Resource
    private Environment environment;
    
	@Bean(name = "translateApi")
    public Translate getTranslateApi() throws MovieDbException {
		Translate translate = new Translate(environment.getProperty("yandex.api.key"));
		return translate;
    }
	
}
