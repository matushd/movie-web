/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.review.controller;

import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.review.exception.ReviewNotFound;
import com.emde.movieweb.review.model.Review;
import com.emde.movieweb.review.service.ReviewService;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author emde
 */
@Controller
@RequestMapping(value="/review")
public class ReviewController {
    
	public static final Integer PACK_SIZE = 1;
	
    private static Logger logger = LogManager.getLogger(ReviewController.class.getName());
    
    @Autowired
    private ReviewService reviewService;
    
    /**
     * Reviews list action
     * @param model
     * @param movieId
     * @param pageNo
     * @return
     * @throws MovieNotFound
     * @throws ReviewNotFound
     */
    @RequestMapping(value="/list/{movieId}/{pageNo}", method=RequestMethod.GET)
    public String reviews(Model model, @PathVariable(value="movieId") Integer movieId,
    		@PathVariable(value="pageNo") Integer pageNo) throws MovieNotFound, ReviewNotFound {
    	Movie movie = new Movie();
    	movie.setId(movieId);
    	Page<Review> reviews = reviewService.findAll(movieId, pageNo, PACK_SIZE);
    	if(reviews.getTotalElements() <= 0) {
        	try {
				reviewService.importMovieReviews(movie);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
        }
    	model.addAttribute("reviews", reviewService.findAll(movieId, pageNo, PACK_SIZE));
    	model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/review/list/" + movieId + "/");
    	return "review/detail";
    }
}
