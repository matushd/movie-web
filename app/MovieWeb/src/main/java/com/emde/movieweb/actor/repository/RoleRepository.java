/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.actor.repository;

import com.emde.movieweb.actor.model.Role;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    
	@Query("select r from Role r join r.actor a join r.movie m where m.id = ?1")
    public Page<Role> searchByMovieId(Integer movieId, Pageable page);
	
	@Query("select r from Role r join r.actor a join r.movie m where a.id = ?1 order by m.releaseDate desc")
    public Page<Role> searchByActorIdOrderByYearDesc(Integer actorId, Pageable page);
}
