/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.main.service.pgsql;

import com.emde.movieweb.main.exception.UserNotFound;
import com.emde.movieweb.main.exception.UsernameAlreadyInUseException;
import com.emde.movieweb.main.model.User;
import com.emde.movieweb.main.repository.UserRepository;
import com.emde.movieweb.main.service.UserService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;
    
    @Override
    @Transactional(rollbackFor=UsernameAlreadyInUseException.class)
    public User create(User user) {
        User createdUser = user;
        return userRepository.save(createdUser);
    }

    @Override
    @Transactional(rollbackFor=UserNotFound.class)
    public User delete(Long id) throws UserNotFound {
        User deletedUser = userRepository.findOne(id);
        if (deletedUser == null) {
        	throw new UserNotFound();
        }
        userRepository.delete(deletedUser);
        return deletedUser;
    }

    @Override
    @Transactional
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=UserNotFound.class)
    public User update(User user) throws UserNotFound {
    	if(!userRepository.exists(user.getId())) {
    		throw new UserNotFound();
    	}
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User findById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByLogin(String username) {
        return userRepository.findByLogin(username);
    }
    
}
