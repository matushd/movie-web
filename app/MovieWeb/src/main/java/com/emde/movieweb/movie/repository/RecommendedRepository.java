/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.repository;

import com.emde.movieweb.movie.model.Recommended;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author emde
 */
public interface RecommendedRepository extends JpaRepository<Recommended, Integer>, JpaSpecificationExecutor<Recommended> {
    
    
}
