/**
 * 
 */
package com.emde.movieweb.signin.interceptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ProviderSignInInterceptor;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.LikeOperations;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.WebRequest;

import com.emde.movieweb.signin.model.UserLikes;

/**
 * @author emde
 *
 */
public class SignInInterceptor implements ProviderSignInInterceptor<Facebook> {

	private static final Logger logger = LogManager.getLogger(SignInInterceptor.class.getName());
	
	@Override
	public void preSignIn(ConnectionFactory<Facebook> connectionFactory, MultiValueMap<String, String> parameters,
			WebRequest request) {
	}

	@Override
	public void postSignIn(Connection<Facebook> connection, WebRequest request) {
		LikeOperations likes = connection.getApi().likeOperations();
		UserLikes userLikes = new UserLikes();
		likes.getBooks().forEach(book -> userLikes.addBook(book.getName()));
		likes.getMovies().forEach(movie -> userLikes.addMovie(movie.getName()));
		likes.getMusic().forEach(music -> userLikes.addMusic(music.getName()));
		likes.getTelevision().forEach(tv -> userLikes.addTv(tv.getName()));
		userLikes.setSessionId(request.getSessionId());
		userLikes.setUserId(request.getUserPrincipal().getName());
		logger.debug(userLikes);
	}
}
