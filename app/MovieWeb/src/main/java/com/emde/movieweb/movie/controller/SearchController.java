/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.controller;

import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.ImportService;
import com.emde.movieweb.movie.service.MovieService;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.enumeration.SearchType;
import com.omertron.themoviedbapi.model.movie.MovieInfo;
import com.omertron.themoviedbapi.results.ResultList;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * Search controller
 * @author emde
 */
@Controller
public class SearchController {
    
	public static final String locale = "pl";
    
    /**
     * TODO: move to properties
     */
    public static final Integer supplementLimit = 3;
    
    private static Logger logger = LogManager.getLogger(SearchController.class.getName());
    
    @Autowired
    private TheMovieDbApi moviedbApi;
    
    @Autowired
    private ImportService importService;
    
    @Autowired
    private MovieService movieService;
    
    /**
     * Search movie action
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
    @RequestMapping(value="/search", method=RequestMethod.POST)
    public String search(NativeWebRequest request) throws MovieDbException, UnsupportedEncodingException {
        String data = request.getParameter("data").toLowerCase();
        try {
            return checkLocal(data);
        } catch(MovieNotFound ex) {
        	//continue
        } catch(MovieDbException e) {
            logger.error(e.getMessage(), e);
            return "common/error";
        }
        /**
         * If movie not locally exists - import it
         */
        try {
            importMovie(data);
        } catch(MovieDbException e) {
            logger.error(e.getMessage(), e);
            return "common/error";
        }

        return "redirect:/search/" + data;
    }
    
    /**
     * Search locally
     * @param model
     * @param movieName
     * @return
     * @throws UnsupportedEncodingException 
     */
    @Link(label="Wyszukiwanie", family="", parent = "Filmy")
    @RequestMapping(value="/search/{movieName}", method=RequestMethod.GET)
    public String search(Model model, @PathVariable(value="movieName") String movieName,
    		HttpSession session) throws UnsupportedEncodingException {
        Page<Movie> res = null;
        try {
            res = movieService.searchByTitle(movieName, 1, MovieController.PACK_SIZE);
        } catch (MovieNotFound e) {
            logger.warn(e.getMessage());
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + movieName);
    	model.addAttribute("name", movieName);
        model.addAttribute("movies", res);
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/search/" + movieName + "/");
        return "movie/all";
    }
    
    /**
     * Search locally pageable
     * @param model
     * @param movieName
     * @return
     * @throws UnsupportedEncodingException 
     */
    @Link(label="Wyszukiwanie", family="", parent = "Filmy")
    @RequestMapping(value="/search/{movieName}/{pageNo}", method=RequestMethod.GET)
    public String search(Model model, @PathVariable(value="movieName") String movieName,
    		@PathVariable(value="pageNo") Integer pageNo, HttpSession session)
    				throws UnsupportedEncodingException {
        Page<Movie> res = null;
        try {
            res = movieService.searchByTitle(movieName, pageNo, MovieController.PACK_SIZE);
        } catch (MovieNotFound e) {
            logger.warn(e.getMessage());
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + movieName);
    	model.addAttribute("name", movieName);
        model.addAttribute("movies", res);
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/search/" + movieName + "/");
        return "movie/all";
    }
    
    /**
     * Search local repository by string
     * @param data
     * @return String
     * @throws MovieNotFound
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
    private String checkLocal(String data) throws MovieNotFound, MovieDbException, UnsupportedEncodingException {
        List<Movie> localSearchMovies = movieService.searchByTitle(data);
        ResultList<MovieInfo> list = moviedbApi.searchMovie(data, 0, 
        		locale, Boolean.FALSE, 0, 0, SearchType.PHRASE);
        if(localSearchMovies.size() < list.getResults().size()) {
            supplement(localSearchMovies, list);
        }
        return "redirect:/search/" + data;
    }
    
    /**
     * Supplement async next movies from list
     * @param movies
     * @param movieInfos
     */
    private void supplement(List<Movie> movies, ResultList<MovieInfo> movieInfos) {
        Integer i = 0;
        for(MovieInfo movieInfo : movieInfos.getResults()) {
            List<Movie> result = movies.stream().filter(movie -> movie.getTitle().toLowerCase()
                    .equals(movieInfo.getTitle().toLowerCase())).collect(Collectors.toList());
            if(result.isEmpty()) {
            	importService.runAsync(movieInfo);
                i++;
            }
            if(i >= supplementLimit) {
                break;
            }
        }
    }
    
    /**
     * Import movie by title string
     * @param String data
     * @throws MovieDbException
     */
    private void importMovie(String data) throws MovieDbException {
        ResultList<MovieInfo> list = moviedbApi.searchMovie(data, 0, 
        		locale, Boolean.FALSE, 0, 0, SearchType.PHRASE);
        logger.debug("Results size " + list.getResults().size() + " for phrase " + data);
        if(!list.getResults().isEmpty()) {
        	importService.run(list.getResults().get(0));
        	if(list.getResults().size() > 1) {
        		importService.runAsync(list, 1, supplementLimit);
        	}
        }
    }
}
