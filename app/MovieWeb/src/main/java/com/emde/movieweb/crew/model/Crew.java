/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.crew.model;

import com.emde.movieweb.department.model.Department;
import com.emde.movieweb.image.model.Image;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "crew")
public class Crew implements Serializable {
    
	@Version
	private static final long serialVersionUID = -4795617593539724654L;

	@Id
    @Column(name = "crew_id")
    private Integer id;
    
    @Column(name = "crew_name")
    private String name;
    
    @JsonBackReference
    @OneToMany(mappedBy = "crew",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY)
    private Set<Job> jobs;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})  
    @JoinTable(name="crew_image", joinColumns=@JoinColumn(name="crew_id"), inverseJoinColumns=@JoinColumn(name="image_id"))  
    private Set<Image> images;
    
    @ManyToOne(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;
    
    @JsonIgnore
    @OneToOne(mappedBy = "crew",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY) 
    private Detail detail;

    public Crew() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Set<Job> jobs) {
        this.jobs = jobs;
    }

    /**
	 * @return the images
	 */
	public Set<Image> getImages() {
		return images;
	}
	
	public Image getImage() {
		return images.iterator().hasNext() ? images.iterator().next() : null;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}
    
}
