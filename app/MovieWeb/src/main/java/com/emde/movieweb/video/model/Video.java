/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video.model;

import com.emde.movieweb.movie.model.Movie;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "video")
public class Video implements Serializable {
    
	@Version
	private static final long serialVersionUID = -7738875061881983687L;

	@Id
    @SequenceGenerator(name = "videoSeq", sequenceName="video_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "videoSeq")
    @Column(name = "video_id")
    private Integer id;
    
    @Column(name = "video_name")
    private String name;
    
    @Column(name = "video_key", unique = true)
    private String key;
    
    @Column(name = "video_size")
    private Integer size;
    
    @ManyToOne
    @JoinColumn(name = "video_type_id")
    private VideoType type;
    
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public Video() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public VideoType getType() {
        return type;
    }

    public void setType(VideoType type) {
        this.type = type;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
    
}
