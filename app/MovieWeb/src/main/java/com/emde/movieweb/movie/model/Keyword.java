package com.emde.movieweb.movie.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "keyword")
public class Keyword {

	@Id
    @Column(name = "keyword_id")
	private Integer id;
	
	@Column(name="keyword_name")
	private String name;
	
	@ManyToMany(mappedBy="keywords",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Movie> movies;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}
	
}
