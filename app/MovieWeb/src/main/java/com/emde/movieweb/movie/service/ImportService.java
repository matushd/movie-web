/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.service;

import java.util.List;

import com.emde.movieweb.actor.exception.ActorNotFound;
import com.emde.movieweb.actor.model.Actor;
import com.emde.movieweb.crew.exception.CrewNotFound;
import com.emde.movieweb.crew.model.Crew;
import com.emde.movieweb.exception.EmptyCreditsException;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.model.artwork.Artwork;
import com.omertron.themoviedbapi.model.movie.MovieInfo;
import com.omertron.themoviedbapi.results.ResultList;

/**
 *
 * @author emde
 */

public interface ImportService {
    /**
     * Run import
     * @param movie
     * @return
     */
	public Boolean run(MovieInfo movie);
	
	/**
     * Run async import
     * @param list
     * @param skip
     * @param limit
     */
    public void runAsync(MovieInfo movie);
    
    /**
     * Run async import list
     * @param list
     * @param skip
     * @param limit
     */
    public void runAsync(ResultList<MovieInfo> list, Integer skip, Integer limit);
    
    /**
     * Run import
     * @param movie
     * @return
     */
	public void runSimilar(MovieInfo movie, Movie similar);
	
    /**
     * Run similar movie async import
     * @param list
     * @param skip
     * @param limit
     */
    public void runSimilarAsync(ResultList<MovieInfo> list, Integer skip, Integer limit, Movie similar) throws MovieNotFound;
    
    /**
     * Import actors for movie
     * @param movie
     * @throws MovieDbException
     * @throws EmptyCreditsException
     */
    public void importActors(Movie movie) throws MovieDbException, EmptyCreditsException;
    
    /**
     * Import crews for movie
     * @param movie
     * @throws MovieDbException
     * @throws EmptyCreditsException
     */
    public void importCrews(Movie movie) throws MovieDbException, EmptyCreditsException;
    
    /**
     * Import actor images
     * @param actor
     * @param image
     */
    public void importActorImage(Actor actor, List<Artwork> images) throws ActorNotFound;
    
    /**
     * Import crew images
     * @param crew
     * @param image
     */
    public void importCrewImages(Crew crew, List<Artwork> images) throws CrewNotFound;
}
