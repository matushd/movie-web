/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video.service;

import com.emde.movieweb.video.exception.VideoAlreadyExists;
import com.emde.movieweb.video.exception.VideoNotFound;
import com.emde.movieweb.video.exception.VideoTypeAlreadyExists;
import com.emde.movieweb.video.exception.VideoTypeNotFound;
import com.emde.movieweb.video.model.Video;
import com.emde.movieweb.video.model.VideoType;
import java.util.List;

/**
 *
 * @author emde
 */

public interface VideoService {
    /* Video */
    public Video create(Video video) throws VideoAlreadyExists;
    public Video delete(Integer id) throws VideoNotFound;
    public List<Video> findAll();
    public Video update(Video video) throws VideoNotFound;
    public Video findById(Integer id);
    
    /* VideoType */
    public VideoType createType(VideoType videoType) throws VideoTypeAlreadyExists;
    public VideoType deleteType(Integer id) throws VideoTypeNotFound;
    public List<VideoType> findAllTypes();
    public VideoType updateType(VideoType videoType) throws VideoTypeNotFound;
    public VideoType findTypeById(Integer id);
}
