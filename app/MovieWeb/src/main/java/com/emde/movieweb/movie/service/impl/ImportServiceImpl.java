/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.service.impl;

import com.emde.movieweb.actor.exception.ActorAlreadyExists;
import com.emde.movieweb.actor.exception.ActorNotFound;
import com.emde.movieweb.actor.exception.RoleAlreadyExists;
import com.emde.movieweb.actor.model.Actor;
import com.emde.movieweb.actor.model.Role;
import com.emde.movieweb.actor.service.ActorService;
import com.emde.movieweb.company.exception.CompanyAlreadyExists;
import com.emde.movieweb.company.model.Company;
import com.emde.movieweb.company.service.CompanyService;
import com.emde.movieweb.country.exception.CountryAlreadyExists;
import com.emde.movieweb.country.model.Country;
import com.emde.movieweb.country.service.CountryService;
import com.emde.movieweb.crew.exception.CrewAlreadyExists;
import com.emde.movieweb.crew.exception.CrewNotFound;
import com.emde.movieweb.crew.model.Crew;
import com.emde.movieweb.crew.model.Job;
import com.emde.movieweb.crew.service.CrewService;
import com.emde.movieweb.department.exception.DepartmentAlreadyExists;
import com.emde.movieweb.department.model.Department;
import com.emde.movieweb.department.service.DepartmentService;
import com.emde.movieweb.exception.EmptyCreditsException;
import com.emde.movieweb.exception.EmptyImagesException;
import com.emde.movieweb.genre.exception.GenreAlreadyExists;
import com.emde.movieweb.genre.model.Genre;
import com.emde.movieweb.genre.service.GenreService;
import com.emde.movieweb.image.exception.ImageAlreadyExists;
import com.emde.movieweb.image.exception.ImageTypeAlreadyExists;
import com.emde.movieweb.image.model.Image;
import com.emde.movieweb.image.model.ImageType;
import com.emde.movieweb.image.service.ImageService;
import com.emde.movieweb.movie.exception.KeywordAlreadyExists;
import com.emde.movieweb.movie.exception.MovieAlreadyExists;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Keyword;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.ImportService;
import com.emde.movieweb.movie.service.MovieService;
import com.emde.movieweb.video.exception.VideoAlreadyExists;
import com.emde.movieweb.video.exception.VideoTypeAlreadyExists;
import com.emde.movieweb.video.model.Video;
import com.emde.movieweb.video.model.VideoType;
import com.emde.movieweb.video.service.VideoService;
import com.google.common.base.Stopwatch;
import com.omertron.themoviedbapi.AppendToResponseBuilder;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.enumeration.MovieMethod;
import com.omertron.themoviedbapi.model.artwork.Artwork;
import com.omertron.themoviedbapi.model.credits.MediaCreditCast;
import com.omertron.themoviedbapi.model.credits.MediaCreditCrew;
import com.omertron.themoviedbapi.model.movie.MovieInfo;
import com.omertron.themoviedbapi.results.ResultList;
import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;

import java.sql.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class ImportServiceImpl implements ImportService {
    
	public static final Integer actorLimit = 12;
    
    public static final Integer crewLimit = 8;
    
    private static final Logger logger = LogManager.getLogger(ImportServiceImpl.class.getName());
    
    private static final String locale = "pl";
    
    private static final String localeMedia = "";
    
    @Autowired
    private MovieService movieService;
    
    @Autowired
    private VideoService videoService;
    
    @Autowired
    private ImageService imageService;
    
    @Autowired
    private GenreService genreService;
    
    @Autowired
    private DepartmentService departmentService;
    
    @Autowired
    private CrewService crewService;
    
    @Autowired
    private CountryService countryService;
    
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private ActorService actorService;
    
    @Autowired
    private TheMovieDbApi moviedbApi;
    
    @Autowired
    private Translate translateApi;
    
    @Override
    @Async
    public void runAsync(ResultList<MovieInfo> list, Integer skip, Integer limit) {
    	logger.info("Start async movie import!");
    	limit = limit < list.getResults().size() ? limit : list.getResults().size();
    	for(MovieInfo movie : list.getResults().subList(skip, limit) ) {
    		try {
                exec(movie);
            } catch(EmptyCreditsException | EmptyImagesException ex) {
            	logger.warn(ex.getMessage());
            } catch (Exception ex) {
            	logger.error("Import exec error! " + ex.getMessage());
            }
    	}
    	logger.info("Finish async movie import!");
    }
    
    @Override
    @Async
    public void runSimilarAsync(ResultList<MovieInfo> list, Integer skip, Integer limit, Movie similar)  throws MovieNotFound{
    	logger.info("Start async similar movie import!");
    	limit = limit < list.getResults().size() ? limit : list.getResults().size();
    	for(MovieInfo movie : list.getResults().subList(skip, limit) ) {
    		try {
				if(!movieService.exists(movie.getId())) {
					exec(movie);
				}
				similar.addSimilarMovie(movieService.findById(movie.getId()));
            } catch(EmptyCreditsException | EmptyImagesException ex) {
            	logger.warn(ex.getMessage());
            } catch (Exception ex) {
                logger.error("Import exec error! " + ex.getMessage());
            }
    	}
    	movieService.update(similar);
		movieService.flush();
		logger.info("Finish async similar movie import!");
    }
    
    @Override
    @Async
    public void runSimilar(MovieInfo movie, Movie similar) {
    	try {
			if(!movieService.exists(movie.getId())) {
				exec(movie);
			}
			similar.addSimilarMovie(movieService.findById(movie.getId()));
        } catch(EmptyCreditsException | EmptyImagesException ex) {
        	logger.warn(ex.getMessage());
        } catch (Exception ex) {
            logger.error("Import exec error! " + ex.getMessage());
        }
    }
    
    @Override
    public Boolean run(MovieInfo moviett) {
        Boolean res = false;
        try {
            res = exec(moviett);
        } catch (Exception ex) {
        	logger.error("Import exec error! " + ex.getMessage());
        }
        return res;
    }
    
    @Override
    @Async
    public void runAsync(MovieInfo moviett) {
        try {
            exec(moviett);
        } catch (Exception ex) {
        	logger.error("Import exec error! " + ex.getMessage());
        }
    }
    
    @Override
    public void importActors(Movie movie) throws MovieDbException, EmptyCreditsException {
    	MovieInfo moviett = getMovieInfo(movie.getId());
    	Integer limit = moviett.getCast().size() > actorLimit ? actorLimit : moviett.getCast().size();
    	Iterator<MediaCreditCast> actors = moviett.getCast().listIterator(limit);
        while(actors.hasNext()) {
            createActor(actors.next(), movie);
        }
    }
    
    @Override
    public void importCrews(Movie movie) throws MovieDbException, EmptyCreditsException {
    	MovieInfo moviett = getMovieInfo(movie.getId());
    	Integer limit = moviett.getCrew().size() > crewLimit ? crewLimit : moviett.getCrew().size();
    	Iterator<MediaCreditCrew> crews = moviett.getCrew().listIterator(limit);
        while(crews.hasNext()) {
            createCrew(crews.next(), movie);
        }
    }
    
    @Transactional
    @Override
    public void importActorImage(Actor actor, List<Artwork> images) throws ActorNotFound {
    	Set<Image> newImages = new HashSet<>();
    	images.iterator().forEachRemaining(image -> {
    		Image actorProfileImage = null;
    		try {
    			actorProfileImage = generateImage(image);
    			newImages.add(imageService.create(actorProfileImage));
    		} catch (ImageAlreadyExists ex) {
    			logger.debug("Image already exists!" + ex.getMessage());
    		}
    	});
    	actor.setImages(newImages);
    	actorService.update(actor);
	}
    
    @Transactional
    @Override
    public void importCrewImages(Crew crew, List<Artwork> images) throws CrewNotFound {
    	Set<Image> newImages = new HashSet<>();
    	images.iterator().forEachRemaining(image -> {
    		Image actorProfileImage = null;
    		try {
    			actorProfileImage = generateImage(image);
    			newImages.add(imageService.create(actorProfileImage));
    		} catch (ImageAlreadyExists ex) {
    			logger.debug("Image already exists!" + ex.getMessage());
    		}
    	});
    	crew.setImages(newImages);
    	crewService.update(crew);
    }
    
    /**
     * Execute import
     * @param moviett
     * @return
     * @throws Exception
     */
    private Boolean exec(MovieInfo moviett) throws Exception {
    	if(movieService.exists(moviett.getId())) {
    		throw new MovieAlreadyExists(moviett.getTitle());
    	}
        Stopwatch stopwatch = Stopwatch.createStarted();
        
        /* Init */
        Movie newMovie;
        Set<Genre> newGenres;
        Set<Keyword> newKeywords;
        Set<Country> newCountries;
        Set<Company> newCompanies;
        Set<Image> newImages;
        
        /* Get full movie info */
        MovieInfo movie;
        try {
        	/* Get movie info in polish */
        	String atr = new AppendToResponseBuilder(MovieMethod.CREDITS)
        			.add(MovieMethod.VIDEOS).add(MovieMethod.KEYWORDS).build();
            moviett = moviedbApi.getMovieInfo(moviett.getId(), locale, atr);
            if(moviett.getCast().size() <= 1 || moviett.getCrew().size() <= 1) {
                throw new EmptyCreditsException("Empty actors or crews!");
            }
            
            /* Get movie info in original language */
            atr = new AppendToResponseBuilder(MovieMethod.VIDEOS).add(MovieMethod.IMAGES).build();
            movie = moviedbApi.getMovieInfo(moviett.getId(), localeMedia, atr);
            if(movie.getImages().size() <= 0) {
            	throw new EmptyImagesException("Empty images!");
            }
            
            logger.info("Start movie import: " + movie.getTitle());
        } catch (MovieDbException ex) {
            logger.error("Cant get movie info!", ex);
            throw ex;
        }
        
        /* Genre */
        newGenres = createGenres(moviett);
        
        /* Keyword */
        newKeywords = createKeywords(moviett);
        
        /* Companies */
        newCompanies = createCompanies(moviett);
        
        /* Country */
        newCountries = createCountries(moviett);
        
        /* Image */
        newImages = createMovieImages(movie.getImages());
        
        /* Movie */
        newMovie = createMovie(moviett, newGenres, newCompanies, newCountries, newImages, newKeywords);
        
        /* Video */
        createVideos(moviett.getVideos().size() > 0 ? moviett.getVideos() : movie.getVideos(), newMovie);
        
        /* Actors */
        Integer limit = moviett.getCast().size() > actorLimit ? actorLimit : moviett.getCast().size();
        Iterator<MediaCreditCast> actors = moviett.getCast().subList(0, limit).iterator();
        while(actors.hasNext()) {
            createActor(actors.next(), newMovie);
            timeout(5);
        }
        
        /* Crew */
        limit = moviett.getCrew().size() > crewLimit ? crewLimit : moviett.getCrew().size();
        Iterator<MediaCreditCrew> crews = moviett.getCrew().subList(0, limit).iterator();
        while(crews.hasNext()) {
            createCrew(crews.next(), newMovie);
            timeout(5);
        }
        
        logger.info("End movie import: " + movie.getTitle() + " in time " + stopwatch.elapsed(TimeUnit.SECONDS));
        return true;
    }
    
    /**
     * Create movie
     * @param movie
     * @throws MovieAlreadyExists
     */
    @Transactional( rollbackFor=Exception.class )
    private Movie createMovie(MovieInfo movie, Set<Genre> newGenres, Set<Company> newCompanies,
    		Set<Country> newCountries, Set<Image> newImages, Set<Keyword> newKeywords) throws MovieAlreadyExists {
    	Movie newMovie = new Movie();
    	try {
        	newMovie.setId(movie.getId());
            newMovie.setDate(new Date(LocalDateTime.now().toDate().getTime()));
            newMovie.setReleaseDate(movie.getReleaseDate());
            newMovie.setOriginalTitle(movie.getOriginalTitle());
            newMovie.setOverview(movie.getOverview());
            newMovie.setTitle(movie.getTitle());
            newMovie.setVoteAvg(movie.getVoteAverage());
            newMovie.setVoteCount(movie.getVoteCount());
            newMovie.setBudget(new Double(movie.getBudget()));
            newMovie.setHomepage(movie.getHomepage());
            newMovie.setRuntime(movie.getRuntime());
            newMovie.setRevenue(new Double(movie.getRevenue()));
            newMovie.setOriginalLanguage(movie.getOriginalLanguage());
            /* Add movie to genre */
            newMovie.setGenres(newGenres);
            /* Add movie to keyword */
            newMovie.setKeywords(newKeywords);
            /* Add movie to company */
            newMovie.setCompanies(newCompanies);
            /* Add movie to country */
            newMovie.setCountries(newCountries);
            /* Add movie to images */
            newMovie.setImages(newImages);
            newMovie = movieService.create(newMovie);
        } catch (MovieAlreadyExists ex) {
            logger.error("Cant add new movie!");
            throw ex;
        }
    	return newMovie;
    }

    /**
     * Create movie genres
     * @param movie
     */
    @Transactional( rollbackFor=Exception.class )
    private Set<Genre> createGenres(MovieInfo movie) {
    	Set<Genre> newGenres = new HashSet<>();
        movie.getGenres().iterator().forEachRemaining(genre -> {
            Genre newGenre = new Genre();
            newGenre.setId(genre.getId());
            newGenre.setName(genre.getName());
            try {
            	newGenre = genreService.create(newGenre);
            } catch (GenreAlreadyExists ex) { 
                logger.debug(ex.getMessage());
            }
            newGenres.add(newGenre);
        });
        logger.debug("Added new genres:" + newGenres.size());
        return newGenres;
    }
    
    /**
     * Create movie keywords
     * @param movie
     */
    @Transactional( rollbackFor=Exception.class )
    private Set<Keyword> createKeywords(MovieInfo movie) {
    	Set<Keyword> newKeywords = new HashSet<>();
        movie.getKeywords().iterator().forEachRemaining(keyword -> {
        	Keyword newKeyword = new Keyword();
        	newKeyword.setId(keyword.getId());
            try {
            	newKeyword.setName(translateApi.execute(keyword.getName(), Language.ENGLISH, Language.POLISH));
            	newKeyword = movieService.create(newKeyword);
            	newKeywords.add(newKeyword);
            } catch(KeywordAlreadyExists ex) {
            	newKeywords.add(newKeyword);
            } catch (Exception ex) { 
                logger.debug(ex.getMessage());
            }
        });
        logger.debug("Added new keywords:" + newKeywords.size());
        return newKeywords;
    } 
    
    /**
     * Create movie companies
     * @param movie
     */
    @Transactional( rollbackFor=Exception.class )
    private Set<Company> createCompanies(MovieInfo movie) {
    	Set<Company> newCompanies = new HashSet<>();
        movie.getProductionCompanies().iterator().forEachRemaining(company -> {
            Company newCompany = new Company();
            newCompany.setId(company.getId());
            newCompany.setName(company.getName());
            try {
            	newCompany = companyService.create(newCompany);
                logger.debug("New company " + newCompany.getName());
            } catch (CompanyAlreadyExists ex) {
            	logger.debug(ex.getMessage());
            }
            newCompanies.add(newCompany);
        });
        logger.debug("Added new companies:" + newCompanies.size());
        return newCompanies;
    }
    
    /**
     * Create movie countries
     * @param movie
     */
    @Transactional( rollbackFor=Exception.class )
    private Set<Country> createCountries(MovieInfo movie) {
    	Set<Country> newCountries = new HashSet<>();
        movie.getProductionCountries().iterator().forEachRemaining(country -> {
            Country newCountry = new Country();
            newCountry.setIso(country.getCountry());
            try {
            	newCountry.setName(WordUtils.capitalize(translateApi.execute(country.getName(), Language.ENGLISH, Language.POLISH)));
            	newCountry = countryService.create(newCountry);
                logger.debug("New country " + newCountry.getName());
                newCountries.add(newCountry);
            } catch (CountryAlreadyExists ex) {
            	newCountries.add(newCountry);
            } catch (Exception ex) { 
                logger.debug(ex.getMessage());
            }
        });
        logger.debug("Added new countries:" + newCountries.size());
        return newCountries;
    }

    /**
     * Create movie videos
     * @param movieId
     * @throws MovieDbException
     */
    @Transactional( rollbackFor=Exception.class )
    private void createVideos(List<com.omertron.themoviedbapi.model.media.Video> videos, Movie newMovie) throws MovieDbException {
    	videos.iterator().forEachRemaining(video -> {
            VideoType videoType = new VideoType();
            videoType.setName(video.getType());
            try {
                videoType = videoService.createType(videoType);
                logger.debug("New video type " + videoType.getName());
            } catch (VideoTypeAlreadyExists ex) {
            	logger.debug(ex.getMessage());
            }

            try {
                Video newVideo = new Video();
                newVideo.setName(video.getName());
                newVideo.setKey(video.getKey());
                newVideo.setSize(video.getSize());
                newVideo.setType(videoType);
                newVideo.setMovie(newMovie);
                newVideo = videoService.create(newVideo);
                logger.debug("New video " + newVideo.getName());
            } catch (VideoAlreadyExists ex) {
            	logger.debug(ex.getMessage());
            }
        });
    }
    
    /**
     * Create movie images
     * @param movieId
     * @throws MovieDbException
     * @throws EmptyImagesException
     */
    @Transactional( rollbackFor=Exception.class )
    private Set<Image> createMovieImages(List<Artwork> images) throws MovieDbException, EmptyImagesException {
    	Set<Image> newImages = new HashSet<>();
    	logger.debug("Movie images results: " + images.size());
        images.iterator().forEachRemaining(image -> {
            try {
                Image img = generateImage(image);
                img = imageService.create(img);
                newImages.add(img);
            } catch (ImageAlreadyExists ex) {
            	logger.debug(ex.getMessage());
            }
        });
        if(newImages.size() <= 0) {
        	throw new EmptyImagesException("Images not inserted!");
        }
        logger.debug("New movie images size: " + newImages.size());
        return newImages;
    }
    
    /**
     * Create actor and related role
     * @param actor
     * @throws MovieDbException
     */
    @Transactional( rollbackFor=Exception.class )
    private void createActor(MediaCreditCast actor, Movie newMovie) throws MovieDbException {
    	
        /* Actor */
        Actor newActor = new Actor();
        newActor.setId(actor.getId());
        newActor.setName(actor.getName());
        try {
            /* Actor profile image */
            ResultList<Artwork> actorImages= moviedbApi.getPersonImages(actor.getId());
            if(!actorImages.isEmpty()) {
                Image actorProfileImage = generateImage(actorImages.getResults().get(0));
                imageService.create(actorProfileImage);
                Set<Image> newImages = new HashSet<>();
                newImages.add(imageService.create(actorProfileImage));
                newActor.setImages(newImages);
            }
            newActor = actorService.create(newActor);
            logger.debug("New actor " + newActor.getName());
        } catch (ActorAlreadyExists | ImageAlreadyExists ex) {
        	logger.debug("Actor or his image already exists!" + ex.getMessage());
        } catch (MovieDbException ex) {
            logger.warn("Cant get person images!", ex);
            throw ex;
        }

        /* Role */
        try {
            Role role = new Role();
            role.setCharacter(translateApi.execute(actor.getCharacter(), Language.ENGLISH, Language.POLISH));
            role.setId(actor.getCastId());
            role.setOrder(actor.getOrder());
            role.setProfilePath(actor.getArtworkPath());
            role.setActor(newActor);
            role.setMovie(newMovie);
            role = actorService.createRole(role);
        } catch (RoleAlreadyExists ex) {
            logger.debug("Role already exists!", ex);
        } catch (Exception e) {
        	logger.debug(e.getMessage(), e);
		}
    }
    
    /**
     * Create crew and related cast
     * @param crew
     * @throws MovieDbException
     */
    @Transactional( rollbackFor=Exception.class )
    private void createCrew(MediaCreditCrew crew, Movie newMovie) throws MovieDbException {
    	
        /* Department */
        Department department = new Department();
        department.setName(crew.getDepartment());
        try {
            department = departmentService.create(department);
            logger.debug("New department " + department.getName());
        } catch (DepartmentAlreadyExists ex) {
        	logger.debug(ex.getMessage());
        }

        /* Crew */
        Crew newCrew = new Crew();
        newCrew.setId(crew.getId());
        newCrew.setName(crew.getName());
        newCrew.setDepartment(department);
        try {
            /* Crew profile image */
            ResultList<Artwork> crewImages= moviedbApi.getPersonImages(crew.getId());
            if(!crewImages.isEmpty()) {
                Image crewProfileImage = generateImage(crewImages.getResults().get(0));
                Set<Image> newImages = new HashSet<>();
                newImages.add(imageService.create(crewProfileImage));
                newCrew.setImages(newImages);
            }
            newCrew = crewService.create(newCrew);
            logger.debug("New crew " + newCrew.getName());
        } catch (CrewAlreadyExists | ImageAlreadyExists ex) {
        	logger.debug(ex.getMessage());
        } catch (MovieDbException ex) {
            logger.warn("Cant get crew images!", ex);
            throw ex;
        }
        
        /* Job */
        try {
            Job job = new Job();
            job.setJob(translateApi.execute(crew.getJob(), Language.ENGLISH, Language.POLISH));
            job.setCrew(newCrew);
            job.setMovie(newMovie);
            job = crewService.createJob(job);
            logger.debug("New job " + job.getJob());
        } catch (Exception ex) {
        	logger.debug(ex.getMessage());
        }
    }
    
    private MovieInfo getMovieInfo(Integer movieId) throws MovieDbException, EmptyCreditsException {
    	String atr = new AppendToResponseBuilder(MovieMethod.CREDITS).build();
    	MovieInfo moviett = moviedbApi.getMovieInfo(movieId, locale, atr);
    	if(moviett.getCast().size() <= 1 || moviett.getCrew().size() <= 1) {
            throw new EmptyCreditsException("Empty actors or crews!");
        }
    	return moviett;
    }
    
    /**
     * Generate image
     * @param image
     * @return
     */
    private Image generateImage(Artwork image) {
        ImageType newImageType = new ImageType();
        newImageType.setName(image.getArtworkType().name());
        try {
            newImageType = imageService.createType(newImageType);
        } catch (ImageTypeAlreadyExists ex) {
        	logger.debug(ex.getMessage());
        }
        
        Image newImage = new Image();
        newImage.setFilePath(image.getFilePath());
        newImage.setHeight(image.getHeight());
        newImage.setWidth(image.getWidth());
        newImage.setHashId(image.getId());
        newImage.setType(newImageType);
        
        return newImage;
    }
    
    /**
     * Import timeout
     * @param time
     */
    private void timeout(Integer time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            logger.warn(ex.getMessage(), ex);
        }
    }
    
}
