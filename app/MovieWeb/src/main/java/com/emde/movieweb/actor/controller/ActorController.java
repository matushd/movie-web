package com.emde.movieweb.actor.controller;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.emde.movieweb.actor.exception.ActorNotFound;
import com.emde.movieweb.actor.model.Actor;
import com.emde.movieweb.actor.model.Detail;
import com.emde.movieweb.actor.model.Role;
import com.emde.movieweb.actor.service.ActorService;
import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.exception.EmptyCreditsException;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.ImportService;
import com.emde.movieweb.movie.service.MovieService;
import com.emde.movieweb.movie.service.impl.ImportServiceImpl;
import com.emde.movieweb.personal.service.PersonalService;
import com.google.common.base.Stopwatch;
import com.omertron.themoviedbapi.AppendToResponseBuilder;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.enumeration.PeopleMethod;
import com.omertron.themoviedbapi.model.person.PersonInfo;
import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@RequestMapping(value="/actor")
public class ActorController {
	
	public static final Integer PACK_SIZE = 16;
	
	@Autowired
    private ImportService importService;
	
	@Autowired
    private ActorService actorService;
	
	@Autowired
    private PersonalService personalService;
	
	@Autowired
    private MovieService movieService;
	
	@Autowired
    private TheMovieDbApi moviedbApi;
	
	@Autowired
    private Translate translateApi;
	
	private static final Logger logger = LogManager.getLogger(ActorController.class.getName());
	
	/**
	 * Actor detail action
	 * @param model
	 * @param id
	 * @param session
	 * @return
	 * @throws MovieDbException
	 * @throws ActorNotFound 
	 * @throws MovieNotFound 
	 */
	@Link(label="Aktor", family="", parent="Aktorzy")
	@RequestMapping(value="/{actorId}", method=RequestMethod.GET)
	public String actor(Model model, @PathVariable(value="actorId") Integer id, HttpSession session,
			UsernamePasswordAuthenticationToken user) throws MovieDbException, ActorNotFound, MovieNotFound {
		Actor actor = actorService.findFullById(id);
		if(actor == null) {
			return "common/error";
		}
		
		if(actor.getDetail() == null) {
			importDetails(actor);
		}
		
		Breadcrump.setLastLabelName(session, actor.getName());
		model.addAttribute(actor);
		model.addAttribute("rolesByYear", actorService.searchByActorIdGroupedByYear(id, 1, 20).descendingMap());
		model.addAttribute("recommended", user != null ? movieService.recommendedForUser((String)user.getPrincipal(), 1, 5) : null);
		return "actor/detail";
	}
	
	/**
	 * Actors pageable list action
	 * @param 
	 * @return
	 */
	@Link(label="Aktorzy", family="", parent = "MovieWeb")
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public String actors(Model model) {
		model.addAttribute("actors", actorService.findAll(1, PACK_SIZE));
		model.addAttribute("current", 1);
		model.addAttribute("baseUrl", "/actor/list/");
		return "actor/list";
	}
	
	/**
	 * Actors pageable list action
	 * @param model
	 * @return
	 */
	@Link(label="Aktorzy", family="", parent = "MovieWeb")
	@RequestMapping(value="/list/{pageNo}", method=RequestMethod.GET)
	public String actors(Model model, @PathVariable(value="pageNo") Integer pageNo) {
		model.addAttribute("actors", actorService.findAll(pageNo, PACK_SIZE));
		model.addAttribute("current", pageNo);
		model.addAttribute("baseUrl", "/actor/list/");
		return "actor/list";
	}
	
	/**
     * Search actor action
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieAktorzy", family="", parent = "Aktorzy")
	@RequestMapping(value="/search", method=RequestMethod.GET)
    public String search(Model model, NativeWebRequest request, HttpSession session)
    		throws UnsupportedEncodingException {
		String actorName = request.getParameter("data").toLowerCase();
        Page<Actor> res = null;
        try {
            res = actorService.searchByName(actorName, 1, PACK_SIZE);
        } catch (ActorNotFound e) {
            logger.warn(e.getMessage());
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + actorName);
        model.addAttribute("actors", res);
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/actor/search/" + actorName + "/");
        return "actor/list";
    }
	
	/**
     * Search actor action pageable
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieAktorzy", family="", parent = "Aktorzy")
	@RequestMapping(value="/search/{actorName}/{pageNo}", method=RequestMethod.GET)
    public String search(NativeWebRequest request, Model model, @PathVariable(value="actorName") String actorName,
    		@PathVariable(value="pageNo") Integer pageNo, HttpSession session)
    		throws UnsupportedEncodingException {
		Page<Actor> res = null;
        try {
            res = actorService.searchByName(actorName, pageNo, PACK_SIZE);
        } catch (ActorNotFound e) {
            logger.warn(e.getMessage());
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + actorName);
        model.addAttribute("actors", res);
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/actor/search/" + actorName + "/");
        return "actor/list";
    }
	
	/**
	 * Movie actors rest action
	 * @param model
	 * @param movieId
	 * @param actorId
	 * @return
	 * @throws EmptyCreditsException 
	 * @throws MovieDbException 
	 */
    @RequestMapping(value="/movie/{movieId}/{pageNo}", method=RequestMethod.GET)
    public String importByMovie(Model model, @PathVariable(value="movieId") Integer movieId,
    		@PathVariable(value="pageNo") Integer pageNo) throws MovieDbException, EmptyCreditsException {
		Page<Role> roles = null;
    	try {
    		roles = actorService.searchByMovieId(movieId, pageNo, ImportServiceImpl.actorLimit);
    		if(pageNo > 1 && roles.getTotalElements() <= ImportServiceImpl.actorLimit) {
        		Movie movie = movieService.findById(movieId);
        		importService.importActors(movie);
        		roles = actorService.searchByMovieId(movieId, pageNo, ImportServiceImpl.actorLimit);
    		}
		} catch (MovieNotFound | ActorNotFound e) {
			logger.warn(e.getMessage());
		}
    	
    	model.addAttribute("roles", roles.getContent());
    	return "actor/roles";
    }
	
	/**
	 * Import actor details
	 * @param actor
	 */
	private void importDetails(Actor actor) {
		try {
			Stopwatch stopwatch = Stopwatch.createStarted();
			String atr = new AppendToResponseBuilder(PeopleMethod.IMAGES).build();
			PersonInfo person = moviedbApi.getPersonInfo(actor.getId(), atr);
			if(person.getBiography() != null) {
				person.setBiography(translateApi.translate(person.getBiography(),
						Language.ENGLISH, Language.POLISH));
				Detail actorDetail = new Detail(personalService.create(person));
				actor.setDetail(actorDetail);
			}
			if(actor.getImages().size() < 2) {
				importService.importActorImage(actor, person.getImages());
			}
			logger.info("Actor detail import: " + actor.getName() + " in time " + stopwatch.elapsed(TimeUnit.SECONDS));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}
}
