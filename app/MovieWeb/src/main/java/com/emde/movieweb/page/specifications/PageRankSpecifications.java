/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.page.specifications;

import java.sql.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import com.emde.movieweb.page.model.PageRank;

/**
 *
 * @author emde
 */
public class PageRankSpecifications {
    
    public static Specification<PageRank> dateIsMax() {
        return new Specification<PageRank>() {
            @Override
            public Predicate toPredicate(Root<PageRank> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
            	Subquery<Date> sq = cq.subquery(Date.class);
                Root<PageRank> pagerank = sq.from(PageRank.class);
                sq.select(cb.greatest(pagerank.get("date")));
            	return cb.equal(root.get("date"), sq);
            }
        };
    }
}
