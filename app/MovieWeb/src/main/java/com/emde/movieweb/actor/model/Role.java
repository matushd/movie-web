/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.actor.model;

import com.emde.movieweb.image.model.Image;
import com.emde.movieweb.movie.model.Movie;
import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "role")
public class Role implements Serializable, Comparable<Role> {
    
	private static final long serialVersionUID = -802891392568223176L;

	@Id
	@SequenceGenerator(name = "roleSeq", sequenceName="role_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roleSeq")
    @Column(name = "role_id")
    private Integer id;
    
    @Column(name = "role_character")
    private String character;
            
    @Column(name = "role_profile_path")
    private String profilePath;
    
    @Column(name = "role_order")
    private Integer order;
    
    @ManyToOne
    @JoinColumn(name = "actor_id")
    private Actor actor;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public Role() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getProfilePath() {
        return profilePath;
    }
    
    public String getProfilePath(Integer width) {
        return Image.BASE_URL + width + profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    @Override
    public int compareTo(Role o) {
        return this.order.compareTo(o.getOrder());
    }
    
}
