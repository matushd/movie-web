/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.department.service;

import com.emde.movieweb.department.exception.DepartmentAlreadyExists;
import com.emde.movieweb.department.exception.DepartmentNotFound;
import com.emde.movieweb.department.model.Department;
import java.util.List;

/**
 *
 * @author emde
 */
public interface DepartmentService {
    public Department create(Department department) throws DepartmentAlreadyExists;
    public Department delete(Integer id) throws DepartmentNotFound;
    public List<Department> findAll();
    public Department update(Department department) throws DepartmentNotFound;
    public Department findById(Integer id);
}
