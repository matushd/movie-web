package com.emde.movieweb.review.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Version;

import com.emde.movieweb.movie.model.Movie;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author emde
 */
@Entity
@Table(name="review")
public class Review implements Serializable {	

	@Version
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "review_id")
	private String id;
	
	@Column(name = "review_author")
	private String author;
	
	@Column(name = "review_content")
	private String content;
	
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
}
