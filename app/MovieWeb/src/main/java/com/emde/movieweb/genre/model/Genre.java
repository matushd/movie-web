/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.genre.model;

import com.emde.movieweb.movie.model.Movie;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.springframework.data.domain.Persistable;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "genre")
public class Genre implements Persistable<Integer> {
    
    @Version
	private static final long serialVersionUID = -6833746263965953177L;

	@Id
    @Column(name = "genre_id")
    private Integer id;
    
    @Column(name = "genre_name")
    private String name;
    
    @ManyToMany(mappedBy="genres",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})
    private Set<Movie> movies;

    public Genre() {
    }
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

	@Override
	public boolean isNew() {
		return id == null;
	}
    
}
