/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.crew.service;

import com.emde.movieweb.crew.exception.CrewAlreadyExists;
import com.emde.movieweb.crew.exception.CrewNotFound;
import com.emde.movieweb.crew.exception.CrewInAlreadyExists;
import com.emde.movieweb.crew.exception.CrewInNotFound;
import com.emde.movieweb.crew.model.Crew;
import com.emde.movieweb.crew.model.Job;
import com.emde.movieweb.movie.exception.MovieNotFound;

import java.util.List;
import java.util.TreeMap;

import org.springframework.data.domain.Page;

/**
 *
 * @author emde
 */

public interface CrewService {
    /* Crew */
    public Crew create(Crew crew) throws CrewAlreadyExists;
    public Crew delete(Integer id) throws CrewNotFound;
    public Page<Crew> findAll(Integer pageNo, Integer size);
    public Crew update(Crew crew) throws CrewNotFound;
    public Crew findById(Integer id);
    public Crew findFullById(Integer id);
    public Page<Crew> searchByName(String name, Integer pageNo, Integer size) throws CrewNotFound;
    
    /* CrewIn */
    public Job createJob(Job crewIn) throws CrewInAlreadyExists;
    public Job deleteJob(Integer id) throws CrewInNotFound;
    public List<Job> findAllJob();
    public Job updateJob(Job crewIn) throws CrewInNotFound;
    public Job findJobById(Integer id);
    public Page<Job> searchByMovieId(Integer movieId, Integer pageNo, Integer size)
    		throws MovieNotFound, CrewNotFound;
    public TreeMap<Integer, List<Job>> searchByCrewIdGroupedByYear(Integer crewId, Integer pageNo, Integer size)
    		throws CrewNotFound;
}
