/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.main.model;

/**
 *
 * @author emde
 */
import java.io.Serializable;
import javax.persistence.Column;
 
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.Version;
 
@Entity
@Table(name = "appuser")
public class User implements Serializable {
    
	@Version
	private static final long serialVersionUID = -2631930800140017907L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "login")
    private String login;
    
    public User() {

    };

    public User(String login) {
        this.login = login;
    };

    public Long getId() {
    	return id;
    }

    public void setId(Long id) {
    	this.id = id;
    }

    public String getLogin() {
    	return login;
    }

    public void setLogin(String login) {
    	this.login = login;
    }
    
}