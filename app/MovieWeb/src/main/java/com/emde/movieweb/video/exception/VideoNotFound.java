/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video.exception;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
public class VideoNotFound extends Exception {

	@Version
	private static final long serialVersionUID = -1578885703836898461L;
    
}
