/**
 * 
 */
package com.emde.movieweb.navigation.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.emde.movieweb.genre.service.GenreService;

/**
 * @author emde
 *
 */
public class NavigationDataInterceptor extends HandlerInterceptorAdapter {
	
	//private static Logger logger = LogManager.getLogger(NavigationDataInterceptor.class.getName());
	
	@Autowired
	private GenreService genreService;
	
	@Override
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
		modelAndView.getModel().put("genres", genreService.findAllSortByName());
	}
}
