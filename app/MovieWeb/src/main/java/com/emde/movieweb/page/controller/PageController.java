/**
 * 
 */
package com.emde.movieweb.page.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emde.movieweb.page.service.PageService;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

/**
 * @author emde
 *
 */
@Controller
@RequestMapping(value="/page")
public class PageController {
	
	public static final Integer PACK_SIZE = 20;

	@Autowired
	private PageService pageService;
	
	@Link(label="Ranking stron", family="", parent="MovieWeb")
	@RequestMapping(value="/rank", method=RequestMethod.GET)
    public String rank(Model model) {
		model.addAttribute("pages", pageService.findAll(1, PACK_SIZE));
		model.addAttribute("current", 1);
		model.addAttribute("baseUrl", "/page/rank/");
		return "page/rankList";
	}
	
	@Link(label="Ranking stron", family="", parent="MovieWeb")
	@RequestMapping(value="/rank/{pageNo}", method=RequestMethod.GET)
    public String rank(Model model, @PathVariable(value="pageNo") Integer pageNo) {
		model.addAttribute("pages", pageService.findAll(pageNo, PACK_SIZE));
		model.addAttribute("current", pageNo);
		model.addAttribute("baseUrl", "/page/rank/");
		return "page/rankList";
	}
}
