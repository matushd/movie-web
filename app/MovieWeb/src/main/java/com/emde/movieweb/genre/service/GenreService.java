/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.genre.service;

import com.emde.movieweb.genre.exception.GenreAlreadyExists;
import com.emde.movieweb.genre.exception.GenreNotFound;
import com.emde.movieweb.genre.model.Genre;
import java.util.List;

/**
 *
 * @author emde
 */
public interface GenreService {
    public Genre create(Genre genre) throws GenreAlreadyExists;
    public Genre delete(Integer id) throws GenreNotFound;
    public List<Genre> findAll();
    public List<Genre> findAllSortByName();
    public Genre update(Genre genre) throws GenreNotFound;
    public Genre findById(Integer id);
}
