/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emde.movieweb.image.exception;

import org.springframework.data.annotation.Version;

public class ImageTypeAlreadyExists extends Exception {
	
	@Version
	private static final long serialVersionUID = 5933810834377954862L;

	public ImageTypeAlreadyExists(String name) {
		super("Image type '" + name + "' is already exist.");
	}
	
}
