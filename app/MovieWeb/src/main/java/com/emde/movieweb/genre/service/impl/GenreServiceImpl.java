/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.genre.service.impl;

import com.emde.movieweb.genre.exception.GenreAlreadyExists;
import com.emde.movieweb.genre.exception.GenreNotFound;
import com.emde.movieweb.genre.model.Genre;
import com.emde.movieweb.genre.repository.GenreRepository;
import com.emde.movieweb.genre.service.GenreService;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class GenreServiceImpl implements GenreService {
    
    @Resource
    private GenreRepository genreRepository;

    @Override
    @Transactional(rollbackFor=GenreAlreadyExists.class)
    public Genre create(Genre genre) throws GenreAlreadyExists {
    	if (genreRepository.exists(genre.getId())) {
    		throw new GenreAlreadyExists(genre.getName());
    	}
        return genreRepository.save(genre);
    }

    @Override
    @Transactional(rollbackFor=GenreNotFound.class)
    public Genre delete(Integer id) throws GenreNotFound {
        Genre deleted = genreRepository.findOne(id);
        if (deleted == null) {
        	throw new GenreNotFound();
        }
        genreRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    @Cacheable(value="appCache", key="{#root.class.getName(), #root.methodName}")
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=GenreNotFound.class)
    public Genre update(Genre genre) throws GenreNotFound {
    	if (!genreRepository.exists(genre.getId())) {
    		throw new GenreNotFound();
    	}
        return genreRepository.save(genre);
    }

    @Override
    @Transactional
    public Genre findById(Integer id) {
        return genreRepository.findOne(id);
    }
    
    @Override
    @Transactional
    @Cacheable(value="appCache", key="{#root.class.getName(), #root.methodName}")
    public List<Genre> findAllSortByName() {
        return genreRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }
    
}
