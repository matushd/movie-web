/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.genre.exception;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
public class GenreNotFound extends Exception {

	@Version
	private static final long serialVersionUID = -3012595379682818093L;
    
}
