/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.main.controller;

import com.emde.movieweb.main.service.UserService;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.MovieService;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
    
    private static final Logger logger = LogManager.getLogger(MainController.class.getName());
    
    private final Provider<ConnectionRepository> connectionRepositoryProvider;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MovieService movieService;
    
    @Inject
    public MainController(Provider<ConnectionRepository> connectionRepositoryProvider) {
    	this.connectionRepositoryProvider = connectionRepositoryProvider;
    }
    
    /**
     * Main site
     * @param map
     * @return
     */
    @Link(label="MovieWeb", family="", parent = "")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {
       List<Movie> sliderMovies;
       try {
           sliderMovies = movieService.randomWithImage(5);
       } catch (MovieNotFound ex) {
           logger.warn(ex.getMessage(), ex);
           sliderMovies = null;
       }
       map.put("sliderMovies", sliderMovies);
       return "index";
    }
}
