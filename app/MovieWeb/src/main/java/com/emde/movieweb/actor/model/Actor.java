/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.actor.model;

import com.emde.movieweb.image.model.Image;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "actor")
public class Actor implements Serializable {
    
	private static final long serialVersionUID = -195316204369139080L;

	@Id
    @Column(name = "actor_id")
    private Integer id;
    
    @Column(name = "actor_name")
    private String name;
    
    @JsonBackReference
    @OneToMany(mappedBy = "actor",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY)
    private Set<Role> roles;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})  
    @JoinTable(name="actor_image", joinColumns=@JoinColumn(name="actor_id"), inverseJoinColumns=@JoinColumn(name="image_id"))  
    private Set<Image> images;
    
    @JsonIgnore
    @OneToOne(mappedBy = "actor",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY) 
    private Detail detail;

    public Actor() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

	/**
	 * @return the images
	 */
	public Set<Image> getImages() {
		return images;
	}
	
	public Image getImage() {
		return images.iterator().hasNext() ? images.iterator().next() : null;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}
    
}
