/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.image.service.impl;

import com.emde.movieweb.image.exception.ImageAlreadyExists;
import com.emde.movieweb.image.exception.ImageNotFound;
import com.emde.movieweb.image.exception.ImageTypeAlreadyExists;
import com.emde.movieweb.image.exception.ImageTypeNotFound;
import com.emde.movieweb.image.model.Image;
import com.emde.movieweb.image.model.ImageType;
import com.emde.movieweb.image.repository.ImageRepository;
import com.emde.movieweb.image.repository.ImageTypeRepository;
import com.emde.movieweb.image.service.ImageService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class ImageServiceImpl implements ImageService {

    @Resource
    private ImageRepository imageRepository;
    
    @Resource
    private ImageTypeRepository imageTypeRepository;
    
    @Override
    @Transactional(rollbackFor=ImageAlreadyExists.class)
    public Image create(Image image) throws ImageAlreadyExists {
        Image created = imageRepository.findByHashId(image.getHashId());
        if (created != null) {
        	return created;
        }
        return imageRepository.save(image);
    }

    @Override
    @Transactional(rollbackFor=ImageNotFound.class)
    public Image delete(Integer id) throws ImageNotFound {
        Image deleted = imageRepository.findOne(id);
        if (deleted == null) {
        	throw new ImageNotFound();
        }
        imageRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<Image> findAll() {
        return imageRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=ImageNotFound.class)
    public Image update(Image image) throws ImageNotFound {
    	if(!imageRepository.exists(image.getId())) {
    		throw new ImageNotFound();
    	}
        return imageRepository.save(image);
    }

    @Override
    @Transactional
    public Image findById(Integer id) {
        return imageRepository.findOne(id);
    }

    @Override
    @Transactional(rollbackFor=ImageTypeAlreadyExists.class)
    public ImageType createType(ImageType imageType) throws ImageTypeAlreadyExists {
        ImageType created = imageTypeRepository.findByName(imageType.getName());
        if (created != null) {
        	return created;
        }
        return imageTypeRepository.save(imageType);
    }

    @Override
    @Transactional(rollbackFor=ImageTypeNotFound.class)
    public ImageType deleteType(Integer id) throws ImageTypeNotFound {
        ImageType deleted = imageTypeRepository.findOne(id);
        if (deleted == null) {
        	throw new ImageTypeNotFound();
        }
        imageTypeRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<ImageType> findAllTypes() {
        return imageTypeRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=ImageTypeNotFound.class)
    public ImageType updateType(ImageType imageType) throws ImageTypeNotFound {
    	if (!imageTypeRepository.exists(imageType.getId())) {
    		throw new ImageTypeNotFound();
    	}
        return imageTypeRepository.save(imageType);
    }

    @Override
    @Transactional
    public ImageType findTypeById(Integer id) {
        return imageTypeRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public List<Image> findBackdrops(Integer movieId, Integer pageNo, Integer size) {
        PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "t.name");
        return imageRepository.getBackdrops(movieId, pageRequest);
    }
    
    @Override
    @Transactional
    public List<Image> findPosters(Integer movieId, Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
        return imageRepository.getPosters(movieId, pageRequest);
    }
}
