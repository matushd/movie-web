/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.emde.movieweb.navigation.interceptor.NavigationDataInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import dummiesmind.breadcrumb.springmvc.interceptor.BreadCrumbInterceptor;
 
/**
*
* @author emde
*/
@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan("com.emde.movieweb")
public class Config extends WebMvcConfigurerAdapter {
	
	@Bean
	public NavigationDataInterceptor navigationDataInterceptor() {
		return new NavigationDataInterceptor();
	}
	
	@Bean
	public MappingJackson2HttpMessageConverter jacksonMessageConverter(){
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Hibernate5Module());
        messageConverter.setObjectMapper(mapper);
        return messageConverter;
    }
    
	/**
	 * {@inheritDoc}
	 */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (!registry.hasMappingForPattern("/webjars/**")) {
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        }
        if (!registry.hasMappingForPattern("/resources/**")) {
            registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resources/");
        }
    }
    
    /**
	 * {@inheritDoc}
	 */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    
    /**
	 * {@inheritDoc}
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new BreadCrumbInterceptor()).excludePathPatterns(
				"/movie/backdrops/*", 
				"/movie/posters/*",
				"/event/*",
				"/photo/*",
				"/*/photo/*");
		registry.addInterceptor(navigationDataInterceptor()).excludePathPatterns(
				"/movie/backdrops/*", 
				"/movie/posters/*",
				"/event/*",
				"/photo/*",
				"/*/photo/*");
	}

	/**
	 * {@inheritDoc}
	 */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jacksonMessageConverter());
        super.configureMessageConverters(converters);
    }
}  