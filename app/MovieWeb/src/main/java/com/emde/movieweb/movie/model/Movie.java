/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.springframework.data.domain.Persistable;

import com.emde.movieweb.actor.model.Role;
import com.emde.movieweb.company.model.Company;
import com.emde.movieweb.country.model.Country;
import com.emde.movieweb.crew.model.Job;
import com.emde.movieweb.genre.model.Genre;
import com.emde.movieweb.image.model.Image;
import com.emde.movieweb.review.model.Review;
import com.emde.movieweb.video.model.Video;
import com.google.common.base.Strings;

/**
 *
 * @author emde
 */
@Entity
@Table(name = "movie")
public class Movie implements Persistable<Integer> {
    
    @Version
	private static final long serialVersionUID = 5362618397028536118L;

	@Id
    @Column(name = "movie_id")
    private Integer id;
    
    @Column(name = "movie_title", unique = true)
    private String title;
    
    @Column(name = "movie_vote_avg")
    private Double voteAvg;
    
    @Column(name = "movie_vote_count")
    private Integer voteCount;
    
    @Column(name = "movie_date")
    private Date date;
    
    @Column(name = "movie_original_title")
    private String originalTitle;
    
    @Column(name = "movie_overview")
    private String overview;
    
    @Column(name = "movie_release_date")
    private Date releaseDate;
    
    @Column(name = "movie_homepage")
    private String homepage;
    
    @Column(name = "movie_budget")
    private Double budget;
    
    @Column(name = "movie_revenue")
    private Double revenue;
    
    @Column(name = "movie_runtime")
    private Integer runtime;
    
    @Column(name = "movie_original_lang")
    private String originalLanguage;
    
    @OneToMany(mappedBy = "movie")
    private Set<Role> actors;
    
    @OneToMany(mappedBy = "movie")
    private Set<Job> crews;
    
    @OneToMany(mappedBy = "movie")
    private Set<Video> videos;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH}, 
    		fetch = FetchType.EAGER)  
    @JoinTable(name="movie_genre", 
    	joinColumns=@JoinColumn(name="movie_id"), 
    	inverseJoinColumns=@JoinColumn(name="genre_id"))  
    private Set<Genre> genres;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})  
    @JoinTable(name="movie_country", joinColumns=@JoinColumn(name="movie_id"), inverseJoinColumns=@JoinColumn(name="country_id"))  
    private Set<Country> countries;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})  
    @JoinTable(name="movie_company", joinColumns=@JoinColumn(name="movie_id"), inverseJoinColumns=@JoinColumn(name="company_id"))  
    private Set<Company> companies;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH})  
    @JoinTable(name="movie_image", joinColumns=@JoinColumn(name="movie_id"), inverseJoinColumns=@JoinColumn(name="image_id"))  
    private Set<Image> images;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH}, 
    		fetch = FetchType.LAZY)
    @JoinTable(name="movie_similar",
        joinColumns=@JoinColumn(name="movie_id"),
        inverseJoinColumns=@JoinColumn(name="movie_similar_id"))
    private Set<Movie> similarMovies;
    
    @ManyToMany(mappedBy="similarMovies", 
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY)
    private Set<Movie> similarMoviesTo;
    
    @OneToMany(mappedBy = "movie",
    		cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH},
    		fetch = FetchType.LAZY)
    private Set<Review> reviews;
    
    @ManyToMany(cascade={CascadeType.REFRESH,CascadeType.PERSIST,CascadeType.DETACH}, 
    		fetch = FetchType.EAGER)  
    @JoinTable(name="movie_keyword", 
    	joinColumns=@JoinColumn(name="movie_id"), 
    	inverseJoinColumns=@JoinColumn(name="keyword_id"))  
    private Set<Keyword> keywords;
    
    @OneToMany(mappedBy = "movie")
    private Set<Recommended> recommended;

    public Movie() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getVoteAvg() {
        return voteAvg;
    }

    public void setVoteAvg(Double voteAvg) {
        this.voteAvg = voteAvg;
    }
    
    public void setVoteAvg(float voteAvg) {
        this.voteAvg = (double) voteAvg;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public Set<Country> getCountries() {
        return countries;
    }

    public Set<Company> getCompanies() {
        return companies;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public void setDate(String date) {
    	if(!Strings.isNullOrEmpty(date)) {
    		this.date = Date.valueOf(date);
    	}
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
    
    public void setReleaseDate(String releaseDate) {
    	if(!Strings.isNullOrEmpty(releaseDate)) {
    		this.releaseDate = Date.valueOf(releaseDate);
    	}
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }

    public void setCompanies(Set<Company> companies) {
        this.companies = companies;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<Role> getActors() {
        return actors;
    }

    public void setActors(Set<Role> actors) {
        this.actors = actors;
    }

    public Set<Job> getCrews() {
        return crews;
    }

    public void setCrews(Set<Job> crews) {
        this.crews = crews;
    }

    public Set<Video> getVideos() {
        return videos;
    }

    public void setVideos(Set<Video> videos) {
        this.videos = videos;
    }
    
    public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public Double getBudget() {
		return budget;
	}

	public void setBudget(Double budget) {
		this.budget = budget;
	}

	public Integer getRuntime() {
		return runtime;
	}

	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
	}

	public Double getRevenue() {
		return revenue;
	}

	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}

	public String getOriginalLanguage() {
		return originalLanguage;
	}

	public void setOriginalLanguage(String originalLanguage) {
		this.originalLanguage = originalLanguage;
	}

	public Set<Movie> getSimilarMovies() {
		Set<Movie> similar = new HashSet<Movie>();
		similar.addAll(this.similarMovies);
		similar.addAll(this.similarMoviesTo);
		return similar;
	}

	public void setSimilarMovies(Set<Movie> similarMovies) {
		this.similarMovies = similarMovies;
	}
	
	public void setSimilarMoviesTo(Set<Movie> similarMovies) {
		this.similarMoviesTo = similarMovies;
	}
	
	public void addSimilarMovie(Movie movie) {
		this.similarMovies.add(movie);
	}

	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public Set<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(Set<Keyword> keywords) {
		this.keywords = keywords;
	}

	@Override
	public boolean isNew() {
		return id == null;
	}
    
}
