/**
 * 
 */
package com.emde.movieweb.page.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.common.base.Strings;

/**
 * @author emde
 *
 */
@Entity
@Table(name = "pagerank")
public class PageRank {

	@Id
    @Column(name = "pagerank_id")
    private Integer id;
	
	@Column(name = "pagerank_url")
    private String url;
	
	@Column(name = "pagerank_value")
    private String value;
	
	@Column(name = "pagerank_title")
    private String title;
	
	@Column(name = "pagerank_date")
    private Date date;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
    	if(!Strings.isNullOrEmpty(date)) {
    		this.date = Date.valueOf(date);
    	}
    }

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
