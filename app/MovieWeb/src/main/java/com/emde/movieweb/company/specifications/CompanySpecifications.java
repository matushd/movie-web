/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.company.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

import com.emde.movieweb.company.model.Company;

/**
 *
 * @author emde
 */
public class CompanySpecifications {
    
    public static Specification<Company> nameIsLike(final String name) {
        return new Specification<Company>() {
            @Override
            public Predicate toPredicate(Root<Company> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(cb.lower(root.get("name")), "%" + name + "%");
            }
        };
    }
}
