/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.department.repository;

import com.emde.movieweb.department.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
    
	@Query("select d from Department d where d.name = ?1")
    public Department findByName(String name);
	
}
