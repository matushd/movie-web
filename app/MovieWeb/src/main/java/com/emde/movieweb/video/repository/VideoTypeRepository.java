/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video.repository;

import com.emde.movieweb.video.model.VideoType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface VideoTypeRepository extends JpaRepository<VideoType, Integer> {
    
	@Query("select vt from VideoType vt where vt.name = ?1")
    public VideoType findByName(String name);
	
}
