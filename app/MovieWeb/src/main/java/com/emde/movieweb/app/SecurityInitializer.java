/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.app;

import com.emde.movieweb.config.SecurityConfig;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author emde
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
    
    public SecurityInitializer() {
        super(SecurityConfig .class);
    }
    
}
