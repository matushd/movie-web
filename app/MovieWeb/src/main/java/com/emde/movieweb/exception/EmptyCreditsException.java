/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.exception;

import org.springframework.data.annotation.Version;

/**
 *
 * @author emde
 */
public class EmptyCreditsException extends Exception {
    
	@Version
	private static final long serialVersionUID = -5915677105327240912L;

	public EmptyCreditsException() {
        super();
    }
    
    public EmptyCreditsException(String message) {
        super(message);
    }

    public EmptyCreditsException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyCreditsException(Throwable cause) {
        super(cause);
    }

    protected EmptyCreditsException(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
