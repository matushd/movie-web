/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.actor.service.impl;

import com.emde.movieweb.actor.exception.ActorAlreadyExists;
import com.emde.movieweb.actor.exception.ActorNotFound;
import com.emde.movieweb.actor.exception.RoleAlreadyExists;
import com.emde.movieweb.actor.exception.RoleNotFound;
import com.emde.movieweb.actor.model.Actor;
import com.emde.movieweb.actor.model.Role;
import com.emde.movieweb.actor.repository.ActorRepository;
import com.emde.movieweb.actor.repository.RoleRepository;
import com.emde.movieweb.actor.service.ActorService;
import com.emde.movieweb.actor.specifications.ActorSpecifications;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.repository.MovieRepository;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class ActorServiceImpl implements ActorService {

    @Resource
    private ActorRepository actorRepository;
    
    @Resource
    private RoleRepository roleRepository;
    
    @Resource
    private MovieRepository movieRepository;
    
    @Override
    @Transactional(rollbackFor=ActorAlreadyExists.class)
    public Actor create(Actor actor) throws ActorAlreadyExists {
    	if(actorRepository.exists(actor.getId())) {
    		throw new ActorAlreadyExists(actor.getId());
    	}
        return actorRepository.save(actor);
    }

    @Override
    @Transactional(rollbackFor=ActorNotFound.class)
    public Actor delete(Integer id) throws ActorNotFound {
        Actor deleted = actorRepository.findOne(id);
        if (deleted == null) {
        	throw new ActorNotFound();
        }
        actorRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public Page<Actor> findAll(Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "id");
    	Page<Actor> actors = actorRepository.findAll(pageRequest);
    	actors.iterator().forEachRemaining(actor -> {
    		Hibernate.initialize(actor.getImage());
    	});
    	return actors;
    }

    @Override
    @Transactional(rollbackFor=ActorNotFound.class)
    public Actor update(Actor actor) throws ActorNotFound {
    	if(!actorRepository.exists(actor.getId())) {
    		throw new ActorNotFound();
    	}
        return actorRepository.save(actor);
    }

    @Transactional
    @Override
    public Actor findById(Integer id) {
        return actorRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Actor findFullById(Integer id) {
    	Actor actor = actorRepository.findOne(id);
        Hibernate.initialize(actor.getImages());
        return actor;
    }

    @Override
    @Transactional(rollbackFor=RoleAlreadyExists.class)
    public Role createRole(Role role) throws RoleAlreadyExists {
    	if (roleRepository.exists(role.getId())) {
    		throw new RoleAlreadyExists(role.getId());
    	}
        return roleRepository.save(role);
    }

    @Override
    @Transactional(rollbackFor=RoleNotFound.class)
    public Role deleteRole(Integer id) throws RoleNotFound {
        Role deleted = roleRepository.findOne(id);
        if (deleted == null) {
        	throw new RoleNotFound();
        }
        roleRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<Role> findAllRoles() {
        return roleRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=RoleNotFound.class)
    public Role updateRole(Role role) throws RoleNotFound {
    	if (!roleRepository.exists(role.getId())) {
    		throw new RoleNotFound();
    	}
        return roleRepository.save(role);
    }

    @Override
    @Transactional
    public Role findRoleById(Integer id) {
        return roleRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Page<Role> searchByMovieId(Integer movieId, Integer pageNo, Integer size)
    		throws MovieNotFound, ActorNotFound {
    	if(!movieRepository.exists(movieId)) {
    		throw new MovieNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Role> roles = roleRepository.searchByMovieId(movieId, pageRequest);
    	if(roles.getSize() <= 0) {
    		throw new ActorNotFound();
    	}
    	return roles;
    }
    
    @Override
    @Transactional
    public TreeMap<Integer, List<Role>> searchByActorIdGroupedByYear(Integer actorId, Integer pageNo, Integer size)
    		throws ActorNotFound {
    	if(!actorRepository.exists(actorId)) {
    		throw new ActorNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Role> roles = roleRepository.searchByActorIdOrderByYearDesc(actorId, pageRequest);
    	Map<Integer, List<Role>> rolesByYear = roles.getContent().stream().collect(
				Collectors.groupingBy(role -> {
					Calendar cal = Calendar.getInstance();
					cal.setTime(role.getMovie().getReleaseDate());
					return cal.get(Calendar.YEAR);
				}));
    	return new TreeMap<Integer, List<Role>>(rolesByYear);
    }
    
    @Override
    @Transactional
    public Page<Actor> searchByName(String name, Integer pageNo, Integer size) throws ActorNotFound {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "id");
    	Page<Actor> actors = actorRepository.findAll(ActorSpecifications.nameIsLike(name.toLowerCase()),
    			pageRequest);
        if(actors.getSize() <= 0) throw new ActorNotFound();
        actors.iterator().forEachRemaining(actor -> {
        	if(actor.getImages().size() > 0) {
        		Hibernate.initialize(actor.getImages().iterator().next());
        	}
        });
        return actors;
    }
}
