/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.repository;

import com.emde.movieweb.movie.model.Movie;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface MovieRepository extends JpaRepository<Movie, Integer>, JpaSpecificationExecutor<Movie> {
    
    @Query("select m from Movie m where m.title = ?1")
    public Movie findByIso(String iso);
    
    @Query("select m from Movie m join m.recommended r where r.userId = ?1")
    public Page<Movie> findRecommended(String userId, Pageable page);
}
