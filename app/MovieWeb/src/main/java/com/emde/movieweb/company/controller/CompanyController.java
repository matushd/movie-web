package com.emde.movieweb.company.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.company.exception.CompanyNotFound;
import com.emde.movieweb.company.model.Company;
import com.emde.movieweb.company.service.CompanyService;
import com.emde.movieweb.movie.controller.MovieController;
import com.emde.movieweb.movie.exception.KeywordNotFound;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.service.MovieService;
import com.omertron.themoviedbapi.MovieDbException;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@RequestMapping(value="/company")
public class CompanyController {
	
	private final Integer PACK_SIZE = 50;

	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private MovieService movieService;
	
	/**
     * Genre list action
     * @param model
     * @return
     */
	@Link(label="Firmy", family="", parent = "MovieWeb")
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public String companies(Model model) {
        model.addAttribute("companies",companyService.findAll(1, PACK_SIZE));
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/company/list/");
        return "company/list";
    }
	
	/**
     * Genre pageable list action
     * @param model
     * @return
     */
	@Link(label="Firmy", family="", parent = "MovieWeb")
    @RequestMapping(value="/list/{pageNo}", method=RequestMethod.GET)
    public String companies(Model model, @PathVariable(value="pageNo") Integer pageNo) {
        model.addAttribute("companies",companyService.findAll(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/company/list/");
        return "company/list";
    }
	
	/**
     * Search company action
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieFirmy", family="", parent = "Firmy")
	@RequestMapping(value="/search", method=RequestMethod.GET)
    public String search(Model model, NativeWebRequest request, HttpSession session)
    		throws UnsupportedEncodingException {
		String companyName = request.getParameter("data").toLowerCase();
        Page<Company> res = null;
        try {
            res = companyService.searchByName(companyName, 1, PACK_SIZE);
        } catch (CompanyNotFound e) {
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + companyName);
        model.addAttribute("companies", res);
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/company/search/" + companyName + "/");
        return "company/list";
    }
	
	/**
     * Search company action pageable
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieFirmy", family="", parent = "Firmy")
	@RequestMapping(value="/search/{companyName}/{pageNo}", method=RequestMethod.GET)
    public String search(NativeWebRequest request, Model model, @PathVariable(value="companyName") String companyName,
    		@PathVariable(value="pageNo") Integer pageNo, HttpSession session)
    		throws UnsupportedEncodingException {
		Page<Company> res = null;
        try {
            res = companyService.searchByName(companyName, pageNo, PACK_SIZE);
        } catch (CompanyNotFound e) {
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + companyName);
        model.addAttribute("companies", res);
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/company/search/" + companyName + "/");
        return "company/list";
    }
    
    /**
     * Movie list by genre
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound 
     */
    @Link(label="Firma", family="", parent = "Firmy")
    @RequestMapping(value="/{companyId}", method=RequestMethod.GET)
    public String company(Model model, @PathVariable(value="companyId") Integer id, HttpSession session)
    		throws MovieNotFound, CompanyNotFound {
    	model.addAttribute("movies", movieService.searchByCompanyId(id, 1, MovieController.PACK_SIZE));
    	model.addAttribute("current", 1);
    	model.addAttribute("baseUrl", "/company/" + id + "/");
    	String companyName = companyService.findById(id).getName();
    	model.addAttribute("name", companyName);
    	Breadcrump.setLastLabelName(session, companyName);
        return "movie/all";
    }
    
    /**
     * Movie pageable list by genre
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound 
     */
    @Link(label="Firma", family="", parent = "Firmy")
    @RequestMapping(value="/{companyId}/{pageNo}", method=RequestMethod.GET)
    public String company(Model model, @PathVariable(value="companyId") Integer id, HttpSession session,
    		@PathVariable(value="pageNo") Integer pageNo) throws MovieNotFound, CompanyNotFound {
    	model.addAttribute("movies", movieService.searchByCompanyId(id, pageNo, MovieController.PACK_SIZE));
    	model.addAttribute("current", pageNo);
    	model.addAttribute("baseUrl", "/company/" + id + "/");
    	String companyName = companyService.findById(id).getName();
    	model.addAttribute("name", companyName);
    	Breadcrump.setLastLabelName(session, companyName);
        return "movie/all";
    }
}
