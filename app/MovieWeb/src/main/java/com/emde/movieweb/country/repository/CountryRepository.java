/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.country.repository;

import com.emde.movieweb.country.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface CountryRepository extends JpaRepository<Country, Integer> {
    
    @Query("select c from Country c where c.iso = ?1")
    public Country findByIso(String iso);
}
