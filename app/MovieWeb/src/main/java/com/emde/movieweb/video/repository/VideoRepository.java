/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video.repository;

import com.emde.movieweb.video.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface VideoRepository extends JpaRepository<Video, Integer> {
    
	@Query("select v from Video v where v.key = ?1")
    public Video findByName(String key);
	
}
