/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.main.service;

import com.emde.movieweb.main.exception.UserNotFound;
import com.emde.movieweb.main.exception.UsernameAlreadyInUseException;
import com.emde.movieweb.main.model.User;
import java.util.List;

/**
 *
 * @author emde
 */
public interface UserService {
    public User create(User user) throws UsernameAlreadyInUseException;
    public User delete(Long id) throws UserNotFound;
    public List<User> findAll();
    public User update(User user) throws UserNotFound;
    public User findById(Long id);
    public User findByLogin(String username);
}
