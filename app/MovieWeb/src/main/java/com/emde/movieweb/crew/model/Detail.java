package com.emde.movieweb.crew.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import com.emde.movieweb.personal.model.Personal;

/**
 *
 * @author emde
 */
@Entity(name="CrewDetail")
@DiscriminatorValue(value="00")  
public class Detail extends Personal {	

	@OneToOne
    @JoinColumn(name = "personal_id")
	private Crew crew;
	
	public Detail(Personal personal) {
		super(personal);
	}
	
	public Detail() {
		super();
	}

	public Crew getCrew() {
		return crew;
	}

	public void setCrew(Crew crew) {
		this.crew = crew;
	}
	
}
