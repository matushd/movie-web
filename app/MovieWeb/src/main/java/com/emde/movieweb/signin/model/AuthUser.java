/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.signin.model;

/**
 *
 * @author emde
 */
public class AuthUser {
    
    private final String userId;
    
    private final String profileImageUrl;
    
    private final String displayName;
    
    private AuthUser(AuthUserBuilder builder) {
        this.userId = builder.userId;
        this.profileImageUrl = builder.profileImageUrl;
        this.displayName = builder.displayName;
    }
    
    public String getUserId() {
        return userId;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getDisplayName() {
        return displayName;
    }
    
    public static class AuthUserBuilder {

        private String userId;
        private String profileImageUrl;
        private String displayName;
        
        public AuthUserBuilder (String userId) {
            this.userId = userId;
        }
        
        public AuthUserBuilder profileImageUrl(String profileImageUrl) {
            this.profileImageUrl = profileImageUrl;
            return this;
        }
        
        public AuthUserBuilder displayName(String displayName) {
            this.displayName = displayName;
            return this;
        }
        
        public AuthUser build() {
            return new AuthUser(this);
        }
        
    }
}
