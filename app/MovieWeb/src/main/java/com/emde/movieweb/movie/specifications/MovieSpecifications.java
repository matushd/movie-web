/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.specifications;

import com.emde.movieweb.movie.model.Movie;

import java.sql.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author emde
 */
public class MovieSpecifications {
    
    public static Specification<Movie> titleIsEqual(final String titlle) {
        return new Specification<Movie>() {
            @Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.equal(root.get("title"), titlle);
            }
        };
    }
    
    public static Specification<Movie> titleIsLike(final String titlle) {
        return new Specification<Movie>() {
            @Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(cb.lower(root.get("title")), "%" + titlle + "%");
            }
        };
    }
    
    public static Specification<Movie> releaseIsBeetween(final Date from, final Date to) {
        return new Specification<Movie>() {
            @Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.between(root.<Date>get("releaseDate"), from, to);
            }
        };
    }
    
    public static Specification<Movie> hasImage() {
        return new Specification<Movie>() {
            @Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.createQuery().having(cb.greaterThan(cb.count(root.get("images")), new Long(0))).getRestriction();
            }
        };
    }
    
    public static Specification<Movie> genreIdIsEqual(final Integer genreId) {
    	return new Specification<Movie>() {
    		@Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
    			Join<?, ?> genres = root.join("genres");
    			return genres.on(cb.equal(genres.get("id"), genreId)).getOn();
            }
    	};
    }
    
    public static Specification<Movie> keywordIdIsEqual(final Integer keywordId) {
    	return new Specification<Movie>() {
    		@Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
    			Join<?, ?> keywords = root.join("keywords");
    			return keywords.on(cb.equal(keywords.get("id"), keywordId)).getOn();
            }
    	};
    }
    
    public static Specification<Movie> companyIdIsEqual(final Integer companyId) {
    	return new Specification<Movie>() {
    		@Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
    			Join<?, ?> companies = root.join("companies");
    			return companies.on(cb.equal(companies.get("id"), companyId)).getOn();
            }
    	};
    }
    
    public static Specification<Movie> countryIdIsEqual(final Integer countryId) {
    	return new Specification<Movie>() {
    		@Override
            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
    			Join<?, ?> countries = root.join("countries");
    			return countries.on(cb.equal(countries.get("id"), countryId)).getOn();
            }
    	};
    }
}
