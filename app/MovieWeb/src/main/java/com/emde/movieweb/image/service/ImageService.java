/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.image.service;

import com.emde.movieweb.image.exception.ImageAlreadyExists;
import com.emde.movieweb.image.exception.ImageNotFound;
import com.emde.movieweb.image.exception.ImageTypeAlreadyExists;
import com.emde.movieweb.image.exception.ImageTypeNotFound;
import com.emde.movieweb.image.model.Image;
import com.emde.movieweb.image.model.ImageType;
import java.util.List;

/**
 *
 * @author emde
 */

public interface ImageService {
    /* Image */
    public Image create(Image image) throws ImageAlreadyExists;
    public Image delete(Integer id) throws ImageNotFound;
    public List<Image> findAll();
    public Image update(Image image) throws ImageNotFound;
    public Image findById(Integer id);
    public List<Image> findBackdrops(Integer movieId, Integer pageNo, Integer size);
    public List<Image> findPosters(Integer movieId, Integer pageNo, Integer size);
    
    /* ImageType */
    public ImageType createType(ImageType imageType) throws ImageTypeAlreadyExists;
    public ImageType deleteType(Integer id) throws ImageTypeNotFound;
    public List<ImageType> findAllTypes();
    public ImageType updateType(ImageType imageType) throws ImageTypeNotFound;
    public ImageType findTypeById(Integer id);
}
