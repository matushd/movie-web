/**
 * 
 */
package com.emde.movieweb.page.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emde.movieweb.page.exception.PageRankAlreadyExists;
import com.emde.movieweb.page.exception.PageRankNotFound;
import com.emde.movieweb.page.model.PageRank;
import com.emde.movieweb.page.repository.PageRankRepostiory;
import com.emde.movieweb.page.service.PageService;
import com.emde.movieweb.page.specifications.PageRankSpecifications;

/**
 * @author emde
 *
 */
@Service
public class PageServiceImpl implements PageService {

	@Resource
    private PageRankRepostiory pagerankRepository;
	
	@Override
	@Transactional
	public PageRank create(PageRank pagerank) throws PageRankAlreadyExists {
		if (pagerankRepository.exists(pagerank.getId())) {
    		throw new PageRankAlreadyExists(pagerank.getId());
    	}
        return pagerankRepository.save(pagerank);
	}

	@Override
	@Transactional
	public PageRank delete(Integer id) throws PageRankNotFound {
		PageRank deleted = pagerankRepository.findOne(id);
        if (deleted == null) {
        	throw new PageRankNotFound();
        }
        pagerankRepository.delete(deleted);
        return deleted;
	}

	@Override
	@Transactional
	public Page<PageRank> findAll(Integer pageNo, Integer size) {
		PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.DESC, "value");
		return pagerankRepository.findAll(PageRankSpecifications.dateIsMax(), pageRequest);
	}

	@Override
	@Transactional
	public List<PageRank> findAllSortByDate() {
		return null;
	}

	@Override
	@Transactional
	public PageRank update(PageRank pagerank) throws PageRankNotFound {
		if (!pagerankRepository.exists(pagerank.getId())) {
    		throw new PageRankNotFound();
    	}
        return pagerankRepository.save(pagerank);
	}

	@Override
	@Transactional
	public PageRank findById(Integer id) {
		return pagerankRepository.findOne(id);
	}
}
