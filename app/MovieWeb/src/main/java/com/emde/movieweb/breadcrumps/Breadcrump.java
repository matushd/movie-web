/**
 * 
 */
package com.emde.movieweb.breadcrumps;

import java.util.LinkedList;

import javax.servlet.http.HttpSession;

import dummiesmind.breadcrumb.springmvc.breadcrumb.BreadCrumbLink;

/**
 * @author emde
 *
 */
public class Breadcrump {
	/**
	 * Set lest breadcrump label
	 * @param session
	 * @param name
	 */
	public static void setLastLabelName(HttpSession session, String name) {
		@SuppressWarnings("unchecked")
		LinkedList<BreadCrumbLink> link = (LinkedList<BreadCrumbLink>) session.getAttribute("currentBreadCrumb");
		link.getLast().setLabel(name);
	}
	
	/**
	 * Add breadcrump
	 * @param session
	 * @param name
	 */
	public static void addChild(HttpSession session, String name) {
		@SuppressWarnings("unchecked")
		LinkedList<BreadCrumbLink> link = (LinkedList<BreadCrumbLink>) session.getAttribute("currentBreadCrumb");
		BreadCrumbLink breadcrump = new BreadCrumbLink(link.getLast().getFamily(),
				name, true, link.getLast().getLabel());
		link.getLast().setCurrentPage(false);
		link.add(breadcrump);
	}
}
