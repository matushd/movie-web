/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.crew.repository;

import com.emde.movieweb.crew.model.Job;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface JobRepository extends JpaRepository<Job, Integer> {
    
	@Query("select j from Job j join j.crew c join j.movie m where m.id = ?1")
    public Page<Job> searchByMovieId(Integer movieId, Pageable page);
	
	@Query("select j from Job j join j.crew c join j.movie m where c.id = ?1 order by m.releaseDate desc")
    public Page<Job> searchByCrewIdOrderByYearDesc(Integer crewId, Pageable page);
}
