/**
 * 
 */
package com.emde.movieweb.signin.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * @author emde
 *
 */
public class UserLikes {

	private List<String> books;
	
	private List<String> music;
	
	private List<String> movies;
	
	private List<String> tv;
	
	private String sessionId;
	
	private String userId;
	
	public UserLikes() {
		books = new ArrayList<String>();
		music = new ArrayList<String>();
		movies = new ArrayList<String>();
		tv = new ArrayList<String>();
	}
	
	public void addBook(String name) {
		books.add(name);
	}
	
	public void addMusic(String name) {
		music.add(name);
	}
	
	public void addMovie(String name) {
		movies.add(name);
	}
	
	public void addTv(String name) {
		tv.add(name);
	}
	
	/**
	 * @return the books
	 */
	public List<String> getBooks() {
		return books;
	}

	/**
	 * @param books the books to set
	 */
	public void setBooks(List<String> books) {
		this.books = books;
	}

	/**
	 * @return the music
	 */
	public List<String> getMusic() {
		return music;
	}

	/**
	 * @param music the music to set
	 */
	public void setMusic(List<String> music) {
		this.music = music;
	}

	/**
	 * @return the movies
	 */
	public List<String> getMovies() {
		return movies;
	}

	/**
	 * @param movies the movies to set
	 */
	public void setMovies(List<String> movies) {
		this.movies = movies;
	}

	/**
	 * @return the tv
	 */
	public List<String> getTv() {
		return tv;
	}

	/**
	 * @param tv the tv to set
	 */
	public void setTv(List<String> tv) {
		this.tv = tv;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
	    ObjectWriter ow = new ObjectMapper().writer().with(new MinimalPrettyPrinter());
	    String json = "";
	    try {
	    	json = ow.writeValueAsString(this);
		} catch (JsonProcessingException e) {
		}
	    return json;
	}
}
