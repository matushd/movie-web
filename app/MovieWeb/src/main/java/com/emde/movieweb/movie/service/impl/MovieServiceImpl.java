/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.service.impl;

import com.emde.movieweb.company.exception.CompanyNotFound;
import com.emde.movieweb.company.repository.CompanyRepository;
import com.emde.movieweb.country.exception.CountryNotFound;
import com.emde.movieweb.country.repository.CountryRepository;
import com.emde.movieweb.genre.exception.GenreNotFound;
import com.emde.movieweb.genre.repository.GenreRepository;
import com.emde.movieweb.image.repository.ImageRepository;
import com.emde.movieweb.movie.controller.SearchController;
import com.emde.movieweb.movie.exception.KeywordAlreadyExists;
import com.emde.movieweb.movie.exception.KeywordNotFound;
import com.emde.movieweb.movie.exception.MovieAlreadyExists;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Keyword;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.repository.KeywordRepository;
import com.emde.movieweb.movie.repository.MovieRepository;
import com.emde.movieweb.movie.service.ImportService;
import com.emde.movieweb.movie.service.MovieService;
import com.emde.movieweb.movie.specifications.KeywordSpecifications;
import com.emde.movieweb.movie.specifications.MovieSpecifications;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.model.movie.MovieInfo;
import com.omertron.themoviedbapi.results.ResultList;

import java.sql.Date;
import java.util.List;
import javax.annotation.Resource;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.hibernate.Hibernate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class MovieServiceImpl implements MovieService {

    @Resource
    private MovieRepository movieRepository;
    
    @Resource
    private ImageRepository imageRepository;
    
    @Resource
    private KeywordRepository keywordRepository;
    
    @Resource
    private GenreRepository genreRepository;
    
    @Resource
    private CompanyRepository companyRepository;
    
    @Resource
    private CountryRepository countryRepository;
    
    @Autowired
    private TheMovieDbApi moviedbApi;
    
    @Autowired
    private ImportService importService;
    
    private static Logger logger = LogManager.getLogger(MovieServiceImpl.class.getName());
    
    @Override
    @Transactional(rollbackFor=MovieAlreadyExists.class)
    public Movie create(Movie movie) throws MovieAlreadyExists {
    	if (movieRepository.exists(movie.getId())) {
    		throw new MovieAlreadyExists(movie.getTitle());
    	}
        return movieRepository.save(movie);
    }
    
    @Override
    @Transactional
    public Boolean exists(Integer movieId) {
    	return movieRepository.exists(movieId);
    }

    @Override
    @Transactional(rollbackFor=MovieNotFound.class)
    public Movie delete(Integer id) throws MovieNotFound {
        Movie deleted = movieRepository.findOne(id);
		
        if (deleted == null)
                throw new MovieNotFound();

        movieRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public Page<Movie> findAll(Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.DESC, "id");
        return movieRepository.findAll(pageRequest);
    }
    
    @Override
    @Transactional
    public Page<Movie> findPopular(Integer pageNo, Integer size) {
    	Sort sort = new Sort(Sort.Direction.DESC, "voteCount");
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, sort);
    	return movieRepository.findAll(pageRequest);
    }
    
    @Override
    @Transactional
    public Page<Movie> findBestRated(Integer pageNo, Integer size) {
    	Sort sort = new Sort(Sort.Direction.DESC, "voteAvg");
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, sort);
    	return movieRepository.findAll(pageRequest);
    }
    
    @Override
    @Transactional
    public Page<Movie> findNowPlaying(Integer pageNo, Integer size) {
    	Sort sort = new Sort(Sort.Direction.DESC, "releaseDate");
    	Date to = new Date(LocalDateTime.now().toDate().getTime());
    	Date from = new Date(LocalDateTime.now().minusMonths(1).minusWeeks(2).toDate().getTime());
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, sort);
    	return movieRepository.findAll(MovieSpecifications.releaseIsBeetween(from, to), pageRequest);
    }

    @Override
    @Transactional
    public Movie update(Movie movie) throws MovieNotFound {
    	if (!movieRepository.exists(movie.getId())) {
    		throw new MovieNotFound();
    	}
        return movieRepository.save(movie);
    }
    
    @Override
    @Transactional
    public void flush() {
    	movieRepository.flush();
    }

    @Override
    @Transactional
    public Movie findById(Integer id) {
        return movieRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Movie findFullById(Integer id) throws MovieNotFound {
        Movie movie = movieRepository.findOne(id);
        if (movie == null)
            throw new MovieNotFound();
        Hibernate.initialize(movie.getGenres());
        Hibernate.initialize(movie.getVideos().iterator().next());
        Hibernate.initialize(movie.getSimilarMovies());
        Hibernate.initialize(movie.getCompanies());
        Hibernate.initialize(movie.getCountries());
        return movie;
    }
    
    @Override
    @Transactional
    public List<Movie> findByTitle(String title) throws MovieNotFound {
        List<Movie> movies = movieRepository.findAll(MovieSpecifications.titleIsEqual(title));
        if(movies.isEmpty()) throw new MovieNotFound();
        return movies;
    }
    
    @Override
    @Transactional
    public Page<Movie> searchByTitle(String title, Integer pageNo, Integer size) throws MovieNotFound {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Movie> movies = movieRepository.findAll(MovieSpecifications.titleIsLike(title.toLowerCase()),
    			pageRequest);
        if(movies.getSize() <= 0) throw new MovieNotFound();
        return movies;
    }
    
    @Override
    @Transactional
    public List<Movie> searchByTitle(String title) throws MovieNotFound {
    	List<Movie> movies = movieRepository.findAll(MovieSpecifications.titleIsLike(title));
        if(movies.isEmpty()) throw new MovieNotFound();
        return movies;
    }

    @Override
    @Transactional
    public List<Movie> randomWithImage(Integer count) throws MovieNotFound {
        Pageable page = new PageRequest(0, count);
        Page<Movie> movies = movieRepository.findAll(MovieSpecifications.hasImage(), page);
        if(!movies.hasContent()) throw new MovieNotFound();
        for(Movie movie : movies.getContent()) {
        	Hibernate.initialize(movie.getImages());
        }
        return movies.getContent();
    }
    
    @Override
    @Transactional
    public Keyword create(Keyword keyword) throws KeywordAlreadyExists {
    	if(keywordRepository.exists(keyword.getId())) {
    		throw new KeywordAlreadyExists(keyword.getName());
    	}
    	return keywordRepository.save(keyword);
    }
    
    @Override
    @Transactional
    public Page<Keyword> findAllKeywords(Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	return keywordRepository.findAll(pageRequest);
    }
    
    @Override
    @Transactional
    public Keyword findKeywordById(Integer id) {
    	return keywordRepository.findOne(id);
    }
    
    @Override
    @Transactional
    public Page<Movie> searchByKeywordId(Integer keywordId, Integer pageNo, Integer size) throws MovieNotFound, KeywordNotFound {
    	if(!keywordRepository.exists(keywordId)) {
    		throw new KeywordNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Movie> movies = movieRepository.findAll(MovieSpecifications.keywordIdIsEqual(keywordId), pageRequest);
    	if(movies.getSize() <= 0) {
    		throw new MovieNotFound();
    	}
    	return movies;
    }
    
    @Override
    @Transactional
    public Page<Movie> searchByGenreId(Integer genreId, Integer pageNo, Integer size) throws MovieNotFound, GenreNotFound {
    	if(!genreRepository.exists(genreId)) {
    		throw new GenreNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Movie> movies = movieRepository.findAll(MovieSpecifications.genreIdIsEqual(genreId), pageRequest);
    	if(movies.getSize() <= 0) {
    		throw new MovieNotFound();
    	}
    	return movies;
    }
    
    @Override
    @Transactional
    public Page<Movie> recommendedForUser(String userId, Integer pageNo, Integer size) throws MovieNotFound
    {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Movie> movies = movieRepository.findRecommended(userId, pageRequest);
    	if(movies.getSize() <= 0) {
    		throw new MovieNotFound();
    	}
    	return movies;
    }
    
    @Override
    @Transactional
    public Page<Movie> searchByCompanyId(Integer companyId, Integer pageNo, Integer size)
    		throws MovieNotFound, CompanyNotFound {
    	if(!companyRepository.exists(companyId)) {
    		throw new CompanyNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Movie> movies = movieRepository.findAll(MovieSpecifications.companyIdIsEqual(companyId), pageRequest);
    	if(movies.getSize() <= 0) {
    		throw new MovieNotFound();
    	}
    	return movies;
    }
    
    @Override
    @Transactional
    public Page<Movie> searchByCountryId(Integer countryId, Integer pageNo, Integer size) throws MovieNotFound, CountryNotFound
    {
    	if(!countryRepository.exists(countryId)) {
    		throw new CountryNotFound();
    	}
    	PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Movie> movies = movieRepository.findAll(MovieSpecifications.countryIdIsEqual(countryId), pageRequest);
    	if(movies.getSize() <= 0) {
    		throw new MovieNotFound();
    	}
    	return movies;
    }
    
    /**
     * Import popular movies async
     * @param Movie movie
     * @throws MovieDbException
     * @throws MovieNotFound 
     */
    @Cacheable(value="runCache", key="{#root.class.getName(), #root.methodName, #pageNo}")
    public void importPopularMovies(Integer pageNo) throws MovieDbException, MovieNotFound {
    	ResultList<MovieInfo> list = moviedbApi.getPopularMovieList(pageNo, SearchController.locale);
        logger.debug("Popular movies results size " + list.getResults().size() + " for page: " + pageNo);
        if(!list.getResults().isEmpty()) {
        	importService.runAsync(list, 0, list.getResults().size());
        }
    }
    
    /**
     * Import now playing movies async
     * @param Movie movie
     * @throws MovieDbException
     * @throws MovieNotFound 
     */
    @Cacheable(value="runCache", key="{#root.class.getName(), #root.methodName, #pageNo}")
    public void importNowPlayingMovies(Integer pageNo) throws MovieDbException, MovieNotFound {
    	ResultList<MovieInfo> list = moviedbApi.getNowPlayingMovies(pageNo, SearchController.locale);
        logger.debug("Popular movies results size " + list.getResults().size() + " for page: " + pageNo);
        if(!list.getResults().isEmpty()) {
        	importService.runAsync(list, 0, list.getResults().size());
        }
    }
    
    @Override
    @Transactional
    public Page<Keyword> searchByTagName(String tag, Integer pageNo, Integer size) throws KeywordNotFound {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "id");
    	Page<Keyword> keywords = keywordRepository.findAll(KeywordSpecifications.nameIsLike(tag.toLowerCase()),
    			pageRequest);
        if(keywords.getSize() <= 0) throw new KeywordNotFound();
        return keywords;
    }
}
