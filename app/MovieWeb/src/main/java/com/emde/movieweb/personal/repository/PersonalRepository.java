/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.personal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emde.movieweb.personal.model.Personal;

/**
 *
 * @author emde
 */
public interface PersonalRepository extends JpaRepository<Personal, Integer> {
    
}
