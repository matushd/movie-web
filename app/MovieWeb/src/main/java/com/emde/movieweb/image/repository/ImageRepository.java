/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.image.repository;

import com.emde.movieweb.image.model.Image;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author emde
 */
public interface ImageRepository extends JpaRepository<Image, Integer> {
    
    @Query("select i from Movie m join m.images i join i.type t where m.id = ?1 and i.type.name='BACKDROP'")
    public List<Image> getBackdrops(Integer movieId, Pageable pageRequest);
    
    @Query("select i from Movie m join m.images i join i.type t where m.id = ?1 and i.type.name='POSTER'")
    public List<Image> getPosters(Integer movieId, Pageable pageRequest);
    
    @Query("select i from Image i where i.hashId = ?1")
    public Image findByHashId(String hashId);
    
}
