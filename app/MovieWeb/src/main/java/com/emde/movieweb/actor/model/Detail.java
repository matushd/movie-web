package com.emde.movieweb.actor.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import com.emde.movieweb.personal.model.Personal;

/**
 *
 * @author emde
 */
@Entity(name="ActorDetail")
@DiscriminatorValue(value="0")  
public class Detail extends Personal {	

	@OneToOne
    @JoinColumn(name = "personal_id")
	private Actor actor;
	
	public Detail(Personal personal) {
		super(personal);
	}
	
	public Detail() {
		super();
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}
	
}
