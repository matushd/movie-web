package com.emde.movieweb.movie.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.movie.exception.KeywordNotFound;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Keyword;
import com.emde.movieweb.movie.service.MovieService;
import com.omertron.themoviedbapi.MovieDbException;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@RequestMapping(value="/keyword")
public class KeywordController {

	private final Integer PACK_SIZE = 120;
	
	@Autowired
    private MovieService movieService;
	
	/**
     * Keyword list action
     * @param model
     * @return
     */
	@Link(label="Tagi", family="", parent = "MovieWeb")
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public String keywords(Model model) {
        model.addAttribute("keywords", movieService.findAllKeywords(1, PACK_SIZE));
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/keyword/list/");
        return "keyword/list";
    }
	
	/**
     * Keyword pageable list action
     * @param model
     * @return
     */
	@Link(label="Tagi", family="", parent = "MovieWeb")
    @RequestMapping(value="/list/{pageNo}", method=RequestMethod.GET)
    public String keywords(Model model, @PathVariable(value="pageNo") Integer pageNo) {
        model.addAttribute("keywords",movieService.findAllKeywords(pageNo, PACK_SIZE));
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/keyword/list/");
        return "keyword/list";
    }
	
	/**
     * Search keyword action
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieTagi", family="", parent = "Tagi")
	@RequestMapping(value="/search", method=RequestMethod.GET)
    public String search(Model model, NativeWebRequest request, HttpSession session)
    		throws UnsupportedEncodingException {
		String tag = request.getParameter("data").toLowerCase();
        Page<Keyword> res = null;
        try {
            res = movieService.searchByTagName(tag, 1, PACK_SIZE);
        } catch (KeywordNotFound e) {
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + tag);
        model.addAttribute("keywords", res);
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/keyword/search/" + tag + "/");
        return "keyword/list";
    }
	
	/**
     * Search keyword action pageable
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieTagi", family="", parent = "Tagi")
	@RequestMapping(value="/search/{tagName}/{pageNo}", method=RequestMethod.GET)
    public String search(NativeWebRequest request, Model model, @PathVariable(value="tagName") String tag,
    		@PathVariable(value="pageNo") Integer pageNo, HttpSession session)
    		throws UnsupportedEncodingException {
		Page<Keyword> res = null;
        try {
            res = movieService.searchByTagName(tag, pageNo, PACK_SIZE);
        } catch (KeywordNotFound e) {
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania: - " + tag);
        model.addAttribute("keywords", res);
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/keyword/search/" + tag + "/");
        return "keyword/list";
    }
    
    /**
     * Movie list by keyword
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound 
     */
	@Link(label="Tag", family="", parent = "Tagi")
    @RequestMapping(value="/{keywordId}", method=RequestMethod.GET)
    public String keyword(Model model, @PathVariable(value="keywordId") Integer id, HttpSession session) throws MovieNotFound, KeywordNotFound {
    	model.addAttribute("movies", movieService.searchByKeywordId(id, 1, MovieController.PACK_SIZE));
    	model.addAttribute("current", 1);
    	model.addAttribute("baseUrl", "/keyword/" + id + "/");
    	Breadcrump.setLastLabelName(session, movieService.findKeywordById(id).getName());
        return "movie/all";
    }
	
	/**
	 * Movie pageable list by keyword
	 * @param model
	 * @param id
	 * @param session
	 * @param pageNo
	 * @return
	 * @throws MovieNotFound
	 * @throws KeywordNotFound
	 */
	@Link(label="Tag", family="", parent = "Tagi")
    @RequestMapping(value="/{keywordId}/{pageNo}", method=RequestMethod.GET)
    public String keyword(Model model, @PathVariable(value="keywordId") Integer id, HttpSession session,
    		@PathVariable(value="pageNo") Integer pageNo) throws MovieNotFound, KeywordNotFound {
    	model.addAttribute("movies", movieService.searchByKeywordId(id, pageNo, MovieController.PACK_SIZE));
    	model.addAttribute("current", pageNo);
    	model.addAttribute("baseUrl", "/keyword/" + id + "/");
    	Breadcrump.setLastLabelName(session, movieService.findKeywordById(id).getName());
        return "movie/all";
    }
}
