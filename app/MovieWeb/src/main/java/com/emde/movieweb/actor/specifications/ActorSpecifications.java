/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.actor.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

import com.emde.movieweb.actor.model.Actor;

/**
 *
 * @author emde
 */
public class ActorSpecifications {
    
    public static Specification<Actor> nameIsLike(final String name) {
        return new Specification<Actor>() {
            @Override
            public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(cb.lower(root.get("name")), "%" + name + "%");
            }
        };
    }
}
