/**
 * 
 */
package com.emde.movieweb.page.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.emde.movieweb.page.model.PageRank;

/**
 * @author emde
 *
 */
public interface PageRankRepostiory extends JpaRepository<PageRank, Integer>, JpaSpecificationExecutor<PageRank>{

}
