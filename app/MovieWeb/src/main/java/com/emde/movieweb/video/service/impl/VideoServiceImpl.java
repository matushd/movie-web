/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video.service.impl;

import com.emde.movieweb.video.exception.VideoAlreadyExists;
import com.emde.movieweb.video.exception.VideoNotFound;
import com.emde.movieweb.video.exception.VideoTypeAlreadyExists;
import com.emde.movieweb.video.exception.VideoTypeNotFound;
import com.emde.movieweb.video.model.Video;
import com.emde.movieweb.video.model.VideoType;
import com.emde.movieweb.video.repository.VideoRepository;
import com.emde.movieweb.video.repository.VideoTypeRepository;
import com.emde.movieweb.video.service.VideoService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class VideoServiceImpl implements VideoService {

    @Resource
    private VideoRepository videoRepository;
    
    @Resource
    private VideoTypeRepository videoTypeRepository;
    
    @Override
    @Transactional(rollbackFor=VideoAlreadyExists.class)
    public Video create(Video video) throws VideoAlreadyExists {
    	Video created = videoRepository.findByName(video.getKey());
    	if (created != null) {
    		return created;
    	}
        created = video;
        return videoRepository.save(created);
    }

    @Override
    @Transactional(rollbackFor=VideoNotFound.class)
    public Video delete(Integer id) throws VideoNotFound {
        Video deleted = videoRepository.findOne(id);
        if (deleted == null) {
        	throw new VideoNotFound();
        }
        videoRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<Video> findAll() {
        return videoRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=VideoNotFound.class)
    public Video update(Video video) throws VideoNotFound {
    	if(!videoRepository.exists(video.getId())) {
    		throw new VideoNotFound();
    	}
        return videoRepository.save(video);
    }

    @Override
    @Transactional
    public Video findById(Integer id) {
        return videoRepository.findOne(id);
    }

    @Override
    @Transactional(rollbackFor=VideoTypeAlreadyExists.class)
    public VideoType createType(VideoType videoType) throws VideoTypeAlreadyExists {
    	VideoType created = videoTypeRepository.findByName(videoType.getName());
    	if (created != null) {
    		return created;
    	}
        created = videoType;
        return videoTypeRepository.save(created);
    }

    @Override
    @Transactional(rollbackFor=VideoTypeNotFound.class)
    public VideoType deleteType(Integer id) throws VideoTypeNotFound {
        VideoType deleted = videoTypeRepository.findOne(id);
        if (deleted == null) {
        	throw new VideoTypeNotFound();
        }
        videoTypeRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public List<VideoType> findAllTypes() {
        return videoTypeRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=VideoTypeNotFound.class)
    public VideoType updateType(VideoType videoType) throws VideoTypeNotFound {
        if(!videoTypeRepository.exists(videoType.getId())) {
        	throw new VideoTypeNotFound();
        }
        return videoTypeRepository.save(videoType);
    }

    @Override
    @Transactional
    public VideoType findTypeById(Integer id) {
        return videoTypeRepository.findOne(id);
    }
    
}
