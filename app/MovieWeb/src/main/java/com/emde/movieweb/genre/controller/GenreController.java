package com.emde.movieweb.genre.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.genre.exception.GenreNotFound;
import com.emde.movieweb.genre.service.GenreService;
import com.emde.movieweb.movie.controller.MovieController;
import com.emde.movieweb.movie.exception.KeywordNotFound;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.service.MovieService;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@RequestMapping(value="/genre")
public class GenreController {
	
	//private static Logger logger = LogManager.getLogger(GenreController.class.getName());

	@Autowired
	private GenreService genreService;
	
	@Autowired
	private MovieService movieService;
	
	/**
     * Genre list action
     * @param model
     * @return
     */
	@Link(label="Gatunki", family="", parent = "MovieWeb")
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public String genres(Model model) {
        model.addAttribute("genres",genreService.findAll());
        return "genre/list";
    }
    
    /**
     * Movie list by genre
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound  
     */
    @Link(label="Gatunek", family="", parent = "Gatunki")
    @RequestMapping(value="/{genreId}", method=RequestMethod.GET)
    public String genre(Model model, @PathVariable(value="genreId") Integer id, HttpSession session)
    		throws MovieNotFound, GenreNotFound {
    	model.addAttribute("movies", movieService.searchByGenreId(id, 1, MovieController.PACK_SIZE));
    	model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/genre/" + id + "/");
    	String countryName = genreService.findById(id).getName();
    	Breadcrump.setLastLabelName(session, countryName);
    	model.addAttribute("name", countryName);
        return "movie/all";
    }
    
    /**
     * Movie pageable list by genre
     * @param model
     * @param id
     * @return
     * @throws MovieNotFound
     * @throws KeywordNotFound  
     */
    @Link(label="Gatunek", family="", parent = "Gatunki")
    @RequestMapping(value="/{genreId}/{pageNo}", method=RequestMethod.GET)
    public String genre(Model model, @PathVariable(value="genreId") Integer id,
    		@PathVariable(value="pageNo") Integer pageNo, HttpSession session) throws MovieNotFound, GenreNotFound {
    	model.addAttribute("movies", movieService.searchByGenreId(id, pageNo, MovieController.PACK_SIZE));
    	model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/genre/" + id + "/");
        String countryName = genreService.findById(id).getName();
    	Breadcrump.setLastLabelName(session, countryName);
    	model.addAttribute("name", countryName);
        return "movie/all";
    }
}
