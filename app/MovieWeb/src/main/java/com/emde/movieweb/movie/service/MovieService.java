/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie.service;

import com.emde.movieweb.company.exception.CompanyNotFound;
import com.emde.movieweb.country.exception.CountryNotFound;
import com.emde.movieweb.genre.exception.GenreNotFound;
import com.emde.movieweb.movie.exception.KeywordAlreadyExists;
import com.emde.movieweb.movie.exception.KeywordNotFound;
import com.emde.movieweb.movie.exception.MovieAlreadyExists;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Keyword;
import com.emde.movieweb.movie.model.Movie;
import com.omertron.themoviedbapi.MovieDbException;

import java.util.List;

import org.springframework.data.domain.Page;

/**
 *
 * @author emde
 */

public interface MovieService {
	/* Movie */
    public Movie create(Movie movie) throws MovieAlreadyExists;
    public Movie delete(Integer id) throws MovieNotFound;
    public Page<Movie> findAll(Integer pageNo, Integer size);
    public Page<Movie> findPopular(Integer pageNo, Integer size);
    public Page<Movie> findBestRated(Integer pageNo, Integer size);
    public Page<Movie> findNowPlaying(Integer pageNo, Integer size);
    public void importPopularMovies(Integer pageNo) throws MovieDbException, MovieNotFound;
    public void importNowPlayingMovies(Integer pageNo) throws MovieDbException, MovieNotFound;
    public Movie update(Movie movie) throws MovieNotFound;
    public void flush();
    public Movie findById(Integer id);
    public Movie findFullById(Integer id) throws MovieNotFound;
    public Page<Movie> recommendedForUser(String userId, Integer pageNo, Integer size) throws MovieNotFound;
    public Page<Movie> searchByTitle(String title, Integer pageNo, Integer size) throws MovieNotFound;
    public List<Movie> searchByTitle(String title) throws MovieNotFound;
    public List<Movie> findByTitle(String title) throws MovieNotFound;
    public List<Movie> randomWithImage(Integer count) throws MovieNotFound;
    public Boolean exists(Integer movieId);
    public Page<Movie> searchByKeywordId(Integer keywordId, Integer pageNo, Integer size) throws MovieNotFound, KeywordNotFound;
    public Page<Movie> searchByGenreId(Integer genreId, Integer pageNo, Integer size) throws MovieNotFound, GenreNotFound;
    public Page<Movie> searchByCompanyId(Integer companyId, Integer pageNo, Integer size) throws MovieNotFound, CompanyNotFound;
    public Page<Movie> searchByCountryId(Integer countryId, Integer pageNo, Integer size) throws MovieNotFound, CountryNotFound;
    
    /* Keyword */
    public Keyword create(Keyword keyword) throws KeywordAlreadyExists;
    public Page<Keyword> findAllKeywords(Integer pageNo, Integer size);
    public Keyword findKeywordById(Integer id);
    public Page<Keyword> searchByTagName(String tag, Integer pageNo, Integer size) throws KeywordNotFound;
}
