/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.country.service;

import com.emde.movieweb.country.exception.CountryAlreadyExists;
import com.emde.movieweb.country.exception.CountryNotFound;
import com.emde.movieweb.country.model.Country;
import org.springframework.data.domain.Page;

/**
 *
 * @author emde
 */
public interface CountryService {
    public Country create(Country country) throws CountryAlreadyExists;
    public Country delete(Integer id) throws CountryNotFound;
    public Page<Country> findAll(Integer pageNo, Integer size);
    public Country update(Country country) throws CountryNotFound;
    public Country findById(Integer id);
}
