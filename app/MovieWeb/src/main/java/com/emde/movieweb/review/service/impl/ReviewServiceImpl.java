/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.review.service.impl;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.repository.MovieRepository;
import com.emde.movieweb.review.exception.ReviewAlreadyExists;
import com.emde.movieweb.review.exception.ReviewNotFound;
import com.emde.movieweb.review.model.Review;
import com.emde.movieweb.review.repository.ReviewRepository;
import com.emde.movieweb.review.service.ReviewService;
import com.emde.movieweb.review.specifications.ReviewSpecifications;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.results.ResultList;
import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;

/**
 *
 * @author emde
 */
@Service
public class ReviewServiceImpl implements ReviewService {

	@Resource
    private MovieRepository movieRepository;
	
	@Resource
    private ReviewRepository reviewRepository;
	
	@Autowired
    private TheMovieDbApi moviedbApi;
	
	@Autowired
    private Translate translateApi;
	
	private static Logger logger = LogManager.getLogger(ReviewServiceImpl.class.getName());
	
	@Override
	@Transactional
	@Async
	public void importMovieReviews(Movie movie) throws Exception {
		ResultList<com.omertron.themoviedbapi.model.review.Review> list = moviedbApi.getMovieReviews(movie.getId(), 1, "en");
        logger.debug("Movies review results size " + list.getResults().size() + " for movie: " + movie.getId());
        for(com.omertron.themoviedbapi.model.review.Review review : list.getResults()) {
        	Review newReview = new Review();
        	newReview.setId(review.getId());
        	newReview.setAuthor(review.getAuthor());
        	newReview.setContent(translateApi.translate(review.getContent(), 
        			Language.ENGLISH, Language.POLISH));
        	newReview.setMovie(movie);
        	try {
				create(newReview);
			} catch (ReviewAlreadyExists e) {
				logger.warn(e.getMessage());
			}
        }
	}
	
	@Override
	@Transactional
	public Review create(Review review) throws ReviewAlreadyExists {
		if(reviewRepository.exists(review.getId())) {
			throw new ReviewAlreadyExists(review.getId());
		}
		return reviewRepository.save(review);
	}
	
	@Override
	@Transactional
	public Page<Review> findAll(Integer movieId, Integer pageNo, Integer size)
			throws MovieNotFound, ReviewNotFound {
		if(!movieRepository.exists(movieId)) {
			throw new MovieNotFound();
		}
		PageRequest pageRequest = new PageRequest(pageNo-1, size);
    	Page<Review> reviews = reviewRepository.findAll(ReviewSpecifications.movieIdIsEqual(movieId), pageRequest);
    	if(reviews.getSize() <= 0) {
    		throw new ReviewNotFound();
    	}
    	return reviews;
	}
    
}
