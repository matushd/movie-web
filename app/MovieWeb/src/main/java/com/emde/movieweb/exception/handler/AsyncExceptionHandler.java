/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.exception.handler;

import java.lang.reflect.Method;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

/**
 *
 * @author emde
 */
public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    private final static Logger LOGGER = LogManager.getLogger(AsyncExceptionHandler.class.getName());
    
    @Override
    public void handleUncaughtException(Throwable thrwbl, Method method, Object... os) {
        LOGGER.error("Async exception: " + thrwbl.getMessage() + " | Method: " + method.getName());
    }
    
}
