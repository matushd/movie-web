/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.country.service.impl;

import com.emde.movieweb.country.exception.CountryAlreadyExists;
import com.emde.movieweb.country.exception.CountryNotFound;
import com.emde.movieweb.country.model.Country;
import com.emde.movieweb.country.repository.CountryRepository;
import com.emde.movieweb.country.service.CountryService;
import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author emde
 */
@Service
public class CountryServiceImpl implements CountryService {
    
    @Resource
    private CountryRepository countryRepository;

    @Override
    @Transactional(rollbackFor=CountryAlreadyExists.class)
    public Country create(Country country) throws CountryAlreadyExists {
        Country created = countryRepository.findByIso(country.getIso());
        if(created != null) {
        	return created;
        }
        return countryRepository.save(country);
    }

    @Override
    @Transactional(rollbackFor=CountryNotFound.class)
    public Country delete(Integer id) throws CountryNotFound {
        Country deleted = countryRepository.findOne(id);
		
        if (deleted == null)
                throw new CountryNotFound();

        countryRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional
    public Page<Country> findAll(Integer pageNo, Integer size) {
    	PageRequest pageRequest = new PageRequest(pageNo-1, size, Sort.Direction.ASC, "name");
        return countryRepository.findAll(pageRequest);
    }

    @Override
    @Transactional(rollbackFor=CountryNotFound.class)
    public Country update(Country country) throws CountryNotFound {
    	if(!countryRepository.exists(country.getId())) {
    		throw new CountryNotFound();
    	}
        return countryRepository.save(country);
    }

    @Override
    @Transactional
    public Country findById(Integer id) {
        return countryRepository.findOne(id);
    }
    
}
