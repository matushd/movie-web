/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.actor.service;

import com.emde.movieweb.actor.exception.ActorAlreadyExists;
import com.emde.movieweb.actor.exception.ActorNotFound;
import com.emde.movieweb.actor.exception.RoleAlreadyExists;
import com.emde.movieweb.actor.exception.RoleNotFound;
import com.emde.movieweb.actor.model.Actor;
import com.emde.movieweb.actor.model.Role;
import com.emde.movieweb.movie.exception.MovieNotFound;
import java.util.List;
import java.util.TreeMap;

import org.springframework.data.domain.Page;

/**
 *
 * @author emde
 */
public interface ActorService {
    /* Actor */
    public Actor create(Actor actor) throws ActorAlreadyExists;
    public Actor delete(Integer id) throws ActorNotFound;
    public Page<Actor> findAll(Integer pageNo, Integer size);
    public Actor update(Actor actor) throws ActorNotFound;
    public Actor findById(Integer id);
    public Actor findFullById(Integer id);
    public Page<Actor> searchByName(String name, Integer pageNo, Integer size) throws ActorNotFound;
    
    /* Role */
    public Role createRole(Role role) throws RoleAlreadyExists;
    public Role deleteRole(Integer id) throws RoleNotFound;
    public List<Role> findAllRoles();
    public Role updateRole(Role role) throws RoleNotFound;
    public Role findRoleById(Integer id);
    public Page<Role> searchByMovieId(Integer movieId, Integer pageNo, Integer size)
    		throws MovieNotFound, ActorNotFound;
    public TreeMap<Integer, List<Role>> searchByActorIdGroupedByYear(Integer actorId, Integer pageNo, Integer size)
    		throws ActorNotFound;
}
