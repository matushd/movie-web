package com.emde.movieweb.crew.controller;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.emde.movieweb.crew.model.Detail;
import com.emde.movieweb.crew.model.Job;
import com.emde.movieweb.actor.controller.ActorController;
import com.emde.movieweb.breadcrumps.Breadcrump;
import com.emde.movieweb.crew.exception.CrewNotFound;
import com.emde.movieweb.crew.model.Crew;
import com.emde.movieweb.crew.service.CrewService;
import com.emde.movieweb.exception.EmptyCreditsException;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.ImportService;
import com.emde.movieweb.movie.service.MovieService;
import com.emde.movieweb.movie.service.impl.ImportServiceImpl;
import com.emde.movieweb.personal.exception.PersonalAlreadyExists;
import com.emde.movieweb.personal.service.PersonalService;
import com.google.common.base.Stopwatch;
import com.omertron.themoviedbapi.AppendToResponseBuilder;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.enumeration.PeopleMethod;
import com.omertron.themoviedbapi.model.person.PersonInfo;
import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;

import dummiesmind.breadcrumb.springmvc.annotations.Link;

/**
*
* @author emde
*/
@Controller
@RequestMapping(value="/crew")
public class CrewController {
	@Autowired
    private CrewService crewService;
	
	@Autowired
    private PersonalService personalService;
	
	@Autowired
    private MovieService movieService;
	
	@Autowired
    private TheMovieDbApi moviedbApi;
	
	@Autowired
    private Translate translateApi;
	
	@Autowired
    private ImportService importService;
	
	private static final Logger logger = LogManager.getLogger(CrewController.class.getName());
	
	/**
	 * Crew detail action
	 * @param model
	 * @param id
	 * @param session
	 * @return
	 * @throws MovieDbException
	 * @throws PersonalAlreadyExists
	 * @throws CrewNotFound 
	 * @throws MovieNotFound 
	 */
	@Link(label="Crew", family="", parent="Obsługa")
	@RequestMapping(value="/{crewId}", method=RequestMethod.GET)
	public String crew(Model model, @PathVariable(value="crewId") Integer id, HttpSession session,
			UsernamePasswordAuthenticationToken user)
			throws MovieDbException, PersonalAlreadyExists, CrewNotFound, MovieNotFound {
		Crew crew = crewService.findFullById(id);
		if(crew == null) {
			return "common/error";
		}
		
		if(crew.getDetail() == null) {
			importCrewDetails(crew);
		}
		
		Breadcrump.setLastLabelName(session, crew.getName());
		model.addAttribute(crew);
		model.addAttribute("jobsByYear", crewService.searchByCrewIdGroupedByYear(id, 1, 20).descendingMap());
		model.addAttribute("recommended", user != null ? movieService.recommendedForUser((String)user.getPrincipal(), 1, 5) : null);
		return "crew/detail";
	}
	
	/**
	 * Crews list action
	 * @param model
	 * @return
	 */
	@Link(label="Obsługa", family="", parent="MovieWeb")
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public String crews(Model model) {
		model.addAttribute("crews", crewService.findAll(1, ActorController.PACK_SIZE));
		model.addAttribute("current", 1);
		return "crew/list";
	}
	
	/**
	 * Crews pageable list action
	 * @param model
	 * @return
	 */
	@Link(label="Obsługa", family="", parent="MovieWeb")
	@RequestMapping(value="/list/{pageNo}", method=RequestMethod.GET)
	public String crews(Model model, @PathVariable(value="pageNo") Integer pageNo) {
		model.addAttribute("crews", crewService.findAll(pageNo, ActorController.PACK_SIZE));
		model.addAttribute("current", pageNo);
		return "crew/list";
	}
	
	/**
     * Search crew action
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieObsługa", family="", parent = "Obsługa")
	@RequestMapping(value="/search", method=RequestMethod.GET)
    public String search(Model model, NativeWebRequest request, HttpSession session)
    		throws UnsupportedEncodingException {
		String crewName = request.getParameter("data").toLowerCase();
        Page<Crew> res = null;
        try {
            res = crewService.searchByName(crewName, 1, ActorController.PACK_SIZE);
        } catch (CrewNotFound e) {
            logger.warn(e.getMessage());
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + crewName);
        model.addAttribute("crews", res);
        model.addAttribute("current", 1);
        model.addAttribute("baseUrl", "/crew/search/" + crewName + "/");
        return "crew/list";
    }
	
	/**
     * Search crew action pageable
     * @param request
     * @return
     * @throws MovieDbException
     * @throws UnsupportedEncodingException 
     */
	@Link(label="WyszukiwanieObsługa", family="", parent = "Obsługa")
	@RequestMapping(value="/search/{crewName}/{pageNo}", method=RequestMethod.GET)
    public String search(NativeWebRequest request, Model model, @PathVariable(value="crewName") String crewName,
    		@PathVariable(value="pageNo") Integer pageNo, HttpSession session)
    		throws UnsupportedEncodingException {
		Page<Crew> res = null;
        try {
            res = crewService.searchByName(crewName, pageNo, ActorController.PACK_SIZE);
        } catch (CrewNotFound e) {
            logger.warn(e.getMessage());
            return "common/error";
        }
        Breadcrump.setLastLabelName(session, "Wyniki wyszukiwania - " + crewName);
        model.addAttribute("crews", res);
        model.addAttribute("current", pageNo);
        model.addAttribute("baseUrl", "/crew/search/" + crewName + "/");
        return "crew/list";
    }
	
	/**
	 * Import crews by movie id
	 * @param model
	 * @param movieId
	 * @param pageNo
	 * @return
	 * @throws MovieDbException
	 * @throws EmptyCreditsException
	 */
	@RequestMapping(value="/movie/{movieId}/{pageNo}", method=RequestMethod.GET)
    public String importByMovie(Model model, @PathVariable(value="movieId") Integer movieId,
    		@PathVariable(value="pageNo") Integer pageNo) throws MovieDbException, EmptyCreditsException {
		Page<Job> jobs = null;
    	try {
    		jobs = crewService.searchByMovieId(movieId, pageNo, ImportServiceImpl.crewLimit);
    		if(pageNo > 1 && jobs.getTotalElements() <= ImportServiceImpl.crewLimit) {
        		Movie movie = movieService.findById(movieId);
        		importService.importCrews(movie);
        	}
		} catch (MovieNotFound | CrewNotFound e) {
			logger.warn(e.getMessage());
		}
    	
    	model.addAttribute("jobs", jobs.getContent());
    	return "actor/roles";
    }
	
	/**
	 * Import crew detail
	 * @param crew
	 */
	private void importCrewDetails(Crew crew) {
		try {
			Stopwatch stopwatch = Stopwatch.createStarted();
			String atr = new AppendToResponseBuilder(PeopleMethod.IMAGES).build();
			PersonInfo person = moviedbApi.getPersonInfo(crew.getId(), atr);
			if(person.getBiography() != null) {
				person.setBiography(translateApi.translate(person.getBiography(),
						Language.ENGLISH, Language.POLISH));
				Detail crewDetail = new Detail(personalService.create(person));
				crew.setDetail(crewDetail);
			}
			if(crew.getImages().size() < 2) {
				importService.importCrewImages(crew, person.getImages());
			}
			logger.info("Crew detail import: " + crew.getName() + " in time " + stopwatch.elapsed(TimeUnit.SECONDS));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}
}
