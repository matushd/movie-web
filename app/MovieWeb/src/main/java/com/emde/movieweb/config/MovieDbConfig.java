/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.config;

import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import javax.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 *
 * @author emde
 */
@Configuration
@PropertySource("classpath:application.properties")
public class MovieDbConfig {
    
    @Resource
    private Environment environment;
    
    @Bean(name = "moviedbApi")
    public TheMovieDbApi getMovieDbApi() throws MovieDbException {
    	TheMovieDbApi movieApi = new TheMovieDbApi(environment.getProperty("moviedb.api.key"));
    	return movieApi;
    }
    
}
