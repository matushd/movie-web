package com.emde.movieweb.personal.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.hibernate.annotations.DiscriminatorFormula;

/**
 *
 * @author emde
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorFormula("0")
public class Personal {	

	@Id
    @Column(name = "personal_id")
    protected Integer id;
	
	@Column(name = "personal_bio")
	protected String bio;
	
	@Column(name = "personal_homepage")
	protected String homepage;
	
	@Column(name = "personal_popularity")
	protected Double popularity;
	
	@Column(name = "personal_birth_place")
	protected String birthPlace;
	
	@Column(name = "personal_birth")
	protected Date birthDate;
	
	@Column(name = "personal_death")
	protected Date deathDate;
	
	@Column(name = "personal_adult")
    private Boolean adult;
	
	@Column(name = "personal_knows_as")
	protected String knowsAs;
	
	@Column(name = "personal_gender")
	protected Integer gender;
	
	public Personal(Personal personal) {
		this.id = personal.getId();
		this.bio = personal.getBio();
		this.birthDate = personal.getBirthDate();
		this.birthPlace = personal.getBirthPlace();
		this.deathDate = personal.getDeathDate();
		this.homepage = personal.getHomepage();
		this.knowsAs = personal.getKnowsAs();
		this.popularity = personal.getPopularity();
		this.adult = personal.getAdult();
		this.gender = personal.getGender();
	}
	
	public Personal() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public Double getPopularity() {
		return popularity;
	}

	public void setPopularity(Double popularity) {
		this.popularity = popularity;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public Boolean getAdult() {
		return adult;
	}

	public void setAdult(Boolean adult) {
		this.adult = adult;
	}

	public String getKnowsAs() {
		return knowsAs;
	}

	public void setKnowsAs(String knowsAs) {
		this.knowsAs = knowsAs;
	}

	/**
	 * @return the gender
	 */
	public Integer getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	
	public String getGenderName()
	{
		switch(this.gender) {
			case 1:
				return "Mężczyzna";
			case 0:
				return "Kobieta";
			default:
				return null;
		}
	}
}
