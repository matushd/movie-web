/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var MV = MV || {};

MV.Fragment = MV.Fragment || {};
MV.Fragment.Image = (function() {
    var TMDB_URL = 'http://image.tmdb.org/t/p/w';
    
    function load(url, target, cls, width, height) {
        $.ajax({
            dataType: "json",
            url: url
        }).done(function( data ) {
            var img = $('<img/>').addClass(cls).addClass(data.type.name.toLowerCase())
                    .attr('src', TMDB_URL + width + data.filePath);
            $(target).append(img);
        });
    }
    
    function fancybox(element) {
        $(document).ready(function() {
        	$(element).fancybox();
        });
    }
    
    return {
        load: load
    };
})();

MV.Fancybox = (function() {
    
    function init(element) {
        $(document).ready(function() {
		$(element).fancybox();
	});
    }
    
    return {
        init: init
    };
})();

MV.Modal = (function () {
	var serviceUrl;
	
	function Modal() {
        if (!(this instanceof Modal)) {
            return new Modal();
        }
    }
	
	Modal.prototype.init = function(serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
	Modal.prototype.load = function(destinationEl) {
		$.ajax({
            dataType: "html",
            url: this.serviceUrl
        }).done(function( data ) {
            destinationEl.append(data);
        });
	}
	
	return Modal();
}());

MV.Page = (function () {
	var destination;
	var serviceUrl;
	var current;
	var total;
	
	function Page() {
        if (!(this instanceof Page)) {
            return new Page();
        }
    }
	
	Page.prototype.replace = function(data) {
		$(this.destination).replaceWith(data);
	}
	
	Page.prototype.load = function() {
		$.ajax({
            dataType: "html",
            url: this.serviceUrl + this.current
        }).done(function( data ) {
        	MV.Page.replace(data);
        });
	}
	
	Page.prototype.set = function(pageNo) {
		if(this.current == pageNo || pageNo < this.total || pageNo > 1) {
			this.current = pageNo;
			MV.Page.load();
		}
	}
	
	Page.prototype.init = function(destination, serviceUrl, current, total) {
		this.serviceUrl = serviceUrl;
		this.current = current;
		this.total = total;
		this.destination = destination;
		$(destination + ' a').on('click', function(evt) {
			evt.preventDefault();
			MV.Page.set($(this).data('page'));
		    return false;
		});
	}
	
	return Page();
}());

MV.More = (function () {
	var destination;
	var serviceUrl;
	var current;
	var total;
	var button;
	var packSize;
	
	function More() {
    }
	
	More.prototype.replace = function(data) {
		if(data == "") {
			this.total = 0;
			this.button.hide();
			return;
		}
		if(this.packSize > $(data).size()) {
			this.total = 0;
			this.button.hide();
		} else {
			this.total++;
		}
		$(this.destination).append(data);
	}
	
	More.prototype.load = function() {
		$.ajax({
            dataType: "html",
            url: this.serviceUrl + this.current,
            context: this
        }).done(this.replace);
	}
	
	More.prototype.set = function(pageNo) {
		if(this.current != pageNo && pageNo <= this.total+1 && pageNo > 1) {
			this.current = pageNo;
			this.load();
		}
	}
	
	More.prototype.next = function(event) {
		event.preventDefault();
		var pageNo = this.current + 1;
		this.set(pageNo);
		return false;
	}
	
	More.prototype.init = function(destination, button,  serviceUrl, total, packSize) {
		this.serviceUrl = serviceUrl;
		this.current = 1;
		this.total = total;
		this.destination = destination;
		this.button = button;
		this.packSize = packSize;
		button.on('click', $.proxy(this.next, this));
	}
	
	return new More();
});

MV.Event = (function() {
	var SERVICE_URL = '/event/push';
	var data = {};
	
	function Event() {
        if (!(this instanceof Event)) {
        	
            return new Event();
        }
    }
	
	function getData(pushData) {
		return {
				path: $(location).attr('pathname'),
				hash: $(location).attr('hash'),
				search: $(location).attr('search'),
				referer: document.referrer.replace(/^[^:]+:\/\/[^/]+/, '').replace(/#.*/, ''),
				title: $('title').text(),
				"data": $.extend(data, pushData)
				};
	}
	
	Event.prototype.push = function(data) {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$.ajax({
			type: "POST",
            dataType: "json",
            url: SERVICE_URL,
            data: JSON.stringify(getData(data)),
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
            },
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            }
        });
	}
	
	Event.prototype.add = function(key, value) {
		data[key] = value;
	}
	
	return Event();
})();