/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.genre;

import com.emde.movieweb.config.Config;
import com.emde.movieweb.genre.exception.GenreAlreadyExists;
import com.emde.movieweb.genre.exception.GenreNotFound;
import com.emde.movieweb.genre.model.Genre;
import com.emde.movieweb.genre.service.GenreService;
import javax.transaction.Transactional;
import junit.framework.TestCase;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author emde
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(
        classes = Config.class
)
@TransactionConfiguration(defaultRollback = true)
@Transactional
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@Ignore
public class GenreModelTest extends TestCase {
    
    @Autowired
    private GenreService genreService;
    
    private static Integer testGenreId ;
    
    @Test
    public void get_genre_list() {
        genreService.findAll();
    }
    
    @Test
    public void create_user() throws GenreNotFound, GenreAlreadyExists {
        Genre tmp = new Genre();
        tmp.setName("Testowa");
        Genre genre = genreService.create(tmp);
        testGenreId = genre.getId();
    }
    
    @Test
    public void find_user() {
        genreService.findById(testGenreId);
    }
    
    //@Test
    public void delete_user() throws GenreNotFound {
        genreService.delete(testGenreId);
        
    }
}