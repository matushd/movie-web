/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie;

import com.emde.movieweb.config.Config;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.MovieService;
import java.util.List;
import javax.transaction.Transactional;
import junit.framework.TestCase;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author emde
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(
        classes = Config.class
)
@TransactionConfiguration(defaultRollback = true)
@Transactional
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class SpecificationsTest extends TestCase {
    
    private static final Logger logger = LogManager.getLogger(SpecificationsTest.class.getName());
    
    @Autowired
    private MovieService movieService;
    
   @Test
    public void get_with_title() throws MovieNotFound {
        List<Movie> movies = movieService.randomWithImage(10);
        logger.debug(movies.get(0).getTitle());
    }
}
