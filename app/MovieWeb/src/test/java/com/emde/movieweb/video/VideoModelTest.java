/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.video;

import com.emde.movieweb.config.Config;
import com.emde.movieweb.movie.exception.MovieAlreadyExists;
import com.emde.movieweb.movie.exception.MovieNotFound;
import com.emde.movieweb.movie.model.Movie;
import com.emde.movieweb.movie.service.MovieService;
import com.emde.movieweb.video.exception.VideoAlreadyExists;
import com.emde.movieweb.video.exception.VideoNotFound;
import com.emde.movieweb.video.exception.VideoTypeAlreadyExists;
import com.emde.movieweb.video.exception.VideoTypeNotFound;
import com.emde.movieweb.video.model.Video;
import com.emde.movieweb.video.model.VideoType;
import com.emde.movieweb.video.service.VideoService;
import java.sql.Date;
import javax.transaction.Transactional;
import junit.framework.TestCase;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.joda.time.LocalDateTime;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author emde
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(
        classes = Config.class
)
@TransactionConfiguration(defaultRollback = true)
@Transactional
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class VideoModelTest extends TestCase {
    
    private static final Logger logger = LogManager.getLogger(VideoModelTest.class.getName());
    
    @Autowired
    private VideoService videoService;
    
    @Autowired
    private MovieService movieService;
    
    private static Video testVideo ;
    private static VideoType testVideoType ;
    private static Movie testMovie;
    
    @Test
    public void a_create_movie() throws MovieAlreadyExists {
        Movie tmpMovie = new Movie();
        tmpMovie.setOriginalTitle("Orignal title");
        tmpMovie.setOverview("Owerview");
        tmpMovie.setTitle("Title");
        tmpMovie.setVoteAvg(5.5);
        tmpMovie.setVoteCount(12);
        tmpMovie.setDate(new Date(LocalDateTime.now().toDate().getTime()));
        tmpMovie.setReleaseDate(new Date(LocalDateTime.now().toDate().getTime()));
        testMovie = movieService.create(tmpMovie);
        logger.debug(testMovie.getId());
    }
    
    @Test
    public void b_create_video_type() throws VideoTypeAlreadyExists {
        VideoType tmpType = new VideoType();
        tmpType.setName("Testowy typ video");
        testVideoType = videoService.createType(tmpType);
        logger.debug(testVideoType.getId());
    }
    
    @Test
    public void c_find_video_type() {
        VideoType videoType = videoService.findTypeById(testVideoType.getId());
        logger.debug(videoType);
    }
    
    @Test
    public void d_create_video() throws VideoNotFound, VideoAlreadyExists, VideoTypeAlreadyExists {
        Video tmp = new Video();
        tmp.setName("Tetowy tytul");
        tmp.setKey("sobeKey123");
        tmp.setSize(12);
        tmp.setType(testVideoType);
        tmp.setMovie(testMovie);
        testVideo = videoService.create(tmp);
        logger.debug(testVideo);
    }
    
    @Test
    public void e_get_video_list() {
        logger.debug(videoService.findAll());
    }
    
    @Test
    public void f_get_video_types_list() {
        logger.debug(videoService.findAllTypes());
    }
    
    @Test
    public void g_find_video() {
        logger.debug(videoService.findById(testVideo.getId()));
    }
    
    @Test(expected = VideoNotFound.class) 
    public void h_delete_video() throws VideoNotFound {
        logger.debug(videoService.delete(testVideo.getId()));
    }
    
    @Test(expected = VideoTypeNotFound.class) 
    public void i_delete_video_type() throws VideoTypeNotFound {
        logger.debug(videoService.deleteType(testVideoType.getId()));  
    }
    
    @Test(expected = MovieNotFound.class) 
    public void j_delete_movie() throws MovieNotFound {
        logger.debug(movieService.delete(testMovie.getId()));  
    }
    
}