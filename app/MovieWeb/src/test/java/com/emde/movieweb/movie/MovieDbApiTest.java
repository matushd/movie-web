/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.movie;

import com.emde.movieweb.config.MovieDbConfig;
import com.omertron.themoviedbapi.MovieDbException;
import com.omertron.themoviedbapi.TheMovieDbApi;
import com.omertron.themoviedbapi.enumeration.SearchType;
import com.omertron.themoviedbapi.model.movie.MovieInfo;
import com.omertron.themoviedbapi.results.ResultList;
import static junit.framework.TestCase.assertEquals;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author emde
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = MovieDbConfig.class
)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@Ignore
public class MovieDbApiTest {
    
    private static final Logger logger = LogManager.getLogger(MovieDbApiTest.class.getName());
    
    @Autowired
    private TheMovieDbApi moviedbApi;
    
    @Test
    public void search_movie() throws MovieDbException {
        ResultList<MovieInfo> list = moviedbApi.searchMovie("sami swoi", 0, 
                "pl", Boolean.FALSE, 0, 0, SearchType.PHRASE);
        assertEquals(list.getTotalResults(),1);
        list.getResults().iterator().forEachRemaining(n -> {
            logger.debug(n.getTitle());
        });
    }
    
}
