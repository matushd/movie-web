/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emde.movieweb.main;

import com.emde.movieweb.config.Config;
import com.emde.movieweb.main.exception.UserNotFound;
import com.emde.movieweb.main.exception.UsernameAlreadyInUseException;
import com.emde.movieweb.main.model.User;
import com.emde.movieweb.main.service.UserService;
import javax.transaction.Transactional;
import junit.framework.TestCase;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author emde
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(
        classes = Config.class
)
@TransactionConfiguration(defaultRollback = true)
@Transactional
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@Ignore
public class UserModelTest extends TestCase {
    
    @Autowired
    private UserService userService;
    
    private static Long testUserId ;
    
    @Test
    public void get_user_list() {
        userService.findAll();
    }
    
    @Test
    public void create_user() throws UserNotFound, UsernameAlreadyInUseException {
        User user = userService.create(new User("new test user"));
        testUserId = user.getId();
    }
    
    @Test
    public void find_user() {
        userService.findById(testUserId);
    }
    
    @Test(expected = UserNotFound.class) 
    public void delete_user() throws UserNotFound {
        userService.delete(testUserId);
        
    }
    
}